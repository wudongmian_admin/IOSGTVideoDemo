//
//  DraftsCollectionViewCell.h
//  GTVideoDemo
//
//  Created by apple on 2018/5/21.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DraftsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headerIV;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
