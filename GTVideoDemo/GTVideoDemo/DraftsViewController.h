//
//  DraftsViewController.h
//  GTVideoDemo
//
//  Created by apple on 2018/5/21.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DraftsViewController : UIViewController

@property (nonatomic, strong)NSArray            *videoFilesArr;
@property (nonatomic, strong)NSArray            *cellImageArr;

@end
