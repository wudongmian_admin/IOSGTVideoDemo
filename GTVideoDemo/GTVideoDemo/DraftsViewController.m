//
//  DraftsViewController.m
//  GTVideoDemo
//
//  Created by apple on 2018/5/21.
//  Copyright © 2018年 gtv. All rights reserved.
//
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#import "DraftsViewController.h"
#import "DraftsCollectionViewCell.h"
#import "PreviewViewController.h"
#import "RecordViewController.h"
#import "MergeViewController.h"
#import "UIView+Toast.h"
#import "GTVConfig.h"
@interface DraftsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView          *collectionView;

@end

@implementation DraftsViewController
{
  
}
-(void)backToHome {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [GTVConfig config].backgroundColor;
    self.title = @"发布";
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTitleTextAttributes:dic];
    [self.navigationController.navigationBar setBarTintColor:[GTVConfig config].backgroundColor];
    [self.navigationController.navigationBar setBackgroundColor:[GTVConfig config].backgroundColor];
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 34)];
    [leftBtn setTitle:@"返回" forState:UIControlStateNormal];
    [leftBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    [self.navigationItem setLeftBarButtonItem:leftItem];
    // Do any additional setup after loading the view.
    
   // [self addDetailsCollectionView];
    
}

-(void)viewWillAppear:(BOOL)animatedgg {
    self.navigationController.navigationBar.hidden = NO;
    [self.collectionView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

-(void)setVideoFilesArr:(NSArray *)videoFilesArr {
    _videoFilesArr = videoFilesArr;
    [self addDetailsCollectionView];
}

#pragma mark    ======== UICollectionView ========

- (void)addDetailsCollectionView {
    //collectionView
    UICollectionViewFlowLayout* layout2 = [[UICollectionViewFlowLayout alloc] init];
    float width = self.view.frame.size.width;
    float height = width * 23 / 72;
    layout2.itemSize = CGSizeMake(width, height);
    
    
    //设置分区的头视图和尾视图是否始终固定在屏幕上边和下边
    //    layout2.sectionFootersPinToVisibleBounds = YES;
    //    layout2.sectionHeadersPinToVisibleBounds = YES;
    
    
    //竖直滚动
    layout2.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    // 设置额外滚动区域
    layout2.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // 设置cell间距
    //设置水平间距, 注意点:系统可能会跳转(计算不准确)
    layout2.minimumInteritemSpacing = 0;
    //设置垂直间距
    layout2.minimumLineSpacing = 0;
    
    
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.view.frame.size.height) collectionViewLayout:layout2];//self.view.frame.size.width *9/16
    
    //设置背景颜色
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    //    UIGestureRecognizer *gestur = [[UIGestureRecognizer alloc]init];
    //    gestur.delegate=self;
    //    [self.musicCollectionView addGestureRecognizer:gestur];
    
    
    
    // 设置数据源,展示数据
    self.collectionView.dataSource = self;
    //设置代理,监听
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    // 注册cell
    //[self.collectionView registerClass:[DraftsCollectionViewCell class] forCellWithReuseIdentifier:@"DraftsCollectionViewCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"DraftsCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"DraftsCollectionViewCell"];
    
    /* 设置UICollectionView的属性 */
    //设置滚动条
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = YES;
    
    //设置是否需要弹簧效果
    self.collectionView.bounces = NO;
    
    [self.view insertSubview:self.collectionView atIndex:0];
}

#pragma mark    ======== UICollectionViewDelegate ========
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

// 告诉系统每组多少个
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _videoFilesArr.count;
}

// 告诉系统每个Cell如何显示
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // 1.从缓存池中取
    
    static NSString *cellID2 = @"DraftsCollectionViewCell";
    DraftsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID2 forIndexPath:indexPath];
    
    cell.titleLabel.text = _videoFilesArr[indexPath.row];
    cell.deleteButton.tag = indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(deleteFile:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.headerIV.animationImages = _cellImageArr[indexPath.row];
    cell.headerIV.animationDuration = 0.8f;
    cell.headerIV.animationRepeatCount = 99;
    [cell.headerIV startAnimating];
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        PreviewViewController * vc = [[PreviewViewController alloc] init];
    
        /** 从草稿箱跳转，不是从录制界面跳转 */
        vc.fromRecord = false;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [paths objectAtIndex:0];
        NSString * d = [NSString stringWithFormat:@"%@/abcdef/%@", documentsDir,_videoFilesArr[indexPath.row]];
        NSLog(@"d = %@", d);
        vc.workFolder = d;
        vc.draftsFolder = d;
        vc.vPath = [NSString stringWithFormat:@"%@/merge_normal.mp4",d];;
    
        RecordViewController *rVC = [[RecordViewController alloc]init];
        /** 从草稿箱跳转 */
        rVC._fromDrafts = YES;
        /** 把文件夹路径传过去，当从编辑页面返回到录制页面时，再跳转传入这个文件夹路径 */
        rVC.draftsFolder = d;
    
        MergeViewController *mVC = [[MergeViewController alloc]init];
        mVC.fromRecord = NO;
        mVC.draftsFolder = d;
        mVC.imageArr = _cellImageArr[indexPath.row];
    
        UINavigationController *navi = [[UINavigationController alloc]init];
        NSArray *vcArr = [NSArray arrayWithObjects:rVC,vc,mVC, nil];
        [navi setViewControllers:vcArr];
    
        [self presentViewController:navi animated:YES completion:^{
        
    }];
    
}



-(void)deleteFile:(UIButton *)btn {
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *path = [NSString stringWithFormat:@"%@/abcdef/%@",documentsDir,_videoFilesArr[btn.tag]];
    
    [self removeFile:path];
    self.videoFilesArr = [self reloadFils];
    [self.collectionView reloadData];
}


- (NSArray *)reloadFils {
    
    NSFileManager * fileManger = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString * p = [NSString stringWithFormat:@"%@/abcdef/", documentsDir];
    BOOL isDir = NO;
    
    // isDir判断是否为文件夹
    BOOL isExist = [fileManger fileExistsAtPath:p isDirectory:&isDir];
    
    if( !isExist ) {
        [self.view makeToast:@"没有视频，请先去录制" duration:2.0f position:CSToastPositionCenter];
        return nil;
    }else{
        
        
        if (isDir) {
            
            /** 列出目录内容，会包含文件夹和文件 */
            NSArray * dirArray = [fileManger contentsOfDirectoryAtPath:p error:nil];
            
            NSString * subPath = nil;
            
            for (NSString * str in dirArray) {
                
                subPath  = [p stringByAppendingPathComponent:str];
                
                BOOL issubDir = NO;
                
                [fileManger fileExistsAtPath:subPath isDirectory:&issubDir];
                
                NSLog(@"subPath --  %@",subPath);
            }
            
            [self.collectionView reloadData];
            if (dirArray.count == 0) {
                [self.view makeToast:@"没有视频，请先去录制" duration:2.0f position:CSToastPositionCenter];
            }
            return dirArray;
        }else{
            NSLog(@"%@",p);
            return nil;
        }
        
    }

}

-(void) removeFile:(NSString*) path{
    
    BOOL isDir;
    
    //[[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];
    
    // isDir判断是否为文件夹
    BOOL isExist = [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];
    
    if (!isExist) {
//        NSArray* files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
//        for (NSString* file in files) {
//            [self removeFile:[path stringByAppendingPathComponent:file]];
//        }
    }
    else{
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
