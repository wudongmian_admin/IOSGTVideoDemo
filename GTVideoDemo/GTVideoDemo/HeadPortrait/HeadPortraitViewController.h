//
//  HeadPortraitViewController.h
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreviewViewController.h"

@interface HeadPortraitViewController : UIViewController<UINavigationControllerDelegate>

/**
 前一个VC要动画的View
 */
@property (nonatomic, strong) UIView    *fromeView;
/**
 当前视频总时长
 */
@property (nonatomic, assign) CGFloat   *videoTotalTime;
/**
 传入视频文件路径
 */
@property (nonatomic, strong) NSString * vPath;
/**
 传入下方控件显示的image数组
 */
@property (nonatomic, strong) NSArray   *imageArr;
@property (nonatomic, copy)void(^headerImageArrBlock)(NSArray *headerImageArr);
@end
