//
//  HeadPortraitViewController.m
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
//
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height


#import "HeadPortraitViewController.h"
#import "UIView+Toast.h"
#import <GTVideoLib/GTVideoLib.h>
#import "libimg2webp.h"
#import <ClipFramework/ClipFramework.h>

@interface HeadPortraitViewController ()<VideoFrameViewDelegate>

/**
 动态头像ImageView
 */
@property (nonatomic, strong) UIImageView *animView;
/**
 视频动态头像下面滑动View
 */
@property (nonatomic, strong)VideoFrameView *videoFrameRateView;
/**
 动态头像的时间label
 */
@property (nonatomic, strong)UILabel *timeLabel;

@property (nonatomic, strong) CustomInteractiveTransition *interactiveTransition;

@property (nonatomic, strong)NSArray        *headerImageArr;
@end

@implementation HeadPortraitViewController
{
    CGFloat _currentTime;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    
    _headerImageArr = [NSArray array];
    _currentTime = 0;
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20, SCREEN_HEIGHT - 40, 80, 20)];
    [btn setTitle:@"取消" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn];
    
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 100, SCREEN_HEIGHT - 40, 80, 20)];
    [btn1 setTitle:@"确定" forState:UIControlStateNormal];
    
    [btn1 addTarget:self action:@selector(pickBtn_WithFrame:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn1];
    
    CGFloat y = 0;
    BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
    if (IphoneX) {
        y = 44;
    }
    
    UIImageView *bigIV = [[UIImageView alloc]initWithFrame:CGRectMake(44, y, self.view.frame.size.width-88, (self.view.frame.size.width-88) * 16 / 9)];
    self.animView = bigIV;
    [self.view addSubview:bigIV];
    bigIV.tag = 201;
    
    self.videoFrameRateView = [[VideoFrameView alloc]initWithFrame:CGRectMake(16, SCREEN_HEIGHT - 60 - 60-1, SCREEN_WIDTH-32, 60 +2)];
    [self.videoFrameRateView initViewsWithNum:7];
    self.videoFrameRateView.backgroundColor = [UIColor blackColor];
    //[self.videoFrameRateView setImagetoTimeLine:[UIImage imageNamed:@"IMG_0455.JPG"] withWidth:4];
    self.videoFrameRateView.delegate = self;
    //self.videoFrameRateView.totalTime = self.totalTime;
    [self.view addSubview:self.videoFrameRateView];
    
    
    UILabel *l3 = [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 60 - 60-1 - 20, SCREEN_WIDTH, 15)];
    l3.textColor = [UIColor whiteColor];
    l3.text = @"0";
    l3.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:l3];
    _timeLabel = l3;
    
    
    
    //初始化手势过渡的代理
    self.interactiveTransition = [CustomInteractiveTransition interactiveTransitionWithTransitionType:CustomInteractiveTransitionTypePop GestureDirection:CustomInteractiveTransitionGestureDirectionRight];
    
}
-(void)setImageArr:(NSArray *)imageArr {
    _imageArr = imageArr;
    if (_imageArr.count) {
        for (int i = 0; i<self.imageArr.count; i++) {
            [self.videoFrameRateView loadImages:self.imageArr[i] withIndex:i];
        }
    }
}
-(void)setVideoTotalTime:(CGFloat *)videoTotalTime {
    _videoTotalTime = videoTotalTime;
    [self.videoFrameRateView loadTime:*(_videoTotalTime)];
}
-(void)setVPath:(NSString *)vPath {
    _vPath = vPath;
    [self loadAnimationViewWithTime:0];
}
-(void)dismiss {
    /**
     必须给YES
     */
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 确定后导出当前时间的webP图片
 */
- (void) pickBtn_WithFrame:(id)sender
{
    if (self.headerImageArrBlock) {
        self.headerImageArrBlock(_headerImageArr);
    }
    
    //[self.view makeToastActivity:CSToastPositionCenter];
    [self.view makeToast:@"test.webp已生成至temp目录，请从xcode下查看" duration:2.0f position:CSToastPositionCenter];
    // 抽取动画帧
    NSString * m = _vPath;
    
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_pic_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(_currentTime, 1000);
        NSArray * res = [GTVideoTool syncGrabRawYUV:m toFolder:rootfolder withinRange:r andNumLimit:7];
        
        // 生成webp
        NSString * outwebp = [NSString stringWithFormat:@"%@/test.webp", NSTemporaryDirectory()];
        char * inputlist[10];
        for( int i=0; i<res.count; i++ ) {
            NSDictionary * d = (NSDictionary*)[res objectAtIndex:i];
            NSString * filepath = [d objectForKey:@"filepath"];
            inputlist[i] = (char *)[filepath cStringUsingEncoding:NSUTF8StringEncoding];
        }
        char * output_file = (char*)[outwebp cStringUsingEncoding:NSUTF8StringEncoding];
        img2webp(inputlist, 5, output_file);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view hideToastActivity];
            
            
            
            [self.navigationController popViewControllerAnimated:YES];
        });
    });
    
    return;
}



#pragma mark    ======== VideoFrameViewDelegate ========
-(void)VideoFrameViewDidEndWithTime:(CGFloat)time {
    //thumbList_index = time / (self.totalTime /12 );
    
    _timeLabel.text = [NSString stringWithFormat:@"%.2f",time];
    [self loadAnimationViewWithTime:time];
    _currentTime = time;
}
-(void)loadAnimationViewWithTime:(CGFloat)time {
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_yuv_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(time * 1000, 1000);
        NSArray * res = [GTVideoTool syncGrabJPEG:self.vPath toFolder:rootfolder withinRange:r andNumLimit:7];
        
        NSDictionary * elem = nil;
        NSMutableArray * l = [[NSMutableArray alloc] init];
        for( int i=0; i<res.count; i++ ) {
            
            elem = [res objectAtIndex:i];
            NSString * fpath = ((NSString*)[elem objectForKey:@"filepath"]);
            NSData * d = [NSData dataWithContentsOfFile:fpath];
            UIImage * img = [UIImage imageWithData:d];
            if (img == nil) {
                return;
            }
            [l addObject:img];
        }
        _headerImageArr = l;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view hideToastActivity];
            
            self.animView.animationImages = l;
            self.animView.animationDuration = 0.8f;
            self.animView.animationRepeatCount = 100;
            if (l.count > 0) {
                [self.videoFrameRateView loadSmallImage:(UIImage *)l[0]];
                
            }
            
            [self.animView startAnimating];//开始动画
        });
    });
}
-(void)setImageForDraggingTime:(CGFloat)nowTime {
    
    
    _timeLabel.text = [NSString stringWithFormat:@"%.2f",nowTime];
    
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_yuv_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }

    
    NSRange r = NSMakeRange(nowTime * 1000, 1000);
    NSArray * res = [GTVideoTool syncGrabJPEG:self.vPath toFolder:rootfolder withinRange:r andNumLimit:1];
    
    NSDictionary * elem = nil;
    NSMutableArray * l = [[NSMutableArray alloc] init];
    for( int i=0; i<res.count; i++ ) {
        
        elem = [res objectAtIndex:i];
        NSString * fpath = ((NSString*)[elem objectForKey:@"filepath"]);
        NSData * d = [NSData dataWithContentsOfFile:fpath];
        UIImage * img = [UIImage imageWithData:d];
        if (img == nil) {
            return;
        }
        [l addObject:img];
    }
    
    
    [self.animView stopAnimating];
    if (l.count > 0) {
        [self.animView setImage:l[0]];
        [self.videoFrameRateView loadSmallImage:(UIImage *)l[0]];
    }

}


#pragma mark    ======== UINavigationControllerDelegate ========
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC{
    
    CGFloat y = 0;
    BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
    if (IphoneX) {
        y = 44;
    }
    UIImageView *bigIV = [[UIImageView alloc]initWithFrame:CGRectMake(44, y, self.view.frame.size.width-88, (self.view.frame.size.width-88) * 16 / 9)];
    
    return [CustomAnimatedTransitioning transitionWithTransitionType:operation == UINavigationControllerOperationPush ? CustomTransitionTypePush : CustomTransitionTypePop andWay:1 andFromeView:self.fromeView andToView:bigIV];
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController{
    return _interactiveTransition.interation ? _interactiveTransition : nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
