//
//  MergeViewController.h
//  GTVideoDemo
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MergeViewController : UIViewController
@property (nonatomic, assign) BOOL fromRecord;
@property (nonatomic, strong) NSString * draftsFolder;
@property (nonatomic, strong) NSArray  *imageArr;
@end
