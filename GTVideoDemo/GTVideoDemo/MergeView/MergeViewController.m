//
//  MergeViewController.m
//  GTVideoDemo
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 gtv. All rights reserved.
//
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#import "MergeViewController.h"
#import "UIView+Toast.h"
#import "GTVConfig.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <GTVideoLib/GTVideoLib.h>
@interface MergeViewController ()
@property (nonatomic, strong)UITextField *textField;
@property (nonatomic, strong)UIImageView *imageView;
@property (nonatomic, strong)UIButton    *draftsButton;
@property (nonatomic, strong)UIButton    *sendButton;
@property (nonatomic, strong) GTVReplayController * mergeCtrl;
@property (nonatomic, strong) UILabel            *testExportLabel;
@end

@implementation MergeViewController
-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
    
}
-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

-(void)backToPreView {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)backToDraftsView {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"发布";
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTitleTextAttributes:dic];
    [self.navigationController.navigationBar setBarTintColor:[GTVConfig config].backgroundColor];
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 34)];
    [leftBtn setTitle:@"返回编辑" forState:UIControlStateNormal];
    [leftBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(backToPreView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    [self.navigationItem setLeftBarButtonItem:leftItem];
    
    if (!_fromRecord) {
        UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 34)];
        [rightBtn setTitle:@"取消" forState:UIControlStateNormal];
        [rightBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(backToDraftsView) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
        [self.navigationItem setRightBarButtonItem:rightItem];
    }
    
    // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.view.backgroundColor = [GTVConfig config].backgroundColor;
    CGRect rect = [UIScreen mainScreen].bounds;
    CGFloat y = 0;
    BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
    if (IphoneX) {
        y = 44;
    }
    
    
    _textField = [[UITextField alloc]initWithFrame:CGRectMake(0, y, rect.size.width, 200)];
    _textField.backgroundColor = [UIColor lightTextColor];
    _textField.placeholder = @"请输入您的评论";
    
    y += 220;
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, y, 80, 80)];
    //_imageView.backgroundColor = [UIColor yellowColor];
    
    self.imageView.animationImages = _imageArr;
    self.imageView.animationDuration = 0.8f;
    self.imageView.animationRepeatCount = 99;
    [self.imageView startAnimating];
    
    _draftsButton = [[UIButton alloc]initWithFrame:CGRectMake(10, rect.size.height - 200, rect.size.width / 3, 44)];
    _draftsButton.backgroundColor = [UIColor lightGrayColor];
    [_draftsButton setTitle:@"草稿" forState:UIControlStateNormal];
    [_draftsButton addTarget:self action:@selector(DraftsBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    _sendButton = [[UIButton alloc]initWithFrame:CGRectMake(_draftsButton.frame.size.width + 20, rect.size.height - 200, rect.size.width - _draftsButton.frame.size.width - 30, 44)];
    _sendButton.backgroundColor = [UIColor redColor];
    [_sendButton setTitle:@"发布" forState:UIControlStateNormal];
    [_sendButton addTarget:self action:@selector(sendBtn_OnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_textField];
    [self.view addSubview:_imageView];
    [self.view addSubview:_draftsButton];
    [self.view addSubview:_sendButton];
    [self testExportLabel];
}
//-(UIImageView *)imageView {
//    if (!_imageView) {
//        _imageView = [[UIImageView alloc]init];
//    }
//    return _imageView;
//}
-(void)setImageArr:(NSArray *)imageArr {
    _imageArr  = imageArr;
//    [self.imageView setAnimationImages:imageArr];
//    [self.imageView startAnimating];
//    self.imageView.animationRepeatCount = 99;
}
-(void)sendBtn_OnClicked {
     NSString * tmpDir;
    /** 是否从录制界面进来 */
    if (_fromRecord) {
        /** 录制界面不是从草稿箱进，从录制缓存文件夹取 */
        [self sendFromRec];
        
        
         /** 录制界面是否从草稿箱进-->是否有草稿箱文件夹  有的话需要删除 */
        if (_draftsFolder) {
            tmpDir = [NSString stringWithFormat:@"%@/",_draftsFolder];
            [self removeFile:tmpDir];
        }
        
    }else{
        self.testExportLabel.hidden = NO;
        [self sendFromdDraftsWithPath:_draftsFolder];
     
        /** 发送成功后需要删除草稿箱文件 */
        return;
    }
}

-(void) removeFile:(NSString*) path{
    
    BOOL isDir;
    
    [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];
    
    if (isDir) {
        NSArray* files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
        for (NSString* file in files) {
            [self removeFile:[path stringByAppendingPathComponent:file]];
        }
    }
    else{
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
}
- (NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    return dateTime;
}
- (void) DraftsBtn_OnClicked:(id)sender {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    //[self.view makeToast:@"请稍后" duration:.5f position:CSToastPositionCenter];
    NSString * tmpDir;
    /** 是否从录制界面进来 */
    if (_fromRecord) {
        
        /** 录制界面是否从草稿箱进-->是否有草稿箱文件夹 */
        if (_draftsFolder) {
            tmpDir = [NSString stringWithFormat:@"%@/",_draftsFolder];
            [self removeFile:tmpDir];
        }else{
            /** 录制界面不是从草稿箱进，创建新文件夹 */
            NSString *randomName = [self getCurrentTime];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDir = [paths objectAtIndex:0];
            tmpDir = [NSString stringWithFormat:@"%@/abcdef/%@/", documentsDir,randomName];
        }
        
    }else{
        /** 直接从草稿箱进来，不会有新的视频文件，只需要保存配置文件 */
        //        NSString * path = [NSString stringWithFormat:@"%@/sample.mp4", NSTemporaryDirectory()];
        //        [self.replayCtrl exportReplayFile:path withSize:CGSizeMake(576,1024) andBitrate:2500000];
   //     [self.replayCtrl saveConfigToDisk];
        [self.view hideToastActivity];
        [self saveImageArrayWithPath:[NSString stringWithFormat:@"%@/",_draftsFolder]];
        [self.view makeToast:@"已保存到草稿箱" duration:2.0f position:CSToastPositionCenter];
        return;
    }
    
    
    [self.view hideToastActivity];
    
    NSFileManager * fileManger = [NSFileManager defaultManager];
    
    BOOL isHave = [fileManger fileExistsAtPath:tmpDir];
    if (isHave) {
        
    }else {
        BOOL isSuccessCreate = [fileManger createDirectoryAtPath:tmpDir withIntermediateDirectories:YES attributes:nil error:nil];
        NSLog(@"%@",isSuccessCreate ? @"创建成功" : @"创建失败");
    }
    
    NSString * p = [NSString stringWithFormat:@"%@/abcdefg/", NSTemporaryDirectory()];
    
    
    BOOL isDir = NO;
    
    BOOL isExist = [fileManger fileExistsAtPath:p isDirectory:&isDir];
    
    if( !isExist ) {
        [self.view makeToast:@"请先去录制" duration:2.0f position:CSToastPositionCenter];
        return;
    }else{
        
        
        if (isDir) {
            
            NSArray * dirArray = [fileManger contentsOfDirectoryAtPath:p error:nil];
            
            NSString * subPath = nil;
            
            for (NSString * str in dirArray) {
                
                subPath  = [p stringByAppendingPathComponent:str];
                
                BOOL issubDir = NO;
                
                [fileManger fileExistsAtPath:subPath isDirectory:&issubDir];
                
                NSString *toPath = [NSString stringWithFormat:@"%@/%@",tmpDir,str];
                BOOL isSuccess = [fileManger copyItemAtPath:subPath toPath:toPath error:nil];
                
                if (isSuccess) {
                    NSLog(@"copyItem isSuccess");
                }
                
                NSLog(@"subPath --  %@",subPath);
            }
            
            
        }else{
            NSLog(@"%@",p);
        }
        
    }
    
    [self saveImageArrayWithPath:tmpDir];
    [self.view makeToast:@"已保存到草稿箱" duration:2.0f position:CSToastPositionCenter];
}

-(void)saveImageArrayWithPath:(NSString *)path{
    for (int i = 0; i < _imageArr.count; i++) {
        UIImage *image = _imageArr[i];
        [self saveImage:image index:i path:path];
    }
}
- (void)saveImage:(UIImage *)image index:(int )index path:(NSString *)path{
    //NSArray *paths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    
    NSString *filePath = [path stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"headerImage%d.png",index]];  // 保存文件的名称
    
    BOOL result =[UIImagePNGRepresentation(image)writeToFile:filePath   atomically:YES]; // 保存成功会返回YES
    if (result == YES) {
        NSLog(@"保存成功");
    }
    
}


/** 录制界面不是从草稿箱进，从录制缓存文件夹取 */
-(void)sendFromRec {
    // 全I帧视频存储目录
    NSString * p = [NSString stringWithFormat:@"%@/abcdefg/merge_normal.mp4", NSTemporaryDirectory()];
    if( [[NSFileManager defaultManager] fileExistsAtPath:p] == false ) {
        [self.view makeToast:@"请先去录制" duration:2.0f position:CSToastPositionCenter];
        return;
    }
    
    // 设定文件存储目录
    NSString * d = [NSString stringWithFormat:@"%@/abcdefg/", NSTemporaryDirectory()];
    
    // 指定草稿箱直接合成
    self.mergeCtrl = [[GTVReplayController alloc] init];
    [self.mergeCtrl setReplayDelegate:self];
    
    [self.mergeCtrl setReplayVideoPath:p];
    [self.mergeCtrl setReplayConfigFolder:d forceInitFlag:false];
    
    // 创建一个隐藏view，用于合成
    UIView * v = [[UIView alloc] initWithFrame:CGRectMake(-10, 10, 1, 1)];
    [self.view addSubview:v];
    
    [self.mergeCtrl setReplayRenderView:v withFrame:CGRectMake(0, 0, 1, 1)];
    
    // 设置logo
    [self.mergeCtrl setMark:[UIImage imageNamed:@"logo.png"] atRect:CGRectMake(10, 10, 50, 50)];
    [self.mergeCtrl setExtraMark:[UIImage imageNamed:@"logo.png"] atRect:CGRectMake(10, 80, 50, 50)];
    
    // 开始执行导出操作
    [self.view makeToastActivity:CSToastPositionCenter];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString * path = [NSString stringWithFormat:@"%@/sample.mp4", NSTemporaryDirectory()];
        [self.mergeCtrl exportReplayFile:path withSize:CGSizeMake(540,960) andBitrate:4000000];
        
        NSString * n = path;
        
        if( [[NSFileManager defaultManager] fileExistsAtPath:n] ) {
            NSLog(@"file ok");
            usleep(300*1000);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(n.length > 0){
                NSURL *outputURL = [NSURL URLWithString:n];
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                    [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (error) {
                                NSLog(@"recordFile failed %@ ", n);
                            } else {
                                NSLog(@"recordFile succeed %@ ", n);
                            }
                            
                            [self.view makeToast:@"视频已经导出到相册" duration:2.0f position:CSToastPositionCenter];
                        });
                    }];
                }
            }
            
            [self.view hideToastActivity];
            [v removeFromSuperview];
        });
    });
}


/** 录制界面从草稿箱进，从草稿箱取 */
-(void)sendFromdDraftsWithPath:(NSString *)fPath{
    self.mergeCtrl = [[GTVReplayController alloc] init];
    [self.mergeCtrl setReplayDelegate:self];
    
    [self.mergeCtrl setReplayVideoPath:[NSString stringWithFormat:@"%@/merge_normal.mp4",fPath]];
    [self.mergeCtrl setReplayConfigFolder:[NSString stringWithFormat:@"%@/",fPath] forceInitFlag:false];
  //  dispatch_async(dispatch_get_main_queue(), ^{
        UIView * v = [[UIView alloc] initWithFrame:CGRectMake(-10, 10, 1, 1)];
        [self.view insertSubview:v atIndex:0];
        
        [self.mergeCtrl setReplayRenderView:v withFrame:CGRectMake(0, 0, 1, 1)];
        self.testExportLabel.hidden = NO;
  //  });

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
       // [self.view makeToastActivity:CSToastPositionCenter];
        
    
 //       _exportReplayFile = YES;
        NSString * path = [NSString stringWithFormat:@"%@/sample.mp4", NSTemporaryDirectory()];
        NSString * path1 = [NSString stringWithFormat:@"%@/allKeySample.mp4", NSTemporaryDirectory()];
        //[self.replayCtrl exportReplayFile:path withSize:CGSizeMake(576,1024) andBitrate:2500000];
        
        
        
        BOOL success = [self.mergeCtrl exportReplayFile:path andKeyFile:path1 withSize:CGSizeMake(544, 960) andBitrate:2500000];

        if (success) {
 //           _exportReplayFile = NO;
            NSString * n = path;
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.testExportLabel.hidden = YES;
                self.testExportLabel.text = @"";
            });
           
            
            
            
            
            if( [[NSFileManager defaultManager] fileExistsAtPath:n] ) {
                NSLog(@"file ok");
                usleep(300*1000);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(n.length > 0){
                    NSURL *outputURL = [NSURL URLWithString:n];
                    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                        [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (error) {
                                    NSLog(@"recordFile failed %@ ", n);
                                } else {
                                    NSLog(@"recordFile succeed %@ ", n);
                                }
                                
                               //  [self.view hideToastActivity];
                                
                                [self.view makeToast:@"视频已经导出到相册" duration:2.0f position:CSToastPositionCenter];
                                
                                //[self performSelector:@selector(delayPopup) withObject:nil afterDelay:2.5f];
                            });
                        }];
                    }
                }
                
                
            });
        }
        
        
    });
    
}

-(UILabel *)testExportLabel {
    if (!_testExportLabel) {
        _testExportLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2 - 100, SCREEN_HEIGHT / 2 - 30, 200, 60)];
        _testExportLabel.font = [UIFont systemFontOfSize:14];
        _testExportLabel.backgroundColor = [UIColor blackColor];
        _testExportLabel.textColor = [UIColor whiteColor];
        _testExportLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_testExportLabel];
        _testExportLabel.hidden = YES;
        //_testExportLabel.text = @"12123123123";
    }
    return _testExportLabel;
}

- (void) notifyReplayStatus:(NSString*)evt withInfo:(NSDictionary*)info
{
    
    // 播放器回调
    if( [evt isEqualToString:kGTVReplayEventCompleted] ) {
        NSLog(@"evt ----   kGTVReplayEventCompleted");
    }
    // 预读完毕，获取到文件时长
    if( [evt isEqualToString:kGTVReplayEventPrepared] ) {
        NSLog(@"evt ----   kGTVReplayEventPrepared");
    }
    // 播放失败
    if( [evt isEqualToString:kGTVReplayEventError] ) {
        NSLog(@"evt ----   kGTVReplayEventError");
    }
    // 合成进度更新(返回percent)
    if( [evt isEqualToString:kGTVReplayEventExportStatus] ) {
        NSLog(@"evt ----   kGTVReplayEventExportStatus");
        if (info) {
            NSString *percent = info[@"percent"];
            NSLog(@"percent    %@",percent);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _testExportLabel.text = percent;
                NSLog(@"percent text   %@",_testExportLabel.text);
            });
            
        }
    }
    
    //特效
    if( [evt isEqualToString:kGTVReplayEffectKeyStart] ) {
        NSLog(@"evt ----   kGTVReplayEffectKeyStart");
    }
    if( [evt isEqualToString:kGTVReplayEffectKeyStop] ) {
        NSLog(@"evt ----   kGTVReplayEffectKeyStop");
    }
    if( [evt isEqualToString:kGTVReplayEffectKeyName] ) {
        NSLog(@"evt ----   kGTVReplayEffectKeyName");
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
