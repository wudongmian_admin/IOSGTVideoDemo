//
//  MusicDoneCell.m
//  GTVideoDemo
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import "MusicDoneCell.h"
#import "ComDef.h"
@implementation MusicDoneCell
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 1, frame.size.width, 28)];
        lable.text = @"选定音乐";
        lable.textAlignment = NSTextAlignmentCenter;
        lable.textColor = [UIColor whiteColor];
        [self.contentView addSubview:lable];
        self.contentView.layer.masksToBounds = YES;
        self.contentView.layer.cornerRadius = 8;
        self.backgroundColor = RGBToColor(235, 93, 102);
    }
    return self;
}
@end
