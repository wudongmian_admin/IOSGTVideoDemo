//
//  SelectMusicCell.h
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicSelectElemView.h"

@protocol SelectMusicCellDelegate <NSObject>

- (void) musicElemViewOnTouched:(UIView*)view;
- (void) musicElemViewOnPlayed:(UIView*)view;

@end

@interface SelectMusicCell : UICollectionViewCell
@property (nonatomic, assign) id<SelectMusicCellDelegate> delegate;
@property (nonatomic, copy)void(^musicPlayBlock)(NSInteger index);
 @property (nonatomic, strong) UIButton * musicBtn;
- (void) setIconImage:(UIImage*)img andName:(NSString*)name andInfo:(NSString*)info;

// 外部动态更新
- (void) setSelected:(BOOL)flg;
- (void) setPlaying:(BOOL)flg;

- (BOOL) isPlaying;
- (BOOL) isSelected;
@end