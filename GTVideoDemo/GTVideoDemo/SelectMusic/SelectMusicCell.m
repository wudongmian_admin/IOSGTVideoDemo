//
//  SelectMusicCell.m
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import "SelectMusicCell.h"
#import "ComDef.h"

@interface SelectMusicCell()
@property (nonatomic, strong) UIImageView * iconView;
@property (nonatomic, strong) UIImageView * playView;
@property (nonatomic, strong) UIImageView * pauseView;
//@property (nonatomic, strong) UIButton * musicBtn;

@property (nonatomic, strong) UILabel * nameLbl;
@property (nonatomic, strong) UILabel * infoLbl;
@property (nonatomic, strong) UIButton * checkBtn;
@property (nonatomic, strong) UIButton * selectBtn;

@end
@implementation SelectMusicCell
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:RGBToColor(35, 42, 66)];
        
        float width = frame.size.width;
        float height = frame.size.height;
        
        self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(width/20, height/6, (height*2/3) * (170.0f/160.0f), height*2/3)];
        [self addSubview:self.iconView];
        CGRect iconFrame = self.iconView.frame;
        
        {
            self.playView = [[UIImageView alloc] initWithFrame:CGRectMake(iconFrame.size.width/2-24/2, iconFrame.size.height/2-24/2, 24, 24)];
            [self.playView setImage:[UIImage imageNamed:@"selmusic-play"]];
            [self.iconView addSubview:self.playView];
            
            self.pauseView = [[UIImageView alloc] initWithFrame:CGRectMake(iconFrame.size.width/2-24/2, iconFrame.size.height/2-24/2, 24, 24)];
            [self.pauseView setImage:[UIImage imageNamed:@"selmusic-pause"]];
            [self.iconView addSubview:self.pauseView];
            
            // 默认显示播放按钮
            [self.pauseView setHidden:true];
        }
        self.musicBtn = [[UIButton alloc] initWithFrame:iconFrame];
        [self addSubview:self.musicBtn];
        [self.musicBtn addTarget:self action:@selector(musicBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        float x = width/20 + height*2/3 + width/20;
        self.nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(x, height/4, width/2, height/4)];
        [self.nameLbl setTextColor:[UIColor whiteColor]];
        [self.nameLbl setFont:[UIFont systemFontOfSize:16.0f]];
        [self addSubview:self.nameLbl];
        
        self.infoLbl = [[UILabel alloc] initWithFrame:CGRectMake(x, height/2, width/2, height/3)];
        [self.infoLbl setTextColor:RGBToColor(134,141,163)];
        self.infoLbl.numberOfLines = 0;
        [self.infoLbl setFont:[UIFont systemFontOfSize:10.0f]];
        [self addSubview:self.infoLbl];
        
        self.checkBtn = [[UIButton alloc] initWithFrame:CGRectMake(width-width/20-44, height/2-22, 44, 44)];
        [self.checkBtn setImage:[UIImage imageNamed:@"selmusic-uncheck"] forState:UIControlStateNormal];
        [self.checkBtn setImage:[UIImage imageNamed:@"selmusic-checked"] forState:UIControlStateSelected];
        [self addSubview:self.checkBtn];
        
        UIView * splitView = [[UIView alloc] initWithFrame:CGRectMake(width/20, height-2, width*9/10, 0.5)];
        [splitView setBackgroundColor:RGBToColor(254,254,254)];
        [self addSubview:splitView];
        
        x = iconFrame.size.width + iconFrame.origin.x;
        self.selectBtn = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width-x, height)];
        //        self.selectBtn.alpha = 0.5f;
        //        [self.selectBtn setBackgroundColor:[UIColor orangeColor]];
      //  [self.selectBtn addTarget:self action:@selector(selectBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
       // [self addSubview:self.selectBtn];
    }
    return self;
}

- (void) setIconImage:(UIImage*)img andName:(NSString*)name andInfo:(NSString*)info
{
    [self.iconView setImage:img];
    [self.nameLbl setText:name];
    [self.infoLbl setText:info];
}

- (void) setSelected:(BOOL)flg
{
    //self.checkBtn.selected = flg;
    self.checkBtn.selected = flg;
}

- (void) setPlaying:(BOOL)flg
{
    if( flg ) {
        [self.playView setHidden:true];
        [self.pauseView setHidden:false];
    }
    else {
        [self.playView setHidden:false];
        [self.pauseView setHidden:true];
    }
}

- (BOOL) isPlaying
{
    return self.playView.hidden;
}

- (BOOL) isSelected
{
    return self.checkBtn.selected;
}

- (void) selectBtn_OnClicked:(id)sender
{
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(musicElemViewOnTouched:)] ) {
        
        [self.delegate performSelector:@selector(musicElemViewOnTouched:) withObject:self];
    }
    
    return;
}

- (void) musicBtn_OnClicked:(UIButton *)sender
{
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(musicElemViewOnPlayed:)] ) {
        
        [self.delegate performSelector:@selector(musicElemViewOnPlayed:) withObject:self];
    }
    if (self.musicPlayBlock) {
        self.musicPlayBlock(sender.tag);
    }
    
    return;
}
@end
