//
//  SelectMusicViewController.h
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  SelectMusicViewControllerDelegate <NSObject>

- (void) SelectMusicViewControllerPressedDissmiss;
-(id<UIViewControllerInteractiveTransitioning>)SelectMusicViewControllerInteractiveTransitionForPresent;
-(void)SelectMusicDone:(NSString *)musicPath;

@end
@interface SelectMusicViewController : UIViewController
@property (nonatomic, assign) id< SelectMusicViewControllerDelegate> delegate;
@end
