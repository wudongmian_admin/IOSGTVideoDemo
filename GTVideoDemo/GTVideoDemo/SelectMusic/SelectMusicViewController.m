//
//  SelectMusicViewController.m
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
// 

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#import "SelectMusicViewController.h"
#import "SelectMusicCell.h"
#import "MusicDoneCell.h"
#import <AVFoundation/AVFoundation.h>
#import <ClipFramework/ClipFramework.h>

static NSString * musicList[] = {
    @"-", @"panama.mp3", @"lvxing.mp3", @"hongzhaoyuan.mp3", @"love.mp3",  @"panama.mp3", @"lvxing.mp3", @"hongzhaoyuan.mp3", @"love.mp3"
};
static NSString * musicNameList[] = {
    @"无", @"Panama", @"带你去旅行", @"红昭愿", @"Love Story", @"Panama", @"带你去旅行", @"红昭愿", @"Love Story"
};
static NSString * musicIconList[] = {
    @"0", @"selmusic-cover1", @"selmusic-cover2", @"selmusic-cover3", @"selmusic-cover4", @"selmusic-cover1", @"selmusic-cover2", @"selmusic-cover3", @"selmusic-cover4"
};
static NSString * musicInfoList[] = {
    @"0", @"Matteo\n00:15", @"校长\n00:32", @"音阙诗听\n00:17", @"Tailor Swift\n00:56", @"Matteo\n00:15", @"校长\n00:32", @"音阙诗听\n00:17", @"Tailor Swift\n00:56"
};

@interface SelectMusicViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,MusicSelectElemViewDelegate,UIViewControllerTransitioningDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong)UICollectionView       *musicCollectionView;
@property (nonatomic, strong) NSMutableArray * elemList;
@property (nonatomic, strong) AVAudioPlayer * audioPlayer;
@property (nonatomic, strong) CustomInteractiveTransition *interactiveDismiss;
@property (nonatomic, strong) CustomInteractiveTransition *interactivePush;
@end

@implementation SelectMusicViewController
{
    int choosed;
    CGFloat _lastY;
    UIGestureRecognizer *_gesture;
    NSMutableArray *_selectedArr;
    NSMutableArray *_musicSelectedArr;
    CGFloat     _cellWidth;
    CGFloat     _cellHeight;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
        //self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _selectedArr = [NSMutableArray array];
    _musicSelectedArr = [NSMutableArray array];
    choosed = 0;
    for (int i = 0; i<10; i++) {
        [_selectedArr addObject:@"0"];
        [_musicSelectedArr addObject:@"0"];
    }
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.layer.cornerRadius = 8;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 30)];
    titleLabel.text = @"选择音乐";
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:titleLabel];
    
    
    //collectionView
    UICollectionViewFlowLayout* layout2 = [[UICollectionViewFlowLayout alloc] init];
    float width = self.view.frame.size.width;
    float height = width * 233 / 720;
//    layout2.itemSize = CGSizeMake(width, height);
//    layout2.estimatedItemSize = CGSizeMake(width, height);
    _cellWidth = width;
    _cellHeight = height;
    
    //设置分区的头视图和尾视图是否始终固定在屏幕上边和下边
    //    layout2.sectionFootersPinToVisibleBounds = YES;
    //    layout2.sectionHeadersPinToVisibleBounds = YES;
    
    
    //竖直滚动
    layout2.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    // 设置额外滚动区域
    layout2.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // 设置cell间距
    //设置水平间距, 注意点:系统可能会跳转(计算不准确)
    layout2.minimumInteritemSpacing = 0;
    //设置垂直间距
    layout2.minimumLineSpacing = 0;
    
    
    
    _musicCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 100, SCREEN_WIDTH, self.view.frame.size.height - 120) collectionViewLayout:layout2];
    
    //设置背景颜色
    _musicCollectionView.backgroundColor = [UIColor clearColor];
    
//    UIGestureRecognizer *gestur = [[UIGestureRecognizer alloc]init];
//    gestur.delegate=self;
//    [self.musicCollectionView addGestureRecognizer:gestur];
    

    
    // 设置数据源,展示数据
    _musicCollectionView.dataSource = self;
    //设置代理,监听
    _musicCollectionView.delegate = self;
    _musicCollectionView.alwaysBounceVertical = YES;
    
    // 注册cell
    [_musicCollectionView registerClass:[SelectMusicCell class] forCellWithReuseIdentifier:@"SelectMusicCell"];
    [_musicCollectionView registerClass:[MusicDoneCell class] forCellWithReuseIdentifier:@"MusicDoneCell"];
    
    /* 设置UICollectionView的属性 */
    //设置滚动条
    _musicCollectionView.showsHorizontalScrollIndicator = NO;
    _musicCollectionView.showsVerticalScrollIndicator = YES;
    
    //设置是否需要弹簧效果
    _musicCollectionView.bounces = YES;
    
    [self.view addSubview:_musicCollectionView];
    
    self.interactiveDismiss = [CustomInteractiveTransition interactiveTransitionWithTransitionType:CustomInteractiveTransitionTypeDismiss GestureDirection:CustomInteractiveTransitionGestureDirectionDown];
    [self.interactiveDismiss addPanGestureForViewController:self];
    
    
    NSArray *gestureArray = self.view.gestureRecognizers;
    
    for (UIGestureRecognizer *gestureRecognizer in gestureArray) {
        
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            
            _gesture = gestureRecognizer;
            _gesture.delegate = self;
           // [self.musicCollectionView.panGestureRecognizer requireGestureRecognizerToFail:gestureRecognizer];
            
        }
        
    }
}

#pragma mark    ======== UICollectionViewDelegate ========
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 9;
}

// 告诉系统每组多少个
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return 2;
    
    
    
}

// 告诉系统每个Cell如何显示
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // 1.从缓存池中取
    if (indexPath.row == 0) {
        static NSString *cellID2 = @"SelectMusicCell";
        SelectMusicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID2 forIndexPath:indexPath];
        
        cell.musicBtn.tag = indexPath.section;
        [cell setIconImage:[UIImage imageNamed:musicIconList[indexPath.section]] andName:musicNameList[indexPath.section] andInfo:musicInfoList[indexPath.section]];
        NSString *selected = _selectedArr[indexPath.section];
        [cell setSelected:[selected  isEqual: @"0"] ? NO : YES];
        NSString *musicSelected = _musicSelectedArr[indexPath.section];
        [cell setPlaying:[musicSelected isEqual:@"0"]? NO : YES];
        __weak typeof (self)weakSelf = self;
        cell.musicPlayBlock = ^(NSInteger index) {
            [weakSelf playMusicWithIndex:index];
        };
        return cell;
    }else{
        //MusicDoneCell
        static NSString *cellID2 = @"MusicDoneCell";
        MusicDoneCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID2 forIndexPath:indexPath];
        NSString *selected = _selectedArr[indexPath.section];
        
        return cell;
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
    [self stopMusic];
    NSString * path = musicList[indexPath.section];
    NSString * choosedMusicPath = [[NSBundle mainBundle] pathForResource:path ofType:@"" inDirectory:@""];
    NSLog(@"choosedMusicPath = %@", choosedMusicPath);
    if( choosedMusicPath.length > 0 ) {
        [self playMusic:choosedMusicPath];
    }
    else {
        [self stopMusic];
    }
    
    
    for (int i = 0; i < _musicSelectedArr.count ; i++) {
        NSString *selected = _musicSelectedArr[i];
        selected = @"0";
        _musicSelectedArr[i] = selected;
        _selectedArr[i] = selected;
    }
    NSString *s =  _musicSelectedArr[indexPath.section];
    s = @"1";
    _musicSelectedArr[indexPath.section] = s;
//    for (int i = 0; i < _selectedArr.count ; i++) {
//        NSString *selected = _selectedArr[i];
//        selected = @"0";
//        _selectedArr[i] = selected;
//    }
//    NSString *s = _selectedArr[indexPath.section];
//    s = @"1";
    _selectedArr[indexPath.section] = s;
    [self.musicCollectionView reloadData];
    }else{
        
        NSString * path = musicList[indexPath.section];
        NSString * choosedMusicPath = [[NSBundle mainBundle] pathForResource:path ofType:@"" inDirectory:@""];
        [self dismissViewControllerAnimated:YES completion:^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(SelectMusicDone:)]) {
                [self.delegate SelectMusicDone:choosedMusicPath];
            }
        }];
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return CGSizeMake(_cellWidth, _cellHeight);
    }else {
        if ([_selectedArr[indexPath.section] isEqual:@"0"]) {
            return CGSizeMake(_cellWidth, 0.1);
        }else{
            return CGSizeMake(_cellWidth, 30);
        }
    }
    return CGSizeZero;
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    if (_lastY > 10) {
//        return;
//    }
//    if (_lastY < 1) {
//        self.musicCollectionView.alwaysBounceVertical = NO;
//    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.musicCollectionView) {
        _lastY = scrollView.contentOffset.y;
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.musicCollectionView) {
        _lastY = scrollView.contentOffset.y;
    }
    
}


- (BOOL)panBack:(UIGestureRecognizer *)gestureRecognizer {
    int location_X = 100;
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        UIPanGestureRecognizer *pan = (UIPanGestureRecognizer *)gestureRecognizer;
        CGPoint point = [pan translationInView:self.view];
        UIGestureRecognizerState state = gestureRecognizer.state;
        if (UIGestureRecognizerStateBegan == state || UIGestureRecognizerStatePossible == state) {
            CGPoint location = [gestureRecognizer locationInView:self.view];
            if (_lastY > 0) {// point.y > 0 && location.y < location_X && self.contentOffset.x <= 0
                return YES;
            }
        }
    }
    return NO;
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (_lastY <= 0) {
        //[self.musicCollectionView setContentOffset:CGPointMake(0, 0)];
        return YES;
        
    }
    return NO;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[self.view class]]) {
        return YES;
    }
    return NO;
//    if ([gestureRecognizer isKindOfClass:[_gesture class]] && _lastY < 0 ){
//        self.musicCollectionView.scrollEnabled = NO;
//        return YES;
//    }else{
//
//        self.musicCollectionView.scrollEnabled = YES;
//        return NO;
//    }

}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (_lastY > 1) {
        return NO;
    }
    return YES;


}

- (void) musicElemViewOnTouched:(UIView*)view
{
    // 先清除所有view的选中状态
    for( int i=0; i<self.elemList.count; i++ ) {
        MusicSelectElemView * e = (MusicSelectElemView*)[self.elemList objectAtIndex:i];
        [e setSelected:false];
    }
    
    MusicSelectElemView * o = (MusicSelectElemView*)view;
    if( o.tag == choosed ) {
        // 选择撤销
        choosed = 0;
    }
    else {
        choosed = (int)o.tag;
        [o setSelected:true];
    }
}

-(void)playMusicWithIndex:(NSInteger )index {
    [self stopMusic];
    NSString * path = musicList[index];
    NSString * choosedMusicPath = [[NSBundle mainBundle] pathForResource:path ofType:@"" inDirectory:@""];
    NSLog(@"choosedMusicPath = %@", choosedMusicPath);
    if( choosedMusicPath.length > 0 ) {
        [self playMusic:choosedMusicPath];
    }
    else {
        [self stopMusic];
    }
    
    
    for (int i = 0; i < _musicSelectedArr.count ; i++) {
        NSString *selected = _musicSelectedArr[i];
        selected = @"0";
        _musicSelectedArr[i] = selected;
    }
    NSString *s =  _musicSelectedArr[index];
    s = @"1";
     _musicSelectedArr[index] = s;
    [self.musicCollectionView reloadData];
}


- (void) playMusic:(NSString*)p
{
    NSURL *fileURL = [NSURL URLWithString:p];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    
    self.audioPlayer.numberOfLoops = -1;
    [self.audioPlayer play];
    
    return;
}

- (void) stopMusic
{
    [self.audioPlayer stop];
    self.audioPlayer = nil;
}

#pragma mark    ======== UIViewControllerTransitioningDelegate ========
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    return [CustomAnimatedTransitioning transitionWithTransitionType:CustomTransitionTypePush andWay:0 andFromeView:nil andToView:nil];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [CustomAnimatedTransitioning transitionWithTransitionType:CustomTransitionTypePop andWay:0 andFromeView:nil andToView:nil];
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator{
    return _interactiveDismiss.interation ? _interactiveDismiss : nil;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id<UIViewControllerAnimatedTransitioning>)animator{
    CustomInteractiveTransition *interactivePresent = [_delegate SelectMusicViewControllerInteractiveTransitionForPresent];
    return interactivePresent.interation ? interactivePresent : nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
