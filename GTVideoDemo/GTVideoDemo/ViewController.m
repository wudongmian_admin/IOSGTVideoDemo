//
//  ViewController.m
//  GTVideoDemo
//
//  Created by gtv on 2018/1/28.
//  Copyright © 2018年 gtv. All rights reserved.
//



#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <GTVideoLib/GTVideoLib.h>
#import "RecordViewController.h"
#import "PreviewViewController.h"
#import "PickCoverController.h"
#import "ComDef.h"
#import "GTVConfig.h"
#include "libimg2webp.h"
#import "UIView+Toast.h"
#import "DraftsViewController.h"


#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface ViewController () <GTVReplayControllerDelegate> {
    GTVRecController * rec;
}

@property (nonatomic, strong) GTVReplayController * mergeCtrl;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    GTVConfig *config = [GTVConfig config];
    
    config.backgroundColor = RGBToColor(35, 42, 66);
    config.timelineBackgroundCollor = RGBToColor(35, 42, 66);
    config.timelineDeleteColor = [UIColor redColor];
    config.timelineTintColor = RGBToColor(239, 75, 129);
    config.durationLabelTextColor = [UIColor redColor];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    int w = rect.size.width;
    int h = rect.size.height;
    
    UIImageView * bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, w, h)];
    [bgView setImage:[UIImage imageNamed:@"home-bg"]];
    [self.view addSubview:bgView];
    
    int recWidth = w/4;
    int recPosY = h-50-recWidth/2;
    
    UIButton * recBtn = [[UIButton alloc] initWithFrame:CGRectMake(w/2-recWidth/2, recPosY-recWidth/2, recWidth, recWidth)];
    [recBtn addTarget:self action:@selector(recBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [recBtn setTitle:@"录制" forState:UIControlStateNormal];
    [recBtn setImage:[UIImage imageNamed:@"home-rec"] forState:UIControlStateNormal];
    [recBtn setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:recBtn];
    
    UIButton * replayBtn = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 80, 40)];
    [replayBtn addTarget:self action:@selector(replayBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [replayBtn setTitle:@"草稿" forState:UIControlStateNormal];
//    [recBtn setImage:[UIImage imageNamed:@"home-rec"] forState:UIControlStateNormal];
    [replayBtn setBackgroundColor:[UIColor orangeColor]];
    [self.view addSubview:replayBtn];
    
    UIButton * mergeBtn = [[UIButton alloc] initWithFrame:CGRectMake(100, 200, 180, 40)];
    [mergeBtn addTarget:self action:@selector(mergeBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [mergeBtn setTitle:@"草稿直接合成" forState:UIControlStateNormal];
    //    [recBtn setImage:[UIImage imageNamed:@"home-rec"] forState:UIControlStateNormal];
    [mergeBtn setBackgroundColor:[UIColor orangeColor]];
    [self.view addSubview:mergeBtn];
    
    UIButton * coverBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, recPosY-recWidth/2, recWidth, recWidth)];
    [coverBtn addTarget:self action:@selector(coverBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [coverBtn setTitle:@"WEBP" forState:UIControlStateNormal];
    [coverBtn setBackgroundColor:[UIColor orangeColor]];
    [self.view addSubview:coverBtn];
    
    
    return;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self checkAndRequestVideoAuthStatus];
}

- (void) notifyReplayStatus:(NSString*)evt withInfo:(NSDictionary*)info
{
    NSLog(@"evt = %@ info = %@ ", evt, info);
}

- (void) mergeBtn_OnClicked:(id)sender {
    
    // 全I帧视频存储目录
    NSString * p = [NSString stringWithFormat:@"%@/abcdefg/merge_normal.mp4", NSTemporaryDirectory()];
    if( [[NSFileManager defaultManager] fileExistsAtPath:p] == false ) {
        [self.view makeToast:@"请先去录制" duration:2.0f position:CSToastPositionCenter];
        return;
    }
    
    // 设定文件存储目录
    NSString * d = [NSString stringWithFormat:@"%@/abcdefg/", NSTemporaryDirectory()];
    
    // 指定草稿箱直接合成
    self.mergeCtrl = [[GTVReplayController alloc] init];
    [self.mergeCtrl setReplayDelegate:self];
    
    [self.mergeCtrl setReplayVideoPath:p];
    [self.mergeCtrl setReplayConfigFolder:d forceInitFlag:false];
    
    // 创建一个隐藏view，用于合成
    UIView * v = [[UIView alloc] initWithFrame:CGRectMake(-10, 10, 1, 1)];
    [self.view addSubview:v];
    
    [self.mergeCtrl setReplayRenderView:v withFrame:CGRectMake(0, 0, 1, 1)];
    
    // 设置logo
    [self.mergeCtrl setMark:[UIImage imageNamed:@"logo.png"] atRect:CGRectMake(10, 10, 50, 50)];
    [self.mergeCtrl setExtraMark:[UIImage imageNamed:@"logo.png"] atRect:CGRectMake(10, 80, 50, 50)];
    
    // 开始执行导出操作
    [self.view makeToastActivity:CSToastPositionCenter];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString * path = [NSString stringWithFormat:@"%@/sample.mp4", NSTemporaryDirectory()];
        [self.mergeCtrl exportReplayFile:path withSize:CGSizeMake(540,960) andBitrate:4000000];
        
        NSString * n = path;
        
        if( [[NSFileManager defaultManager] fileExistsAtPath:n] ) {
            NSLog(@"file ok");
            usleep(300*1000);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(n.length > 0){
                NSURL *outputURL = [NSURL URLWithString:n];
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                    [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (error) {
                                NSLog(@"recordFile failed %@ ", n);
                            } else {
                                NSLog(@"recordFile succeed %@ ", n);
                            }
                            
                            [self.view makeToast:@"视频已经导出到相册" duration:2.0f position:CSToastPositionCenter];
                        });
                    }];
                }
            }
            
            [self.view hideToastActivity];
            [v removeFromSuperview];
        });
    });
}

- (void) coverBtn_OnClicked:(id)sender {
    
    PickCoverController * vc = [[PickCoverController alloc] init];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) replayBtn_OnClicked:(id)sender {
    
    NSFileManager * fileManger = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString * p = [NSString stringWithFormat:@"%@/abcdef/", documentsDir];
    BOOL isDir = NO;
    
    // isDir判断是否为文件夹
    BOOL isExist = [fileManger fileExistsAtPath:p isDirectory:&isDir];
    
    if( !isExist ) {
        [self.view makeToast:@"请先去录制" duration:2.0f position:CSToastPositionCenter];
        return;
    }else{
        
        
        if (isDir) {
            
            /** 列出目录内容，会包含文件夹和文件 */
            NSArray * dirArray = [fileManger contentsOfDirectoryAtPath:p error:nil];
            if (dirArray.count == 0) {
                [self.view makeToast:@"请先去录制" duration:2.0f position:CSToastPositionCenter];
                return;
            }
            NSString * subPath = nil;
            
            NSMutableArray *cellImageArr = [NSMutableArray array];
            for (NSString * str in dirArray) {
                
                subPath  = [p stringByAppendingPathComponent:str];
                
                BOOL issubDir = NO;
                
                [fileManger fileExistsAtPath:subPath isDirectory:&issubDir];
                
                NSLog(@"subPath --  %@",subPath);
                
                NSMutableArray *imageArr = [NSMutableArray array];
                for (int i = 0 ; i < 7; i++) {
                    NSString *imageStr = [NSString stringWithFormat:@"%@/headerImage%d.png",subPath,i];
                    if ([fileManger fileExistsAtPath:imageStr isDirectory:&issubDir]) {
                        UIImage *image = [UIImage imageWithContentsOfFile:imageStr];
                        if (image) {
                            [imageArr addObject:image];
                        }
                        
                    }
                }
                [cellImageArr addObject:imageArr];
                
            }
            
            DraftsViewController *vc = [DraftsViewController new];
            vc.videoFilesArr = dirArray;
            vc.cellImageArr = cellImageArr;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            NSLog(@"%@",p);
        }
        
    }

}

-(void) removeFile:(NSString*) path{
    
    BOOL isDir;
    
    [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];
    
    if (isDir) {
        NSArray* files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
        for (NSString* file in files) {
            [self removeFile:[path stringByAppendingPathComponent:file]];
        }
    }
    else{
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
}

- (void) recBtn_OnClicked:(id)sender {
    
    // 删除旧的录制文件，正式APP需要做检测区分是否需要删除
//    NSString * tmpDir = [NSString stringWithFormat:@"%@", NSTemporaryDirectory()];
//    [self removeFile:tmpDir];
    
    RecordViewController * vc = [[RecordViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) finishCheckAndRequestVideoAuthStatus
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    // retry for video auth
    if( status != AVAuthorizationStatusAuthorized ) {
        
        [self performSelectorOnMainThread:@selector(checkAndRequestVideoAuthStatus) withObject:nil waitUntilDone:false];
        
        return;
    }
    
    // 尝试获取声音
    [self performSelectorOnMainThread:@selector(checkAndRequestAudioAuthStatus) withObject:nil waitUntilDone:false];
    
    return;
}

-(void) checkAndRequestVideoAuthStatus
{
    BOOL needRequest = false;
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    switch (status)
    {
        case AVAuthorizationStatusAuthorized:
        {
            needRequest = false;
            break;
        }
        case AVAuthorizationStatusNotDetermined:
        case AVAuthorizationStatusRestricted:
        case AVAuthorizationStatusDenied:
        default:
        {
            needRequest = true;
            break;
        }
    }
    
    if( needRequest == true ) {
        
        __weak typeof(self) _self = self;
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [_self finishCheckAndRequestVideoAuthStatus];
             });
         }];
    }
    else {
        
        [self finishCheckAndRequestVideoAuthStatus];
    }
    
    return;
}

-(void) finishCheckAndRequestAudioAuthStatus
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    
    // retry for audio auth
    if( status != AVAuthorizationStatusAuthorized ) {
        
        [self performSelectorOnMainThread:@selector(checkAndRequestAudioAuthStatus) withObject:nil waitUntilDone:false];
        
        return;
    }
    
    return;
}

-(void) checkAndRequestAudioAuthStatus
{
    BOOL needRequest = false;
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    
    switch (status)
    {
        case AVAuthorizationStatusAuthorized:
        {
            needRequest = false;
            break;
        }
        case AVAuthorizationStatusNotDetermined:
        case AVAuthorizationStatusRestricted:
        case AVAuthorizationStatusDenied:
        default:
        {
            needRequest = true;
            break;
        }
    }
    
    if( needRequest == true ) {
        
        __weak typeof(self) _self = self;
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [_self finishCheckAndRequestAudioAuthStatus];
             });
         }];
    }
    else {
        
        [self finishCheckAndRequestAudioAuthStatus];
    }
    
    return;
}




@end
