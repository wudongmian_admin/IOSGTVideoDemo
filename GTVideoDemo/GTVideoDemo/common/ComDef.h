//
//  ComDef.h
//  GTVideoDemo
//
//  Created by gtv on 2018/1/28.
//  Copyright © 2018年 gtv. All rights reserved.
//

#ifndef ComDef_h
#define ComDef_h

#define RGBToColor(R,G,B)  [UIColor colorWithRed:(R * 1.0) / 255.0 green:(G * 1.0) / 255.0 blue:(B * 1.0) / 255.0 alpha:1.0]
#define rgba(R,G,B,A)  [UIColor colorWithRed:(R * 1.0) / 255.0 green:(G * 1.0) / 255.0 blue:(B * 1.0) / 255.0 alpha:A]

#endif /* ComDef_h */
