//
//  CountDownView.h
//  gtv
//
//  Created by gtv on 2017/11/4.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountDownViewDelegate <NSObject>

- (void) finishCountDown:(id)view;

@end

@interface CountDownView : UIView

@property (nonatomic, strong) NSString * userInfo;
@property (nonatomic, assign) int seconds;
@property (nonatomic, weak) id<CountDownViewDelegate> finishDelegate;

- (void) startCountDown:(int)sec;

@end
