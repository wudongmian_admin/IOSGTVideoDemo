//
//  CountDownView.m
//  gtv
//
//  Created by gtv on 2017/11/4.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import "CountDownView.h"

@interface CountDownView()

@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, strong) UILabel * lbl;

@end

@implementation CountDownView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) dealloc
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void) startCountDown:(int)sec
{
    self.seconds = sec;
    
    self.timer = [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(tik:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    
    CGRect r = CGRectMake(width/2-50, height/2-50, 100, 100);
    
    UIImageView * bg = [[UIImageView alloc] initWithFrame:r];
    [bg setImage:[UIImage imageNamed:@"record_btn_normal"]];
    [self addSubview:bg];
    
    self.lbl = [[UILabel alloc] initWithFrame:r];
    [self.lbl setText:[NSString stringWithFormat:@"%d", sec]];
    [self.lbl setFont:[UIFont systemFontOfSize:40.f]];
    [self.lbl setTextColor:[UIColor blackColor]];
    [self.lbl setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.lbl];
    
    return;
}

- (void) tik:(NSTimer*)t
{
    if( self.timer != t )
        return;
    
    self.seconds --;
    
    NSString * s = [NSString stringWithFormat:@"%d", self.seconds];
    [self.lbl setText:s];
    
    if( self.seconds <= 0 ) {
        if(self.finishDelegate && [self.finishDelegate respondsToSelector:@selector(finishCountDown:)]) {
            [self.finishDelegate finishCountDown:self];
        }
        [self.timer invalidate];
        self.timer = nil;
    }
    
    return;
}

@end
