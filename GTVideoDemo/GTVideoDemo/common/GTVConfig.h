//
//  GTVConfig.h
//  GTVideoDemo
//
//  Created by gtv on 2018/1/28.
//  Copyright © 2018年 gtv. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface GTVConfig : NSObject

@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) UIColor *timelineTintColor;

@property (nonatomic, strong) UIColor *timelineBackgroundCollor;

@property (nonatomic, strong) UIColor *timelineDeleteColor;

@property (nonatomic, strong) UIColor *durationLabelTextColor;

@property (nonatomic, strong) UIColor *cutBottomLineColor;

@property (nonatomic, strong) UIColor *cutTopLineColor;

+ (GTVConfig *)config;

@end
