//
//  GTVConfig.m
//  GTVideoDemo
//
//  Created by gtv on 2018/1/28.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import "GTVConfig.h"

static GTVConfig *uiConfig;

@implementation GTVConfig

+ (GTVConfig *)config
{
    if( uiConfig == nil ) {
        uiConfig = [[GTVConfig alloc] init];
    }
    
    return uiConfig;
}

@end
