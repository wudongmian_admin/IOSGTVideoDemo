//
//  PickCoverController.m
//  GTVideoDemo
//
//  Created by gtv on 2018/3/25.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import "PickCoverController.h"
#import "GTVideoLib/GTVideoLib.h"

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "GTVConfig.h"
#import "MediaControlView.h"
#import "DPHorizontalScrollView.h"
#import "UIView+Toast.h"
#include "libimg2webp.h"

#define SCROLL_VIEW_HEIGHT  60

@interface PickCoverController () <DPHorizontalScrollViewDelegate>

@property (nonatomic, strong) UIView * replayView;
@property (nonatomic, strong) UIImageView * animView;

@property (nonatomic, strong) UIButton * backBtn;
@property (nonatomic, strong) UIButton * pickBtn;

@property (nonatomic, strong) DPHorizontalScrollView * scrollView;
@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, strong) UIView * selectedMarkView;    // 标记选中状态的view

@property (nonatomic, strong) NSArray * thumbList;
@property (nonatomic, strong) NSString * moviePath;

@end

@implementation PickCoverController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGRect rect = [UIScreen mainScreen].bounds;
    {
        int width = rect.size.width;
        int height = width*4/3;
        int x = 0;
        int y = (rect.size.height-height) / 2;
        
        self.replayView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [self.replayView setBackgroundColor:[UIColor blackColor]];
        
        self.animView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [self.animView setBackgroundColor:[UIColor blackColor]];
        [self.replayView addSubview:self.animView];
    }
    [self.view addSubview:self.replayView];
    
    self.view.backgroundColor = [GTVConfig config].backgroundColor;
    
    self.backBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.backBtn.frame = CGRectMake(0, 0, 66, 66);;
    [self.backBtn setTitle:@"返回" forState:(UIControlStateNormal)];//设置封面
    [self.backBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.backBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.backBtn addTarget:self action:@selector(backBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.backBtn];
    
    self.pickBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.pickBtn.frame = CGRectMake(rect.size.width-66, 0, 66, 66);
    [self.pickBtn setTitle:@"抽取webp" forState:(UIControlStateNormal)];
    [self.pickBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.pickBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.pickBtn addTarget:self action:@selector(pickBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.pickBtn];
    
    if( self.selectedMarkView == nil ) {
        
        self.selectedMarkView = [[UIView alloc] initWithFrame:CGRectMake(2, 2, 56, 56)];
        self.selectedMarkView.layer.borderColor = [UIColor redColor].CGColor;
        self.selectedMarkView.layer.borderWidth = 2.0f;
    }
    
    self.moviePath = [[NSBundle mainBundle] pathForResource:@"test.mp4" ofType:nil];
    
    return;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    CGRect mediaFrame = self.replayView.frame;
    float top = mediaFrame.origin.y + mediaFrame.size.height;
    top = top + (rect.size.height - top - SCROLL_VIEW_HEIGHT) / 2;
    
    self.scrollView = [[DPHorizontalScrollView alloc] initWithFrame:CGRectMake(0, top, rect.size.width, SCROLL_VIEW_HEIGHT)];
    [self.view addSubview:self.scrollView];
    
    self.scrollView.scrollViewDelegate = self;
    self.selectedIndex = 0;
    
    [self loadImages];
    
    return;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    self.scrollView.scrollViewDelegate = nil;
    [self.scrollView removeFromSuperview];
    self.scrollView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadImages
{
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_pic_grab_one", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }

    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString * m = self.moviePath;
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        // 4000是获取到视频的时长
        NSRange r = NSMakeRange(0, 4000);
        NSLog(@"syncGrabAndScaleJPEG start");
        NSArray * res = [GTVideoTool syncGrabAndScaleJPEG:m toFolder:rootfolder withinRange:r andNumLimit:10 inScaleFactor:0.2];
        NSLog(@"syncGrabAndScaleJPEG end");
        //NSArray * res = [GTVideoTool syncGrabJPEG:m toFolder:rootfolder withinRange:r andNumLimit:10];
        self.thumbList = res;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view hideToastActivity];
            
            self.selectedIndex = 0;
            [self didSelectViewAtIndex:self.selectedIndex];
        });
    });
}

- (void) backBtn_OnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:false];
    
    return;
}

- (void) pickBtn_OnClicked:(id)sender
{
    if( self.selectedIndex < 0 || self.selectedIndex >= self.thumbList.count ) {
        [self.view makeToast:@"未选择" duration:1.5f position:CSToastPositionCenter];
        return;
    }
    
    NSDictionary * elem = [self.thumbList objectAtIndex:self.selectedIndex];
    int ts = ((NSString*)[elem objectForKey:@"timestamp"]).intValue;
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    // 抽取动画帧
    NSString * m = self.moviePath;
    
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_pic_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(ts, 1000);
        NSArray * res = [GTVideoTool syncGrabRawYUV:m toFolder:rootfolder withinRange:r andNumLimit:5];
        
        // 生成webp
        NSString * outwebp = [NSString stringWithFormat:@"%@/test.webp", NSTemporaryDirectory()];
        char * inputlist[10];
        for( int i=0; i<res.count; i++ ) {
            NSDictionary * d = (NSDictionary*)[res objectAtIndex:i];
            NSString * filepath = [d objectForKey:@"filepath"];
            inputlist[i] = (char *)[filepath cStringUsingEncoding:NSUTF8StringEncoding];
        }
        char * output_file = (char*)[outwebp cStringUsingEncoding:NSUTF8StringEncoding];
        img2webp(inputlist, 5, output_file);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view hideToastActivity];
            
            [self.view makeToast:@"test.webp已生成至temp目录，请从xcode下查看" duration:2.0f position:CSToastPositionCenter];
        });
    });
    
    return;
}

- (NSInteger)numberOfColumnsInTableView:(DPHorizontalScrollView *)tableView
{
    return self.thumbList.count;
}

- (UIView *)tableView:(DPHorizontalScrollView *)tableView viewForColumnAtIndex:(NSInteger)index
{
    UIView * view = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCROLL_VIEW_HEIGHT, SCROLL_VIEW_HEIGHT)];
    UIImageView * icon = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, SCROLL_VIEW_HEIGHT-6, SCROLL_VIEW_HEIGHT-6)];
    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(3, 3, SCROLL_VIEW_HEIGHT-6, SCROLL_VIEW_HEIGHT-6)];
    
    self.selectedMarkView.frame = CGRectMake(2, 2, SCROLL_VIEW_HEIGHT-4, SCROLL_VIEW_HEIGHT-4);
    
    NSDictionary * elem = nil;
    if( index < self.thumbList.count ) {
        
        elem = (NSDictionary*)[self.thumbList objectAtIndex:index];
        NSString * fpath = [elem objectForKey:@"filepath"];
        NSData * d = [NSData dataWithContentsOfFile:fpath];
        UIImage * img = [UIImage imageWithData:d];
        [icon setImage:img];
        [view addSubview:icon];
        
        NSString * tstr = [elem objectForKey:@"timestamp"];
        [lbl setText:[NSString stringWithFormat:@"%@ms", tstr]];
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setFont:[UIFont systemFontOfSize:8.0f]];
        [lbl setTextAlignment:NSTextAlignmentCenter];
        [view addSubview:lbl];
    }
    
    if( self.selectedIndex == index ) {
        [self.selectedMarkView removeFromSuperview];
        [view addSubview:self.selectedMarkView];
    }
    
    return view;
}

/**
 *  每行view的宽度
 */
- (CGFloat)tableView:(DPHorizontalScrollView *)tableView widthForColumnAtIndex:(NSInteger)index
{
    return 60.0f;
}

- (void)didSelectViewAtIndex:(NSInteger)index
{
    self.selectedIndex = (int)index;
    
    [self.scrollView reloadData];
    
    NSDictionary * d = [self.thumbList objectAtIndex:index];
    int ts = ((NSString*)[d objectForKey:@"timestamp"]).intValue;
    
    [self.animView stopAnimating];
    [self.view makeToastActivity:CSToastPositionCenter];
    
    // 抽取动画帧
    NSString * m = self.moviePath;
    
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_yuv_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    [self.scrollView setUserInteractionEnabled:false];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(ts, 1000);
        NSLog(@"syncGrabJPEG start");
        NSArray * res = [GTVideoTool syncGrabJPEG:m toFolder:rootfolder withinRange:r andNumLimit:10];
        NSLog(@"syncGrabJPEG end");
        
        NSDictionary * elem = nil;
        NSMutableArray * l = [[NSMutableArray alloc] init];
        for( int i=0; i<res.count; i++ ) {
            
            elem = [res objectAtIndex:i];
            NSString * fpath = ((NSString*)[elem objectForKey:@"filepath"]);
            NSData * d = [NSData dataWithContentsOfFile:fpath];
            UIImage * img = [UIImage imageWithData:d];
            [l addObject:img];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.scrollView setUserInteractionEnabled:true];
            
            [self.view hideToastActivity];
            
            self.animView.animationImages = l;
            self.animView.animationDuration = 0.8f;
            self.animView.animationRepeatCount = 100;
            
            [self.animView startAnimating];//开始动画
        });
    });
}

@end
