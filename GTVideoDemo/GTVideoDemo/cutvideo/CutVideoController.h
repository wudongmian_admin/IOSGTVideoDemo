//
//  CutVideoController.h
//  GTVideoDemo
//
//  Created by gtv on 2018/4/9.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CutVideoControllerDelegate <NSObject>

- (void) cutVideoDoneWithRange:(NSRange)r;
- (void) cutVideoCancel;
@end

@interface CutVideoController : UIViewController<UINavigationControllerDelegate>

@property (nonatomic, strong) NSString * vPath;
@property (nonatomic, weak) id<CutVideoControllerDelegate> delegate;
@property (nonatomic, strong) UIView    *fromeView;
@property (nonatomic, assign)CGFloat    videoTotalTime;

@end
