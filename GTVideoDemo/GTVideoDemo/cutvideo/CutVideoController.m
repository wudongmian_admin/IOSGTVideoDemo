//
//  CutVideoController.m
//  GTVideoDemo
//
//  Created by gtv on 2018/4/9.
//  Copyright © 2018年 gtv. All rights reserved.
//
#import "GTVideoLib/GTVideoLib.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ClipFramework/ClipFramework.h>

#import "CutVideoController.h"
#import "UIView+Toast.h"

#import "MediaControlView.h"



#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

@interface CutVideoController () <GTVReplayControllerDelegate>

/**
 剪辑视频View
 */
@property (nonatomic, strong) UIView  *clipView;
/**
 剪切视频的滑动View
 */
@property (nonatomic, strong) ClipVideoView * clipVideoView;

@property (nonatomic, strong) UIView * replayView;
@property (nonatomic, strong) GTVReplayController * replayCtrl;

@property (nonatomic, strong) MediaControlView * mediaControl;
@property (nonatomic, strong) UIView * mediaContainer;

@property (nonatomic, strong) NSArray * thumbList;
@property (nonatomic, strong) CustomInteractiveTransition *interactiveTransition;

@end

@implementation CutVideoController

- (void) notifyReplayStatus:(NSString*)evt withInfo:(NSDictionary*)info
{
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGRect rect = [UIScreen mainScreen].bounds;
    {
        int width = rect.size.width-88;
        int height = width * 16 / 9;
        int x = 44;
        int y = 0;
        
        BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
        if (IphoneX) {
            y = 44;
        }
        self.mediaContainer = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [self.mediaContainer setBackgroundColor:[UIColor blackColor]];
        
        self.replayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [self.mediaContainer addSubview:self.replayView];
        
        self.mediaControl = [[MediaControlView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [self.mediaContainer addSubview:self.mediaControl];
    }
    [self.view addSubview:self.mediaContainer];
    
    //CGRectMake(0, SCREEN_HEIGHT - 150 , SCREEN_WIDTH, 150);
    self.clipView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 150 , SCREEN_WIDTH, 150)];
    self.clipView.backgroundColor = [UIColor blackColor];
    
    self.clipVideoView = [[ClipVideoView alloc]initWithFrame:CGRectMake(0, 60, SCREEN_WIDTH, 70)];
    //self.clipVideoView.totalTime = _totalTime;
    [self.clipVideoView initSubViewsWithNumber:10];
    [self.clipView addSubview:self.clipVideoView];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 + 40, 5, 80, 35)];
    btn.layer.cornerRadius = 10;
    [btn setTitle:@"剪辑" forState:UIControlStateNormal];
    [btn setTitle:@"剪辑" forState:UIControlStateSelected];
    [btn addTarget:self action:@selector(doneClipButton:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = 10;
    
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 120, 5, 80, 35)];
    btn1.backgroundColor = [UIColor redColor];
    btn1.layer.cornerRadius = 10;
    [btn1 setTitle:@"不剪辑" forState:UIControlStateNormal];
    [btn1 setTitle:@"不剪辑" forState:UIControlStateSelected];
    [btn1 addTarget:self action:@selector(cancelClipButton:) forControlEvents:UIControlEventTouchUpInside];
    btn1.tag = 11;
    
    [self.clipView addSubview:btn];
    [self.clipView addSubview:btn1];
    
    [self.view addSubview:self.clipView];
    
    self.replayCtrl = [[GTVReplayController alloc] init];
    [self.replayCtrl setReplayDelegate:self];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    //初始化手势过渡的代理
    self.interactiveTransition = [CustomInteractiveTransition interactiveTransitionWithTransitionType:CustomInteractiveTransitionTypePop GestureDirection:CustomInteractiveTransitionGestureDirectionRight];
    return;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    int width = rect.size.width-88;
    int height = width * 16 / 9;

    [self.replayCtrl setReplayRenderView:self.replayView withFrame:CGRectMake(0, 0, width, height)];
    [self.replayCtrl setReplayVideoPath:self.vPath];
    [self.replayCtrl setRepeatCount:999999];//无限循环
    
    // 开始播放后立刻暂停
    [self.replayCtrl playVideo];
    [self.replayCtrl pauseVideo];
    
    // 播放控制权交给控件
    [self.mediaControl startWithPlayer:self.replayCtrl];
    
    [self loadImages];
    
    return;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    // 开启返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    [self.mediaControl stopPlayer];
    [self.replayCtrl stopVideo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat) getVideoDuration:(NSString *)path
{
    AVURLAsset * asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:path]];
    CMTime  time = [asset duration];
    
    CGFloat seconds = ceil(time.value/time.timescale);
    
    return seconds;
}

- (void) loadImages
{
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_pic_cut_video", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    CGFloat sec = [self getVideoDuration:self.vPath];
//    [self.clipVideoView loadVideoTime:(CGFloat)sec];
    [self.clipVideoView loadVideoTime:self.videoTotalTime];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(0, sec * 1000);
        NSArray * res = [GTVideoTool syncGrabAndScaleJPEG:_vPath toFolder:rootfolder withinRange:r andNumLimit:10 inScaleFactor:0.3];
        self.thumbList = res;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            for (int i = 0 ; i < res.count; i++) {
                
                NSDictionary * elem = nil;
                elem = (NSDictionary*)[res objectAtIndex:i];
                
                NSString * fpath = [elem objectForKey:@"filepath"];
                NSData * d = [NSData dataWithContentsOfFile:fpath];
                UIImage * img = [UIImage imageWithData:d];
                
                [self.clipVideoView clipVideoViewAddImage:img withIndex:i];
            }
            
            [self.view hideToastActivity];
        });
    });
}

- (void) doneClipButton:(id)sender
{
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(cutVideoDoneWithRange:)] ) {
        NSLog(@"min:%f max:%f ", self.clipVideoView.min, self.clipVideoView.max);
        NSRange r = NSMakeRange((int)self.clipVideoView.min, (int)(self.clipVideoView.max-self.clipVideoView.min));
        [self.delegate cutVideoDoneWithRange:r];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    return;
}

- (void) cancelClipButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    if ([self.delegate respondsToSelector:@selector(cutVideoCancel)]) {
        [self.delegate cutVideoCancel];
    }
    
    
    return;
}

#pragma mark    ======== UINavigationControllerDelegate ========

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC{
    
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    int width = rect.size.width-88;
    int height = width * 16 / 9;
    int x = 44;
    int y = 0;
    
    
    BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
    if (IphoneX) {
        y = 44;
    }
    UIView *toView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    
    
    
    return [CustomAnimatedTransitioning transitionWithTransitionType:operation == UINavigationControllerOperationPush ? CustomTransitionTypePush : CustomTransitionTypePop andWay:1 andFromeView:self.fromeView andToView:toView];
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController{
    return _interactiveTransition.interation ? _interactiveTransition : nil;
}

@end
