//
//  CutVideoController.h
//  GTVideoDemo
//
//  Created by gtv on 2018/4/9.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EffectVideoControllerDelegate <NSObject>

- (void) effectVideoDoneWithList:(NSArray*)list;

@end

@interface EffectVideoController : UIViewController<UINavigationControllerDelegate>

@property (nonatomic, strong) NSString * vPath;
@property (nonatomic, weak) id<EffectVideoControllerDelegate> delegate;
@property (nonatomic, strong) UIView    *fromeView;
@end
