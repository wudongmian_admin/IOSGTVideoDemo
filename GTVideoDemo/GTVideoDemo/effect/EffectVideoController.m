//
//  CutVideoController.m
//  GTVideoDemo
//
//  Created by gtv on 2018/4/9.
//  Copyright © 2018年 gtv. All rights reserved.
//
#import "GTVideoLib/GTVideoLib.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "EffectVideoController.h"
#import "UIView+Toast.h"
#import "GTVConfig.h"

#import "MediaControlView.h"
#import "DPHorizontalScrollView.h"

#import <ClipFramework/ClipFramework.h>


#define SCROLL_VIEW_HEIGHT  60

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

@interface EffectVideoController () <GTVReplayControllerDelegate, DPHorizontalScrollViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView * replayView;
@property (nonatomic, strong) GTVReplayController * replayCtrl;

@property (nonatomic, strong) MediaControlView * mediaControl;
@property (nonatomic, strong) UIView * mediaContainer;

@property (nonatomic, strong) UIButton * cancelBtn;
@property (nonatomic, strong) UIButton * doneBtn;

@property (nonatomic, strong) DPHorizontalScrollView * scrollView;
@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, strong) UIView * selectedMarkView;    // 标记选中状态的view

@property (nonatomic, strong) UIButton * testBtn;
@property (nonatomic, strong) UIButton * testBtn1;
@property (nonatomic, strong) UIButton * testBtn2;
@property (nonatomic, strong) NSTimer * printTimer;

@property (nonatomic, strong)FilterFrameView *filterFrameView;
@property (nonatomic, strong) CustomInteractiveTransition *interactiveTransition;
@end

@implementation EffectVideoController
{
    BOOL     _newFilterView;
    NSMutableArray *_colorArr;
}
- (void) notifyReplayStatus:(NSString*)evt withInfo:(NSDictionary*)info
{
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _newFilterView = YES;
    
    UIColor *c1 = [UIColor clearColor];
    UIColor *c2 = [UIColor orangeColor];
    UIColor *c3 = [UIColor yellowColor];
    UIColor *c4 = [UIColor greenColor];
    UIColor *c5 = [UIColor cyanColor];
    UIColor *c6 = [UIColor blueColor];
    UIColor *c7 = [UIColor purpleColor];
    UIColor *c8 = [UIColor blackColor];
    UIColor *c9 = [UIColor magentaColor];
    UIColor *c10 = [UIColor brownColor];
    UIColor *c11 = [UIColor lightGrayColor];
    UIColor *c12 = [UIColor redColor];
    _colorArr = [NSMutableArray arrayWithObjects:c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12, nil];
    // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    self.filterFrameView = [[FilterFrameView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - SCROLL_VIEW_HEIGHT - 20 - 90, self.view.frame.size.width, 60)];
    [self.filterFrameView initSubViewsWithNum:12];
    __weak typeof(self) weakSelf = self;
    self.filterFrameView.DraggingBlock = ^(double nowTime) {
        
        [weakSelf.replayCtrl seekVideoTo:nowTime * 1000];
       
    };
    
//    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - SCROLL_VIEW_HEIGHT - 20 - 30, 100, 30)];
//    [button setTitle:@"test" forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(test1) forControlEvents:UIControlEventTouchUpInside];
//    self.testBtn1 = button;
    
    UIButton *button1 = [[UIButton alloc]initWithFrame:CGRectMake(150, self.view.frame.size.height - SCROLL_VIEW_HEIGHT - 20 - 30, 100, 30)];
    [button1 setTitle:@"测试删除" forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(test2) forControlEvents:UIControlEventTouchUpInside];
    self.testBtn2 = button1;
    
    
    CGRect rect = [UIScreen mainScreen].bounds;
    {
//        int width = rect.size.width;
//        int height = rect.size.height-150;//width*4/3;
//        int x = 0;
//        int y = 0;//(rect.size.height-height) / 2;
        
        //44, 0, self.view.frame.size.width-88, (self.view.frame.size.width-88) * 16 / 9)
        int width = rect.size.width-88;
        int height = width * 16 / 9;
        int x = 44;
        int y = 0;
        
        BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
        if (IphoneX) {
            y = 44;
        }
        self.mediaContainer = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        [self.mediaContainer setBackgroundColor:[UIColor blackColor]];
        
        self.replayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [self.mediaContainer addSubview:self.replayView];
        
        self.mediaControl = [[MediaControlView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        __weak typeof(self)weakSelf = self;
        self.mediaControl.timeBlock = ^(CGFloat totalTime) {
            weakSelf.filterFrameView.totalTime = totalTime;
            [weakSelf loadImages];
        };
        self.mediaControl.currentTimeBlock = ^(CGFloat currentTime) {
            [weakSelf.filterFrameView setVideoNowTime:currentTime];
            
        };
        self.mediaControl.videoPausedBlock = ^{
            weakSelf.filterFrameView.hasFilterView = NO;
        };
        [self.mediaContainer addSubview:self.mediaControl];
    }
    [self.view addSubview:self.mediaContainer];
    
    self.view.backgroundColor = [GTVConfig config].backgroundColor;
    
    self.cancelBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.cancelBtn.frame = CGRectMake(0, 0, 66, 66);;
    [self.cancelBtn setTitle:@"取消" forState:(UIControlStateNormal)];//设置封面
    [self.cancelBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.cancelBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.cancelBtn addTarget:self action:@selector(cancelBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.cancelBtn];
    
    self.testBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.testBtn.frame = CGRectMake(rect.size.width/2-33, 0, 66, 66);
    [self.testBtn setTitle:@"开始特效" forState:(UIControlStateNormal)];
    [self.testBtn setTitle:@"结束特效" forState:(UIControlStateSelected)];
    [self.testBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.testBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.testBtn addTarget:self action:@selector(testBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.testBtn];

    self.doneBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.doneBtn.frame = CGRectMake(rect.size.width-66, 0, 66, 66);
    [self.doneBtn setTitle:@"保存" forState:(UIControlStateNormal)];
    [self.doneBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.doneBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.doneBtn addTarget:self action:@selector(doneBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.doneBtn];
    
    if( self.selectedMarkView == nil ) {
        
        self.selectedMarkView = [[UIView alloc] initWithFrame:CGRectMake(2, 2, 56, 56)];
        self.selectedMarkView.layer.borderColor = [UIColor redColor].CGColor;
        self.selectedMarkView.layer.borderWidth = 2.0f;
    }
    
    self.replayCtrl = [[GTVReplayController alloc] init];
    [self.replayCtrl setReplayDelegate:self];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    
    //初始化手势过渡的代理
    self.interactiveTransition = [CustomInteractiveTransition interactiveTransitionWithTransitionType:CustomInteractiveTransitionTypePop GestureDirection:CustomInteractiveTransitionGestureDirectionRight];
    
    return;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    CGRect mediaFrame = self.mediaContainer.frame;
    float top = mediaFrame.origin.y + mediaFrame.size.height;
    top = top + (rect.size.height - top - SCROLL_VIEW_HEIGHT) / 2;
    
    self.scrollView = [[DPHorizontalScrollView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - SCROLL_VIEW_HEIGHT - 20, rect.size.width, SCROLL_VIEW_HEIGHT)];
    [self.view addSubview:self.scrollView];
    
    self.scrollView.scrollViewDelegate = self;
    self.selectedIndex = 0;
    
    [self.scrollView reloadData];
    
    [self.replayCtrl setReplayRenderView:self.replayView withFrame:CGRectMake(0, 0, rect.size.width- 88, (rect.size.width- 88)* 16 / 9)];
    [self.replayCtrl setReplayVideoPath:self.vPath];
    [self.replayCtrl setRepeatCount:999999];//无限循环
    
    // 开始播放后立刻暂停
    [self.replayCtrl playVideo];
    [self.replayCtrl resumeVideo];
    [self.replayCtrl pauseVideo];
    
    
    
    // 播放控制权交给控件
    [self.mediaControl startWithPlayer:self.replayCtrl];
    
    // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    [self.view addSubview:self.filterFrameView];
    [self.view addSubview:self.testBtn1];
    [self.view addSubview:self.testBtn2];
    //[NSThread sleepForTimeInterval:.3];
    return;
}
-(void)test1 {
    
}
-(void)test2 {
    NSArray *arr = [self.replayCtrl currentEffectList];
    if (arr.count == 0) {
        return;
    }
    NSDictionary *dic = arr.lastObject;
    NSString *startTimestamp = dic[@"startTimestamp"];
    
    [self.replayCtrl seekVideoTo:startTimestamp.intValue];
    [self.filterFrameView deleteFilter];
    [self.replayCtrl removeLastEffectShader];
}
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    self.scrollView.scrollViewDelegate = nil;
    [self.scrollView removeFromSuperview];
    self.scrollView = nil;
    
//    // 开启返回手势
//    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
//        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
//    }
    
    [self.mediaControl stopPlayer];
    [self.replayCtrl stopVideo];
    [self.filterFrameView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (int) getVideoDuration:(NSString *)path
{
    AVURLAsset * asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:path]];
    CMTime  time = [asset duration];
    
    int seconds = ceil(time.value/time.timescale);
    
    return seconds;
}

- (void) cancelBtn_OnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    return;
}

- (void) doneBtn_OnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    NSArray * a = [self.replayCtrl currentEffectList];
    NSLog(@"doneBtn_OnClicked = %@", a);
    [self.delegate performSelector:@selector(effectVideoDoneWithList:) withObject:a];
    
    return;
}

- (void) timeoutForPrint:(NSTimer*)t
{
    if( self.printTimer != t )
        return;
    
    NSArray * a = [self.replayCtrl currentEffectList];
    NSLog(@"a = %@", a);
}

- (void) startPrintTimer
{
    if( self.printTimer != nil ) {
        [self.printTimer invalidate];
        self.printTimer = nil;
    }
    
    self.printTimer = [NSTimer timerWithTimeInterval:0.100f target:self selector:@selector(timeoutForPrint:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.printTimer forMode:NSRunLoopCommonModes];
    
    return;
}

- (void) stopPrintTimer
{
    if( self.printTimer != nil ) {
        [self.printTimer invalidate];
        self.printTimer = nil;
    }
    
    return;
}

- (void) testBtn_OnClicked:(id)sender
{
    if( self.selectedIndex == 0 ) {
        [self.view makeToast:@"请选择特效先" duration:3.0f position:CSToastPositionCenter];
        return;
    }
    
    NSString * name = filterList[self.selectedIndex];
    
    if( self.testBtn.selected == false ) {
        [self.replayCtrl startEffectShader:name];
        [self.replayCtrl resumeVideo];
        [self startPrintTimer];
    }
    else {
        // 必须调用stop，否则滤镜不会被保存
        [self.replayCtrl stopEffectShader:name];
        [self.replayCtrl pauseVideo];
        [self stopPrintTimer];
    }
    
    self.testBtn.selected = !self.testBtn.selected;

    return;
}

/**
 *  总列数
 */
- (NSInteger)numberOfColumnsInTableView:(DPHorizontalScrollView *)tableView
{
    return 12;  // 第一个为Normal
}

/**
 *  每列显示的view
 */
static NSString * picList[] = {
    @"无", @"灵魂", @"抖动", @"异世", @"黑白",
    @"尖锐", @"波动", @"透视", @"闪烁", @"素描",
    @"灵异", @"印象"
};

static NSString * filterList[] = {
    DEF_GTV_EFFECT_NONE, DEF_GTV_EFFECT_LINGHUN, DEF_GTV_EFFECT_DOUDONG, DEF_GTV_EFFECT_YISHI, DEF_GTV_EFFECT_HEIBAI,
    DEF_GTV_EFFECT_JIANRUI, DEF_GTV_EFFECT_BODONG, DEF_GTV_EFFECT_TOUSHI,
    DEF_GTV_EFFECT_SHANSUO, DEF_GTV_EFFECT_SUMIAO,
    DEF_GTV_EFFECT_LINGYI, DEF_GTV_EFFECT_YINXIANG
};
static NSString * usIconList[] = {
    @"us-none", @"us-linghun", @"us-doudong", @"us-yishi", @"us-heibai",
    @"us-jianrui", @"us-bodong", @"us-toushi", @"us-shansuo", @"us-sumiao",
    @"us-lingyi", @"us-yinxiang"
};

- (UIView *)tableView:(DPHorizontalScrollView *)tableView viewForColumnAtIndex:(NSInteger)index
{
    UIView * view = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCROLL_VIEW_HEIGHT, SCROLL_VIEW_HEIGHT)];
    UIImageView * icon = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, SCROLL_VIEW_HEIGHT-6, SCROLL_VIEW_HEIGHT-6)];
    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(3, 3, SCROLL_VIEW_HEIGHT-6, SCROLL_VIEW_HEIGHT-6)];
    
    self.selectedMarkView.frame = CGRectMake(2, 2, SCROLL_VIEW_HEIGHT-4, SCROLL_VIEW_HEIGHT-4);
    
    if( index < sizeof(usIconList)/sizeof(usIconList[0]) ) {
        UIImage * img = [UIImage imageNamed:usIconList[index]];
        [icon setImage:img];
        [view addSubview:icon];
    }
    
    if( index < sizeof(picList)/sizeof(picList[0]) ) {
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setText:picList[index]];
        [lbl setTextAlignment:NSTextAlignmentCenter];
    }
    
    [view addSubview:lbl];
    
    if( self.selectedIndex == index ) {
        [self.selectedMarkView removeFromSuperview];
        [view addSubview:self.selectedMarkView];
    }
    
    return view;
}

/**
 *  每行view的宽度
 */
- (CGFloat)tableView:(DPHorizontalScrollView *)tableView widthForColumnAtIndex:(NSInteger)index
{
    return 60.0f;
}

/**
 *  点击每列回调
 */
- (void)didSelectViewAtIndex:(NSInteger)index
{
    self.selectedIndex = (int)index;
    
    [self.scrollView reloadData];
}
/**
 *  长按每列回调
 */
- (void)didSelectLongViewAtIndex:(NSInteger)index{
 //   if (_newFilterView == YES) {
        [self.replayCtrl setRepeatCount:0];
        self.mediaControl.hasFilterView = YES;
        
        UIColor *c = _colorArr[index];
        [self.filterFrameView setNewFilterViewWithColor:(__bridge CGColorRef)(c)];
        self.filterFrameView.hasFilterView = YES;
     //   [self performSelector:@selector(setHasFilterView) withObject:nil afterDelay:.1];
       // _newFilterView = NO;
        
        
        self.selectedIndex = (int)index;
        
        [self.scrollView reloadData];
        NSLog(@"filterList[index]  -------  %@   ----  %ld",filterList[index],index);
        [self.replayCtrl startEffectShader:filterList[index]];
        [self.replayCtrl resumeVideo];
        //[self startPrintTimer];
//    }else{
        
//        self.filterFrameView.hasFilterView = YES;
        //[self.filterFrameView setFilterView:YES andIsNew:NO];
//    }
    
    
    
}
-(void)setHasFilterView {
    self.filterFrameView.hasFilterView = YES;
}
/**
 *  长按结束每列回调
 */
- (void)didLongStopAtIndex:(NSInteger)index{
    
    self.mediaControl.hasFilterView = NO;
   // _newFilterView = YES;
    [self.replayCtrl stopEffectShader:filterList[index]];
    [self.replayCtrl pauseVideo];
    [self stopPrintTimer];
    [self.replayCtrl setRepeatCount:999];
   
}


- (void) loadImages
{
    NSTimeInterval duration = [self.replayCtrl getDuration] * 1.0f / 1000.0f;
    NSInteger intDuration = duration + 0.5;

    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_pic_grab_three", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        
        
        //
        NSRange r = NSMakeRange(0, intDuration * 1000);
        NSArray * res = [GTVideoTool syncGrabJPEG:_vPath toFolder:rootfolder withinRange:r andNumLimit:12];

        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (int i = 0 ; i < res.count; i++) {
                NSDictionary * elem = nil;
                elem = (NSDictionary*)[res objectAtIndex:i];
                //     UIImageView *v = [self.view viewWithTag:301 + i];
                
                NSString * fpath = [elem objectForKey:@"filepath"];
                NSData * d = [NSData dataWithContentsOfFile:fpath];
                UIImage * img = [UIImage imageWithData:d];
                
                
                [self.filterFrameView loadImages:img withIndex:i];
               
                
            }
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view hideToastActivity];
            
        });
    });
}

#pragma mark    ======== UINavigationControllerDelegate ========

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC{

    
        CGRect rect = [UIScreen mainScreen].bounds;
    
        int width = rect.size.width-88;
        int height = width * 16 / 9;
        int x = 44;
        int y = 0;
    
        BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
        if (IphoneX) {
            y = 44;
        }
    
        UIView *toView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    

    
    return [CustomAnimatedTransitioning transitionWithTransitionType:operation == UINavigationControllerOperationPush ? CustomTransitionTypePush : CustomTransitionTypePop andWay:1 andFromeView:self.fromeView andToView:toView];
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController{
    return _interactiveTransition.interation ? _interactiveTransition : nil;
}
@end
