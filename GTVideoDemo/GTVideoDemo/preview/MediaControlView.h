//
//  MediaControlView.h
//  NewTestSDK
//
//  Created by gtv on 2017/11/10.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTVideoLib/GTVideoLib.h"
#import "PreviewViewController.h"
#import "EffectVideoController.h"

@interface MediaControlView : UIView

- (void) startWithPlayer:(GTVReplayController*)player;
- (void) stopPlayer;
@property (nonatomic, assign)BOOL                   isEventCompleted;
@property (nonatomic, assign)BOOL                   hasFilterView;
@property (nonatomic, strong)PreviewViewController  *preViewVC;
@property (nonatomic, strong)EffectVideoController  *effictVC;
@property (nonatomic, copy)void(^timeBlock)(CGFloat totalTime);
@property (nonatomic)void(^currentTimeBlock)(CGFloat currentTime);
@property (nonatomic)void(^videoPausedBlock)(void);
@property (nonatomic)void(^videoPausedForMusicBlock)(void);
@property (nonatomic)void(^videoResumeBlock)(void);
@property (nonatomic)void(^currentTimeForMusicBlock)(CGFloat currentTime);
@property (nonatomic)void(^currentTimeTo0Block)(CGFloat currentTime);

@end
