//
//  MediaControlView.m
//  NewTestSDK
//
//  Created by gtv on 2017/11/10.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import "MediaControlView.h"

@interface MediaControlView() {
    BOOL _isMediaSliderBeingDragged;
    
}

@property(nonatomic,weak) GTVReplayController * delegatePlayer;

@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) UIButton *pauseButton;

@property (nonatomic, strong) UILabel * currentTimeLabel;
@property (nonatomic, strong) UILabel * totalDurationLabel;
@property (nonatomic, strong) UISlider * mediaProgressSlider;

@property (nonatomic, strong) NSTimer * refreshTimer;

@end

@implementation MediaControlView
{
    int num;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self ) {
        num = 1;
        int lblWidth = 60;
        int lblHeight = 60;
        self.currentTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, frame.size.height-lblHeight, lblWidth, lblHeight/2)];
        self.totalDurationLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width-lblWidth-10, frame.size.height-lblHeight, lblWidth, lblHeight/2)];
        self.mediaProgressSlider = [[UISlider alloc] initWithFrame:CGRectMake(10, frame.size.height-lblHeight/2-8, frame.size.width-20, lblHeight/2)];
        
        [self.mediaProgressSlider setThumbImage:[UIImage imageNamed:@"voice_slide"] forState:UIControlStateNormal];
        [self.mediaProgressSlider setMaximumTrackTintColor:[UIColor redColor]];
        [self.mediaProgressSlider setMinimumTrackTintColor:[UIColor whiteColor]];
        
        self.playButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width/2-lblWidth/2, frame.size.height/2-lblWidth/2, lblWidth, lblWidth)];
        self.pauseButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width/2-lblWidth/2, frame.size.height/2-lblWidth/2, lblWidth, lblWidth)];
    }
    
    [self.playButton setImage:[UIImage imageNamed:@"qu_play"] forState:UIControlStateNormal];
    [self.pauseButton setImage:[UIImage imageNamed:@"qu_pause"] forState:UIControlStateNormal];
    
    [_playButton addTarget:self action:@selector(playBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_pauseButton addTarget:self action:@selector(pauseBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.currentTimeLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [self.totalDurationLabel setFont:[UIFont systemFontOfSize:14.0f]];
    
    [self.currentTimeLabel setTextColor:[UIColor whiteColor]];
    [self.totalDurationLabel setTextColor:[UIColor whiteColor]];
    
    [self.currentTimeLabel setTextAlignment:NSTextAlignmentLeft];
    [self.totalDurationLabel setTextAlignment:NSTextAlignmentRight];
    
    [self addSubview:self.mediaProgressSlider];
    [self addSubview:self.currentTimeLabel];
    [self addSubview:self.totalDurationLabel];
    
    [self addSubview:self.playButton];
    [self addSubview:self.pauseButton];
    
    // 默认显示playButton
    [self.pauseButton setHidden:true];
    
    return self;
}

- (void) dealloc
{
    [self.refreshTimer invalidate];
    self.refreshTimer = nil;
}

- (void) pauseBtn_OnClicked:(id)sender
{
    [self.delegatePlayer pauseVideo];
    if (self.videoPausedForMusicBlock) {
        self.videoPausedForMusicBlock();
    }
    return;
}

- (void) playBtn_OnClicked:(id)sender
{
    /** 判断播放器当前状态是否为播放完毕，是就seek到0，从新播放 */
    if (self.isEventCompleted == YES && self.currentTimeTo0Block) {
        [self.delegatePlayer seekVideoTo:0 + self.preViewVC.cutTime];
        self.currentTimeTo0Block(0);
        self.isEventCompleted = NO;
    }
    [self.delegatePlayer resumeVideo];
    if (self.videoResumeBlock) {
        self.videoResumeBlock();
    }
    return;
}

- (void)didSliderTouchDown:(id)sender
{
    [self beginDragMediaSlider];
}

- (void)didSliderTouchCancel:(id)sender
{
    [self endDragMediaSlider];
}

- (void)didSliderTouchUpOutside:(id)sender
{
    [self endDragMediaSlider];
}

- (void)didSliderTouchUpInside:(id)sender
{
    // .currentPlaybackTime = self.mediaProgressSlider.value;
    int milli = self.mediaProgressSlider.value*1000;
    NSLog(@"seek to %d ", milli);
    NSLog(@"cutTime  %f ",self.preViewVC.cutTime);
    [self.delegatePlayer seekVideoTo:milli];//-self.preViewVC.cutTime
    [self endDragMediaSlider];
}

- (void)didSliderValueChanged:(id)sender
{
    if (self.mediaProgressSlider.value < self.preViewVC.cutTime/1000) {
        self.mediaProgressSlider.value = self.preViewVC.cutTime/1000;
    }
    [self continueDragMediaSlider];
}

- (void) startWithPlayer:(GTVReplayController*)player
{
    self.delegatePlayer = player;
    
    _isMediaSliderBeingDragged = false;
    
    self.refreshTimer = [NSTimer timerWithTimeInterval:0.02f target:self selector:@selector(refreshTimeout:) userInfo:nil repeats:true];
    [[NSRunLoop mainRunLoop] addTimer:self.refreshTimer forMode:NSDefaultRunLoopMode];
    
    [self.mediaProgressSlider addTarget:self action:@selector(didSliderTouchDown:) forControlEvents:UIControlEventTouchDown];
    [self.mediaProgressSlider addTarget:self action:@selector(didSliderTouchCancel:) forControlEvents:UIControlEventTouchCancel];
    [self.mediaProgressSlider addTarget:self action:@selector(didSliderTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
    [self.mediaProgressSlider addTarget:self action:@selector(didSliderTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [self.mediaProgressSlider addTarget:self action:@selector(didSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    
    
    return;
}

- (void) stopPlayer
{
    [self.refreshTimer invalidate];
    self.refreshTimer = nil;
    
    self.delegatePlayer = nil;
}

- (void)beginDragMediaSlider
{
    _isMediaSliderBeingDragged = YES;
}

- (void)endDragMediaSlider
{
    _isMediaSliderBeingDragged = NO;
    BOOL isPaused = [self.delegatePlayer isPaused];
    if (!isPaused && self.videoResumeBlock) {
        self.videoResumeBlock();
    }
}

- (void)continueDragMediaSlider
{
    [self refreshMediaControl];
}

- (void) refreshTimeout:(NSTimer*)t
{
    if( self.refreshTimer != t )
        return;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self refreshMediaControl];
    });
    
    return;
}

- (void)refreshMediaControl
{
    
    // status
    BOOL isPaused = [self.delegatePlayer isPaused];
    if (isPaused && self.videoPausedBlock && !self.hasFilterView) {
        self.videoPausedBlock();
    }
    
    self.playButton.hidden = !isPaused;
    self.pauseButton.hidden = isPaused;
    if (isPaused) {
        return;
    }
    
    
    NSLog(@"self.delegatePlayer.currentPlaybackTime-----%d",self.delegatePlayer.currentPlaybackTime);
    NSLog(@"self.delegatePlayer.getDuration-----%d",self.delegatePlayer.getDuration);
    // duration
    NSTimeInterval duration = [self.delegatePlayer getDuration] * 1.0f / 1000.0f;
    NSInteger intDuration = duration + 0.5;
    if (intDuration > 0) {
        self.mediaProgressSlider.maximumValue = duration;
        self.totalDurationLabel.text = [NSString stringWithFormat:@"%02d:%02d", (int)(intDuration / 60), (int)(intDuration % 60)];
        
        self.preViewVC.totalTime = (CGFloat )duration;
        if (num == 1) {
            [self.preViewVC getTime];
            
            self.preViewVC.clipMusicDetailsView.videoTime = (CGFloat )duration;
            [self.preViewVC.clipVideoView loadVideoTime:(CGFloat )duration];
            [self.preViewVC.videoFrameRateView loadTime:(CGFloat )duration];
            if (self.timeBlock) {
                self.timeBlock((CGFloat )duration);
            }
            
            
            num ++;
        }
    } else {
        self.totalDurationLabel.text = @"--:--";
        self.mediaProgressSlider.maximumValue = 1.0f;
    }
    
    // position
    NSTimeInterval position;
    //当前位置
    NSTimeInterval currentTimeValue;
    if (_isMediaSliderBeingDragged) {
        position = self.mediaProgressSlider.value;
        currentTimeValue = self.mediaProgressSlider.value * 1.0f;
    } else {
        position = self.delegatePlayer.currentPlaybackTime * 1.0f / 1000.0f;
        currentTimeValue = self.delegatePlayer.currentPlaybackTime * 1.0f / 1000.0f;
    }
    //当前位置
    
    //NSInteger intCurrentTimeValue = round(currentTimeValue);
    
    NSInteger intPosition = position + 0.5;
    if (position > 1.0f *self.preViewVC.cutTime /1000.0f) {
        self.mediaProgressSlider.value = position ;
    } else {
        self.mediaProgressSlider.value = 1.0f *self.preViewVC.cutTime /1000.0f ;
    }
    self.currentTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d", (int)(intPosition / 60), (int)(intPosition % 60)];
    if (self.currentTimeBlock) {
        self.currentTimeBlock((CGFloat)currentTimeValue);
    }
    if (self.currentTimeForMusicBlock && _isMediaSliderBeingDragged) {
        self.currentTimeForMusicBlock((CGFloat)currentTimeValue);
    }
    
//    // status
//    BOOL isPaused = [self.delegatePlayer isPaused];
//    if (isPaused && self.videoPausedBlock) {
//        self.videoPausedBlock();
//    }
//    
//    self.playButton.hidden = !isPaused;
//    self.pauseButton.hidden = isPaused;
    
    return;
}

@end
