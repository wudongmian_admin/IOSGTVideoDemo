//
//  MusicData.h
//  MiGuLives
//
//  Created by 锦 on 2018/3/26.
//  Copyright © 2018年 锦. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MusicData : NSObject
@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* eid;
@property (nonatomic,strong) NSString* musicPath;
@property (nonatomic,strong) NSString* iconPath;
@property (nonatomic,assign) BOOL isSelected;
@end
