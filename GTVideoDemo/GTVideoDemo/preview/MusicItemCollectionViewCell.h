//
//  MusicItemCollectionViewCell.h
//  MiGu
//
//  Created by 锦 on 2018/3/22.
//  Copyright © 2018年 锦. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusicItemCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) UIImageView* iconImgView;
@property (nonatomic,strong) UILabel* nameLabel;
@property (nonatomic,strong) UIImageView* CheckMarkImgView;
@end
