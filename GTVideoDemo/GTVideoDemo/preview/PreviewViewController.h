//
//  PreviewViewController.h
//  NewTestSDK
//
//  Created by gtv on 2017/9/30.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ClipFramework/ClipFramework.h>





@interface PreviewViewController : UIViewController

@property (nonatomic, strong) NSString * musicPath;

@property (nonatomic, assign) BOOL fromRecord;

@property (nonatomic, strong) NSString * workFolder;
@property (nonatomic, strong) NSString * draftsFolder;

@property (nonatomic, strong) NSString * vPath;     // 正序视频文件
@property (nonatomic, strong) NSString * vReversePath;  // 反序视频文件
/** 剪切后的时间 */
@property (nonatomic, assign) CGFloat cutTime;
/** 上次剪切后的时间 */
@property (nonatomic, assign) CGFloat lastCutTime;
/**
 剪切音乐的滑动View
 */
@property (nonatomic, strong)ClipMusicView *clipMusicDetailsView;
@property (nonatomic, assign)CGFloat totalTime;
/**
 剪切视频的滑动View
 */
@property (nonatomic, strong)ClipVideoView  *clipVideoView;
/**
 视频动态头像下面滑动View
 */
@property (nonatomic, strong)VideoFrameView *videoFrameRateView;

/**
 第一次加载视频时间获取
 */
-(void) getTime;
@end
