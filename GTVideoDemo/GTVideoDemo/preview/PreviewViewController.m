//
//  PreviewViewController.m
//  NewTestSDK
//
//  Created by gtv on 2017/9/30.
//  Copyright © 2017年 gtv. All rights reserved.
//
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height


#import "PreviewViewController.h"
#import "DPHorizontalScrollView.h"
#import "GTVideoLib/GTVideoLib.h"

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "GTVConfig.h"
#import "MediaControlView.h"
#import "SlowSlideView.h"
#import "UIView+Toast.h"
#import "MusicItemCollectionViewCell.h"
#import "MusicData.h"
#include "libimg2webp.h"
#import "CutVideoController.h"
#import "EffectVideoController.h"
#import "HeadPortraitViewController.h"
#import "SelectMusicViewController.h"
#import "MergeViewController.h"
#define SCROLL_VIEW_HEIGHT  60

@interface PreviewViewController () <DPHorizontalScrollViewDelegate, UIGestureRecognizerDelegate, SlowSlideViewDelegate, GTVReplayControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,ClipMusicViewDelegate,VideoFrameViewDelegate,SelectMusicViewControllerDelegate,CutVideoControllerDelegate,EffectVideoControllerDelegate>

@property (nonatomic, strong) UIView * replayView;
@property (nonatomic, strong) GTVReplayController * replayCtrl;

@property (nonatomic, strong) UIButton * pickBtn;           // 挑选封面图片
@property (nonatomic, strong) UIButton * slowBtn;           // 慢动作按钮
@property (nonatomic, strong) UIButton * backBtn;
@property (nonatomic, strong) UIButton * mergeBtn;

@property (nonatomic, strong) UIButton * testMusicBtn;
@property (nonatomic, strong) UIButton * testClipBtn;
@property (nonatomic, strong) UIButton * testDelBtn;
@property (nonatomic, strong) UIButton * testMuteBtn;
@property (nonatomic, strong) UIButton * testEffectBtn;
@property (nonatomic, strong) UIButton * DraftsBtn;

@property (nonatomic, strong) DPHorizontalScrollView * scrollView;
@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, strong) UIView * selectedMarkView;    // 标记选中状态的view

// 封面设置
@property (nonatomic, strong) UIImage * pickedImage;

@property (nonatomic, strong) NSString * recPath;

@property (nonatomic, strong) MediaControlView * mediaControl;
@property (nonatomic, strong) SlowSlideView * slowSlideView;

@property (nonatomic, strong) UIView * mediaContainer;

@property (nonatomic, strong) NSMutableDictionary * filterSetting;
@property (nonatomic, assign) int slowSetting;
/**
 音乐数组
 */
@property (nonatomic, strong)NSMutableArray *musicAry;
/**
 动态头像View
 */
@property (nonatomic, strong)UIView        *videoFrameView;
/**
 剪辑视频View
 */
@property (nonatomic, strong) UIView  *clipView;
/**
 剪辑音乐View
 */
@property (nonatomic, strong)UIView     *clipMusicView;
/**
 音乐选择View
 */
@property (nonatomic, strong)UICollectionView *musicCollectionView;
/**
 音乐时间Label
 */
@property (nonatomic, strong)UILabel *musicTimeLabel;
/**
 动态头像ImageView
 */
@property (nonatomic, strong) UIImageView *animView;
@property (nonatomic, strong) NSArray * thumbList;
@property (nonatomic, strong) NSArray * imageArr;
@property (nonatomic, strong) NSArray * headerImageArr;

@property (nonatomic, strong) CustomInteractiveTransition *interactivePush;
@property (nonatomic, strong) UILabel            *testExportLabel;
@end

@implementation PreviewViewController
{
    /**
     动态头像的label
     */
    UILabel *timeLabel;
    NSString *_mPath;
    int      thumbList_index;
    CGFloat _topTime;
    /** 剪切后的时间 */
    //CGFloat _cutTime;
    CGFloat _cutTopTime;
    /** 记录音乐控件滚动后的音乐开始时间 */
    CGFloat _lastMusicTime;
    BOOL    _exportReplayFile;
    
}

-(void)viewWillAppear:(BOOL)animated {
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    /**
     获取音乐数组
     */
    _musicAry = [NSMutableArray arrayWithArray:[self creatMusicData]];
    self.thumbList = [NSArray array];
    self.imageArr = [NSArray array];
    _cutTime = 0;
    // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    CGRect rect = [UIScreen mainScreen].bounds;
    {
        int width = rect.size.width;
        //int height = width*4/3;
        int x = 0;
        //int y = (rect.size.height-height) / 2;
        
        self.mediaContainer = [[UIView alloc] initWithFrame:CGRectMake(x, 80, width, SCREEN_HEIGHT-80)];
        
        
        [self.mediaContainer setBackgroundColor:[UIColor blackColor]];
        
        self.replayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, SCREEN_HEIGHT-80)];
        [self.mediaContainer addSubview:self.replayView];
        
        self.mediaControl = [[MediaControlView alloc] initWithFrame:CGRectMake(0, 0, width,SCREEN_HEIGHT-80)];
        __weak typeof(self) weakSelf = self;
        self.mediaControl = [[MediaControlView alloc] initWithFrame:CGRectMake(0, 0, width,SCREEN_HEIGHT-80)];
        self.mediaControl.videoResumeBlock = ^{
            //[weakSelf.clipMusicDetailsView animationContinue];
        };
        self.mediaControl.videoPausedForMusicBlock  = ^{
            //[weakSelf.clipMusicDetailsView animationPause];
        };
        self.mediaControl.currentTimeForMusicBlock = ^(CGFloat currentTime) {
          [weakSelf.clipMusicDetailsView seekTo:currentTime  - 1.00f *_cutTime /1000 ];
        };
        self.mediaControl.currentTimeBlock = ^(CGFloat currentTime) {
//            NSLog(@"-------%f",currentTime  - 1.00f *_cutTime /1000);
            [weakSelf.clipMusicDetailsView seekTo:currentTime  - 1.00f *_cutTime /1000 ];
        };
        self.mediaControl.currentTimeTo0Block = ^(CGFloat currentTime) {
            [weakSelf.clipMusicDetailsView seekTo:currentTime];
        };
//        self.mediaControl.timeBlock = ^(CGFloat totalTime) {
//
//        };
        [self.mediaContainer addSubview:self.mediaControl];
        self.mediaControl.preViewVC = self;
        
        self.slowSlideView = [[SlowSlideView alloc] initWithFrame:CGRectMake(10, 0, rect.size.width-20, 40)];
        [self.mediaContainer addSubview:self.slowSlideView];
        [self.slowSlideView setHidden:true];
        self.slowSlideView.delegate = self;
    }
    [self.view addSubview:self.mediaContainer];
    
    self.view.backgroundColor = [GTVConfig config].backgroundColor;
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 40, 66, 36)];
    [backBtn setTitle:@"返回" forState:(UIControlStateNormal)];
    [backBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    // backBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [backBtn addTarget:self action:@selector(backToRecordView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    self.pickBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.pickBtn.frame = CGRectMake(75, 40, 66, 36);;
    [self.pickBtn setTitle:@"设置封面" forState:(UIControlStateNormal)];//设置封面
    [self.pickBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.pickBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.pickBtn addTarget:self action:@selector(pickBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.pickBtn];
    
    self.slowBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.slowBtn.frame = CGRectMake(rect.size.width/2-33, 40, 66, 36);
    [self.slowBtn setTitle:@"慢动作" forState:(UIControlStateNormal)];
    [self.slowBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.slowBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.slowBtn addTarget:self action:@selector(slowBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.slowBtn];

    self.mergeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.mergeBtn.frame = CGRectMake(rect.size.width-66, 40, 66, 36);
    [self.mergeBtn setTitle:@"下一步" forState:(UIControlStateNormal)];
    [self.mergeBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.mergeBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.mergeBtn addTarget:self action:@selector(mergeBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.mergeBtn];
  
//    if (!_fromRecord) {
//        UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(rect.size.width-66 - 80, 40, 60, 36)];
//        [rightBtn setTitle:@"取消" forState:UIControlStateNormal];
//        [rightBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//        [rightBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
//        [rightBtn addTarget:self action:@selector(dismissToDraftsVC) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:rightBtn];
//    }
    
    
    

    self.testMusicBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, rect.size.height/2, 80, 30)];
    [self.testMusicBtn setBackgroundColor:[UIColor orangeColor]];
    [self.testMusicBtn setTitle:@"测试音乐" forState:UIControlStateNormal];
    [self.testMusicBtn setTitle:@"测试音乐" forState:UIControlStateSelected];
    [self.testMusicBtn addTarget:self action:@selector(testMusicBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.testMusicBtn];
    
    self.testClipBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, rect.size.height/2+50, 80, 30)];
    [self.testClipBtn setBackgroundColor:[UIColor orangeColor]];
    [self.testClipBtn setTitle:@"测试视频" forState:UIControlStateNormal];
    [self.testClipBtn setTitle:@"测试视频" forState:UIControlStateSelected];
    [self.testClipBtn addTarget:self action:@selector(testClipBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.testClipBtn];
    
    self.testDelBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, rect.size.height/2-50, 80, 30)];
    [self.testDelBtn setBackgroundColor:[UIColor orangeColor]];
    [self.testDelBtn setTitle:@"删除滤镜" forState:UIControlStateNormal];
    [self.testDelBtn addTarget:self action:@selector(testDelBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.testDelBtn];
    
    self.testMuteBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, rect.size.height/2+100, 80, 30)];
    [self.testMuteBtn setBackgroundColor:[UIColor orangeColor]];
    [self.testMuteBtn setTitle:@"选择音乐" forState:UIControlStateNormal];
    [self.testMuteBtn setTitle:@"选择音乐" forState:UIControlStateSelected];
    [self.testMuteBtn addTarget:self action:@selector(testMuteBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.testMuteBtn];
    
    self.testEffectBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, rect.size.height/2+200, 80, 30)];
    [self.testEffectBtn setBackgroundColor:[UIColor orangeColor]];
    [self.testEffectBtn setTitle:@"编辑特效" forState:(UIControlStateNormal)];
    [self.testEffectBtn addTarget:self action:@selector(testEffectBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.testEffectBtn];
    
    self.DraftsBtn = [[UIButton alloc] initWithFrame:CGRectMake(rect.size.width - 100, rect.size.height/2+200, 80, 30)];
    [self.DraftsBtn setBackgroundColor:[UIColor orangeColor]];
    [self.DraftsBtn setTitle:@"保存" forState:(UIControlStateNormal)];
    [self.DraftsBtn addTarget:self action:@selector(DraftsBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
   // [self.view addSubview:self.DraftsBtn];
    
    if( self.selectedMarkView == nil ) {
        
        self.selectedMarkView = [[UIView alloc] initWithFrame:CGRectMake(2, 2, 56, 56)];
        self.selectedMarkView.layer.borderColor = [UIColor redColor].CGColor;
        self.selectedMarkView.layer.borderWidth = 2.0f;
    }
    
    self.filterSetting = [[NSMutableDictionary alloc] init];
    self.slowSetting = -1;
    
    self.replayCtrl = [[GTVReplayController alloc] init];
    [self.replayCtrl setReplayDelegate:self];
    
    [self.replayCtrl setReplayVideoPath:self.vPath];
    [self.replayCtrl setReplayConfigFolder:self.workFolder forceInitFlag:self.fromRecord];
    
    [self otherViews];
    
    [self.view addSubview:self.videoFrameView];
    
    _interactivePush = [CustomInteractiveTransition interactiveTransitionWithTransitionType:CustomInteractiveTransitionTypePresent GestureDirection:CustomInteractiveTransitionGestureDirectionUp];
    typeof(self)weakSelf = self;
    _interactivePush.presentConifg = ^(){
        [weakSelf testMuteBtn_OnClicked:nil];
    };
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackground:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [self loadAnimationViewWithTime:0];
    return;
    
}
-(void)appDidEnterBackground:(id)sender {
    if (_exportReplayFile) {
        [self.replayCtrl cancelExportReplayFile];
        _exportReplayFile = NO;
        //[self.view hideToastActivity];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.testExportLabel.hidden = YES;
            self.testExportLabel.text = @"";
        });
    }
    
}
-(void)appDidBecomeActive:(id)sender {
    
}

-(void)dismissToDraftsVC {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
    UITouch *touch = [allTouches anyObject];
    
    if ([touch view] == self.clipView || [touch view] == self.clipMusicView) {
        return;
    }
    
    if (self.testMusicBtn.selected == YES) {
        self.testMusicBtn.selected = NO;
        [self clipMViewDown];
      //  [self.clipMusicDetailsView end];
    }
    if (self.testClipBtn.selected == YES) {
        self.testClipBtn.selected = NO;
        [self clipVViewDown];
    }
    
}
/**
 隐藏剪辑View
 */
- (void) clipMViewDown {
    [UIView animateWithDuration:.5 animations:^{
        self.clipMusicView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 150);
    }];
}
- (void) clipVViewDown {
    
    [UIView animateWithDuration:.5 animations:^{
        self.clipView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 150);
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
     self.navigationController.navigationBar.hidden = YES;
   
    
    [self.replayCtrl setReplayVideoPath:self.vPath];
    
    // 设置慢速区间
//    [self.replayCtrl setSlowPlayFrom:1000 toTime:4000];
//    [self.replayCtrl setRepeatCount:3];
    
    // 单位是毫秒
//    NSRange testRange = NSMakeRange(3000, 6000);
//    [self.replayCtrl setMusicPath:self.musicPath andRange:testRange];
//    [self.replayCtrl setReplayValidRange:testRange];
    
    // 可以通过界面选择设置配乐
//    NSString * musicPath = [[NSBundle mainBundle] pathForResource:@"hongzhaoyuan.mp3" ofType:nil];
//    [self.replayCtrl setMusicPath:musicPath andStartTime:0];
    
    // 开始播放后立刻暂停
    [self.replayCtrl playVideo];
    [self.replayCtrl pauseVideo];
    
    // 播放控制权交给控件
    [self.mediaControl startWithPlayer:self.replayCtrl];
    //self.scrollView.scrollViewDelegate = self;
    return;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    // 开启返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
//    self.scrollView.scrollViewDelegate = nil;
//    [self.scrollView removeFromSuperview];
//    self.scrollView = nil;
    
    [self.mediaControl stopPlayer];
    [self.replayCtrl stopVideo];
    
    
}
-(void) removeFile:(NSString*) path{
    
    BOOL isDir;
    
    [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];
    
    if (isDir) {
        NSArray* files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
        for (NSString* file in files) {
            [self removeFile:[path stringByAppendingPathComponent:file]];
        }
    }
    else{
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
}
-(void) backToRecordView {
    if (_fromRecord) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        
        
        NSString * tmpDir = [NSString stringWithFormat:@"%@/abcdefg/", NSTemporaryDirectory()];
        
        [self removeFile:tmpDir];
        NSFileManager * fileManger = [NSFileManager defaultManager];
        
        BOOL isHave = [fileManger fileExistsAtPath:tmpDir];
        if (isHave) {
            
        }else {
            BOOL isSuccessCreate = [fileManger createDirectoryAtPath:tmpDir withIntermediateDirectories:YES attributes:nil error:nil];
            NSLog(@"%@",isSuccessCreate ? @"创建成功" : @"创建失败");
        }
        
        NSString * p = _workFolder;
        
        
        BOOL isDir = NO;
        
        BOOL isExist = [fileManger fileExistsAtPath:p isDirectory:&isDir];
        
        if( !isExist ) {
            [self.view makeToast:@"请先去录制" duration:2.0f position:CSToastPositionCenter];
            return;
        }else{
            
            
            if (isDir) {
                
                NSArray * dirArray = [fileManger contentsOfDirectoryAtPath:p error:nil];
                
                NSString * subPath = nil;
                
                for (NSString * str in dirArray) {
                    
                    subPath  = [p stringByAppendingPathComponent:str];
                    
                    BOOL issubDir = NO;
                    
                    [fileManger fileExistsAtPath:subPath isDirectory:&issubDir];
                    
                    NSString *toPath = [NSString stringWithFormat:@"%@/%@",tmpDir,str];
                    BOOL isSuccess = [fileManger copyItemAtPath:subPath toPath:toPath error:nil];
                    
                    if (isSuccess) {
                        NSLog(@"copyItem isSuccess");
                    }
                    
                    NSLog(@"subPath --  %@",subPath);
                }
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                NSLog(@"%@",p);
            }
            
        }
    }
    
}
-(void)setMusicPath:(NSString *)musicPath {
    _musicPath = musicPath;
//    [self.replayCtrl setMusicPath:_musicPath andRange:NSMakeRange(0, 0)];
//    [self.replayCtrl playVideo];
//    [self.replayCtrl pauseVideo];
    // 可以通过界面选择设置配乐
    
    NSRange testRange = NSMakeRange(0, 0);
    [self.replayCtrl setMusicPath:musicPath andRange:testRange];
    
    // 开始播放后立刻暂停
    [self replay];
}
-(void) otherViews {
    [self.view makeToast:@"选择特效后预览合成效果" duration:3.0f position:CSToastPositionCenter];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    CGRect mediaFrame = self.mediaContainer.frame;
    float top = mediaFrame.origin.y + mediaFrame.size.height;
    top = top + (rect.size.height - top - SCROLL_VIEW_HEIGHT) / 2;
    
//    self.scrollView = [[DPHorizontalScrollView alloc] initWithFrame:CGRectMake(0, rect.size.height-SCROLL_VIEW_HEIGHT - 30, rect.size.width, SCROLL_VIEW_HEIGHT)];
//    [self.view addSubview:self.scrollView];
    
//    self.scrollView.scrollViewDelegate = self;
//    self.selectedIndex = 0;
//    
//    [self.scrollView reloadData];
//    [self.scrollView setHidden:NO];
    
    /////////////////////////////////////////////////////////
    int width = rect.size.width;
   // int height = width*4/3;
    // 开始播放器
    [self.replayCtrl setReplayRenderView:self.replayView withFrame:CGRectMake(0, 0, width, SCREEN_HEIGHT-80)];
    
    [self.replayCtrl setReplayVideoPath:self.vPath];
    
    // 设置慢速区间
    //    [self.replayCtrl setSlowPlayFrom:1000 toTime:4000];
    //    [self.replayCtrl setRepeatCount:3];
    
    // 单位是毫秒
    //    NSRange testRange = NSMakeRange(3000, 6000);
    //    [self.replayCtrl setMusicPath:self.musicPath andRange:testRange];
    //    [self.replayCtrl setReplayValidRange:testRange];
    
    // 可以通过界面选择设置配乐
    //    NSString * musicPath = [[NSBundle mainBundle] pathForResource:@"hongzhaoyuan.mp3" ofType:nil];
    //    [self.replayCtrl setMusicPath:musicPath andStartTime:0];
    
    // 开始播放后立刻暂停
    [self.replayCtrl playVideo];
    [self.replayCtrl pauseVideo];
    //***************
    // 播放控制权交给控件
    //  [self.mediaControl startWithPlayer:self.replayCtrl];
    
    
    self.clipView = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 150)];
    self.clipView.backgroundColor = [UIColor blackColor];
    
    self.clipVideoView = [[ClipVideoView alloc]initWithFrame:CGRectMake(0, 60, SCREEN_WIDTH, 70)];
    //self.clipVideoView.totalTime = _totalTime;
    [self.clipVideoView initSubViewsWithNumber:7];
    [self.clipView addSubview:self.clipVideoView];
    
    
    
    
    
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 + 40, 5, 80, 35)];
    btn.layer.cornerRadius = 10;
    [btn setTitle:@"剪辑" forState:UIControlStateNormal];
    [btn setTitle:@"剪辑" forState:UIControlStateSelected];
    [btn addTarget:self action:@selector(clipButton:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = 10;
    
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 120, 5, 80, 35)];
    btn1.backgroundColor = [UIColor redColor];
    btn1.layer.cornerRadius = 10;
    [btn1 setTitle:@"不剪辑" forState:UIControlStateNormal];
    [btn1 setTitle:@"不剪辑" forState:UIControlStateSelected];
    [btn1 addTarget:self action:@selector(clipButton:) forControlEvents:UIControlEventTouchUpInside];
    btn1.tag = 11;
    
    [self.clipView addSubview:btn];
    [self.clipView addSubview:btn1];
    
    
    
    [self.view addSubview:self.clipView];
    
    
    self.clipMusicView = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 150)];
    self.clipMusicView.backgroundColor =  [UIColor blackColor];
    
    self.clipMusicDetailsView = [[ClipMusicView alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, 110)];
    self.clipMusicDetailsView.delegate = self;
    [self.clipMusicView addSubview:self.clipMusicDetailsView];
//    self.clipMusicDetailsView.musicTime = musicTime;
//    self.clipMusicDetailsView.musicTotalTime = musicTotalTime;
    
    UILabel *musicTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 25, 40, 15)];
    musicTimeLabel.font = [UIFont systemFontOfSize:12];
    musicTimeLabel.textColor = [UIColor whiteColor];
    musicTimeLabel.text = @"00:00";
    self.musicTimeLabel = musicTimeLabel;
    [self.clipMusicView addSubview:musicTimeLabel];
    
    
    UIButton *b = [[UIButton alloc]initWithFrame:CGRectMake(50, 5, 90, 40)];
    b.titleLabel.font = [UIFont systemFontOfSize:14];
    [b setTitle:@"单独测试播放" forState:UIControlStateNormal];
    [self.clipMusicView addSubview:b];
    [b addTarget:self action:@selector(testRestart) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *b1 = [[UIButton alloc]initWithFrame:CGRectMake(170, 5, 90, 40)];
    b1.titleLabel.font = [UIFont systemFontOfSize:14];
    [b1 setTitle:@"单独测试动画" forState:UIControlStateNormal];
    [self.clipMusicView addSubview:b1];
    [b1 addTarget:self action:@selector(testAnimation:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *selectMusicBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 100, 5, 80, 40)];
    selectMusicBtn.layer.cornerRadius = 10;
    selectMusicBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [selectMusicBtn setTitle:@"选择音乐" forState:UIControlStateNormal];
    [selectMusicBtn setTitle:@"选择音乐" forState:UIControlStateSelected];
    [selectMusicBtn addTarget:self action:@selector(musicHiddenBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.clipMusicView addSubview:selectMusicBtn];
    
    
    [self.view addSubview:self.clipMusicView];
    
    
    
    //collectionView
    UICollectionViewFlowLayout* layout2 = [[UICollectionViewFlowLayout alloc] init];
    layout2.itemSize = CGSizeMake(83, 115);
    layout2.estimatedItemSize = CGSizeMake(83, 115);
    
    //设置分区的头视图和尾视图是否始终固定在屏幕上边和下边
    //    layout2.sectionFootersPinToVisibleBounds = YES;
    //    layout2.sectionHeadersPinToVisibleBounds = YES;
    
    // 设置水平滚动方向
    //水平滚动
    layout2.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    // 设置额外滚动区域
    layout2.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // 设置cell间距
    //设置水平间距, 注意点:系统可能会跳转(计算不准确)
    layout2.minimumInteritemSpacing = 0;
    //设置垂直间距
    layout2.minimumLineSpacing = 0;
    
    
    
    _musicCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 45, SCREEN_WIDTH, 115) collectionViewLayout:layout2];
    
    //设置背景颜色
    _musicCollectionView.backgroundColor = [UIColor clearColor];
    
    
    // 设置数据源,展示数据
    _musicCollectionView.dataSource = self;
    //设置代理,监听
    _musicCollectionView.delegate = self;
    
    // 注册cell
    [_musicCollectionView registerClass:[MusicItemCollectionViewCell class] forCellWithReuseIdentifier:@"MyCollectionCell2"];
    
    /* 设置UICollectionView的属性 */
    //设置滚动条
    _musicCollectionView.showsHorizontalScrollIndicator = NO;
    _musicCollectionView.showsVerticalScrollIndicator = NO;
    
    //设置是否需要弹簧效果
    _musicCollectionView.bounces = YES;
    
    [_clipMusicView addSubview:_musicCollectionView];
    
    _musicCollectionView.hidden = YES;
    
    
    NSRange testRange = NSMakeRange(0, 0);
    if (_musicPath) {
        [self.replayCtrl setMusicPath:_musicPath andRange:testRange];
        self.clipMusicDetailsView.musicTotalTime = [self durationWithMusic:_musicPath];
        // 开始播放后立刻暂停
        //    [self replay];
        [self.replayCtrl playVideo];
        [self.replayCtrl pauseVideo];
    }
    
}
-(void)testAnimation:(UIButton *)btn {
    btn.selected = !btn.selected;
    if (btn.selected) {
        [self.replayCtrl pauseVideo];
        //[self.clipMusicDetailsView animationPause];
    }else {
        [self.replayCtrl resumeVideo];
        //[self.clipMusicDetailsView animationContinue];
    }
    
}
-(void)testRestart {
    [self replay];
    //[self.clipMusicDetailsView restart];
}
-(void)ClipMusicViewDidEndWithMusicTime:(CGFloat)musicNowTime MusictotalTime:(CGFloat)musicAllTime {
    _lastMusicTime = musicNowTime;
    self.musicTimeLabel.text = [NSString stringWithFormat:@"%.2d:%.2d",(int)musicNowTime/60,(int)musicNowTime%60];
    NSString * musicPath = _musicPath;
    

    [self.replayCtrl setMusicPath:musicPath andRange:NSMakeRange(musicNowTime * 1000, musicAllTime * 1000)];
    [self replay];
    
}
-(void) musicHiddenBtn:(UIButton *)btn {
    btn.selected = !btn.selected;
    if (btn.selected) {
        btn.backgroundColor = [UIColor redColor];
        _musicCollectionView.hidden = NO;
        [self.view viewWithTag:20].hidden = YES;
        [self.view viewWithTag:21].hidden = YES;
        self.clipMusicDetailsView.hidden = YES;
//        [self.clipMusicDetailsView end];
//        [self.clipMusicDetailsView showAgain];
        
    }else{
        btn.backgroundColor = [UIColor blackColor];
        _musicCollectionView.hidden = YES;
        [self.view viewWithTag:20].hidden = NO;
        [self.view viewWithTag:21].hidden = NO;
        self.clipMusicDetailsView.hidden = NO;
        
    }
}


-(void) replay {
    
    [self.replayCtrl seekVideoTo:0 + self.cutTime];
    
    [self.replayCtrl resumeVideo];
}


-(void) clipButton:(UIButton *)btn {
    //btn.selected = !btn.selected;
    
    
    if( btn.tag == 10 ) {
        btn.backgroundColor = [UIColor redColor];
        UIButton *b = [self.view viewWithTag:11];
        b.backgroundColor = [UIColor blackColor];
        
        [self.replayCtrl setReplayValidRange:NSMakeRange(self.clipVideoView.min, self.clipVideoView.max - self.clipVideoView.min)];
        NSLog(@"%f  ------   %f",self.clipVideoView.min, self.clipVideoView.max);
        self.clipMusicDetailsView.videoTime = (self.clipVideoView.max - self.clipVideoView.min)  ;
        
    }else {
        btn.backgroundColor = [UIColor redColor];
        UIButton *b = [self.view viewWithTag:10];
        b.backgroundColor = [UIColor blackColor];
        [self.replayCtrl setReplayValidRange:NSMakeRange(0, 0)];
        self.clipMusicDetailsView.videoTime = self.totalTime * 1000;
    }
    
    [self replay];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  总列数
 */
- (NSInteger)numberOfColumnsInTableView:(DPHorizontalScrollView *)tableView
{
    return 12;  // 第一个为Normal
}

/**
 *  每列显示的view
 */
static NSString * picList[] = {
    @"无", @"灵魂", @"抖动", @"异世", @"黑白",
    @"尖锐", @"波动", @"透视", @"闪烁", @"素描",
    @"灵异", @"印象"
};

static NSString * filterList[] = {
    DEF_GTV_EFFECT_NONE, DEF_GTV_EFFECT_LINGHUN, DEF_GTV_EFFECT_DOUDONG, DEF_GTV_EFFECT_YISHI, DEF_GTV_EFFECT_HEIBAI,
    DEF_GTV_EFFECT_JIANRUI, DEF_GTV_EFFECT_BODONG, DEF_GTV_EFFECT_TOUSHI,
    DEF_GTV_EFFECT_SHANSUO, DEF_GTV_EFFECT_SUMIAO,
    DEF_GTV_EFFECT_LINGYI, DEF_GTV_EFFECT_YINXIANG
};
static NSString * usIconList[] = {
    @"us-none", @"us-linghun", @"us-doudong", @"us-yishi", @"us-heibai",
    @"us-jianrui", @"us-bodong", @"us-toushi", @"us-shansuo", @"us-sumiao",
    @"us-lingyi", @"us-yinxiang"
};

- (UIView *)tableView:(DPHorizontalScrollView *)tableView viewForColumnAtIndex:(NSInteger)index
{
    UIView * view = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCROLL_VIEW_HEIGHT, SCROLL_VIEW_HEIGHT)];
    UIImageView * icon = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, SCROLL_VIEW_HEIGHT-6, SCROLL_VIEW_HEIGHT-6)];
    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(3, 3, SCROLL_VIEW_HEIGHT-6, SCROLL_VIEW_HEIGHT-6)];
    
    self.selectedMarkView.frame = CGRectMake(2, 2, SCROLL_VIEW_HEIGHT-4, SCROLL_VIEW_HEIGHT-4);
    
    if( index < sizeof(usIconList)/sizeof(usIconList[0]) ) {
        UIImage * img = [UIImage imageNamed:usIconList[index]];
        [icon setImage:img];
        [view addSubview:icon];
    }
    
    if( index < sizeof(picList)/sizeof(picList[0]) ) {
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setText:picList[index]];
        [lbl setTextAlignment:NSTextAlignmentCenter];
    }
    
    [view addSubview:lbl];
    
    if( self.selectedIndex == index ) {
        [self.selectedMarkView removeFromSuperview];
        [view addSubview:self.selectedMarkView];
    }
    
    return view;
}

/**
 *  每行view的宽度
 */
- (CGFloat)tableView:(DPHorizontalScrollView *)tableView widthForColumnAtIndex:(NSInteger)index
{
    return 60.0f;
}

/**
 *  点击每列回调
 */
- (void)didSelectViewAtIndex:(NSInteger)index
{
    self.selectedIndex = (int)index;
    
    [self.scrollView reloadData];
}
-(void)didSelectLongViewAtIndex:(NSInteger)index {
    
    self.selectedIndex = (int )index;
    //[self.replayCtrl setEffectShader:filterList[index]];
    NSLog(@"filterList[index]  -------  %@   ----  %ld",filterList[index],index);
    [self.replayCtrl startEffectShader:filterList[index]];
    [self.replayCtrl resumeVideo];
    [self.scrollView reloadData];
}
- (void)didLongStopAtIndex:(NSInteger)index {
    [self.replayCtrl pauseVideo];
    [self.replayCtrl stopEffectShader:filterList[index]];
}
-(void)didLongStop {
    [self.replayCtrl pauseVideo];
}
- (void) delayPopup
{
    [self.navigationController popToRootViewControllerAnimated:true];
}
// 将特效编辑界面的设定内容反馈到播放器
- (void) effectVideoDoneWithList:(NSArray*)list
{
    if( list.count > 0 ) {
        [self.replayCtrl cloneEffectList:list];
    }
    else {
        [self.replayCtrl clearAllEffect];
    }
}

- (NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    return dateTime;
}

- (void) DraftsBtn_OnClicked:(id)sender {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    //[self.view makeToast:@"请稍后" duration:.5f position:CSToastPositionCenter];
    NSString * tmpDir;
    /** 是否从录制界面进来 */
    if (_fromRecord) {
        
        /** 录制界面是否从草稿箱进-->是否有草稿箱文件夹 */
        if (_draftsFolder) {
            tmpDir = [NSString stringWithFormat:@"%@/",_draftsFolder];
            [self removeFile:tmpDir];
        }else{
            /** 录制界面不是从草稿箱进，创建新文件夹 */
            NSString *randomName = [self getCurrentTime];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDir = [paths objectAtIndex:0];
            tmpDir = [NSString stringWithFormat:@"%@/abcdef/%@/", documentsDir,randomName];
        }
        
    }else{
        /** 直接从草稿箱进来，不会有新的视频文件，只需要保存配置文件 */
//        NSString * path = [NSString stringWithFormat:@"%@/sample.mp4", NSTemporaryDirectory()];
//        [self.replayCtrl exportReplayFile:path withSize:CGSizeMake(576,1024) andBitrate:2500000];
        [self.replayCtrl saveConfigToDisk];
        [self.view hideToastActivity];
        [self.view makeToast:@"已保存到草稿箱" duration:2.0f position:CSToastPositionCenter];
        return;
    }
    
    
    [self.replayCtrl saveConfigToDisk];
//    NSString * path = [NSString stringWithFormat:@"%@/sample.mp4", NSTemporaryDirectory()];
//    [self.replayCtrl exportReplayFile:path withSize:CGSizeMake(576,1024) andBitrate:2500000];
    [self.view hideToastActivity];
    
    NSFileManager * fileManger = [NSFileManager defaultManager];
    
    BOOL isHave = [fileManger fileExistsAtPath:tmpDir];
    if (isHave) {
        
    }else {
        BOOL isSuccessCreate = [fileManger createDirectoryAtPath:tmpDir withIntermediateDirectories:YES attributes:nil error:nil];
         NSLog(@"%@",isSuccessCreate ? @"创建成功" : @"创建失败");
    }
    
    NSString * p = [NSString stringWithFormat:@"%@/abcdefg/", NSTemporaryDirectory()];
    
    
    BOOL isDir = NO;
    
    BOOL isExist = [fileManger fileExistsAtPath:p isDirectory:&isDir];
    
    if( !isExist ) {
        [self.view makeToast:@"请先去录制" duration:2.0f position:CSToastPositionCenter];
        return;
    }else{
        
        
        if (isDir) {
            
            NSArray * dirArray = [fileManger contentsOfDirectoryAtPath:p error:nil];
            
            NSString * subPath = nil;
            
            for (NSString * str in dirArray) {
                
                subPath  = [p stringByAppendingPathComponent:str];
                
                BOOL issubDir = NO;
                
                [fileManger fileExistsAtPath:subPath isDirectory:&issubDir];
                
                NSString *toPath = [NSString stringWithFormat:@"%@/%@",tmpDir,str];
                BOOL isSuccess = [fileManger copyItemAtPath:subPath toPath:toPath error:nil];
                
                if (isSuccess) {
                    NSLog(@"copyItem isSuccess");
                }
                
                NSLog(@"subPath --  %@",subPath);
            }
            
            
        }else{
            NSLog(@"%@",p);
        }
        
    }
    
    
    [self.view makeToast:@"已保存到草稿箱" duration:2.0f position:CSToastPositionCenter];
}
- (void) testEffectBtn_OnClicked:(id)sender
{
    [self.replayCtrl pauseVideo];
    
    EffectVideoController * cut = [[EffectVideoController alloc] init];
    self.navigationController.delegate = cut;
    cut.vPath = self.vPath;
    cut.delegate = self;
    cut.fromeView = self.mediaContainer;
    [self.navigationController pushViewController:cut animated:YES];
}


-(void)SelectMusicDone:(NSString *)musicPath {
    _musicPath = musicPath;
    // 可以通过界面选择设置配乐
        NSRange testRange = NSMakeRange(0, 0);
    //[self.clipMusicDetailsView seekTo:0];
    [self.replayCtrl setMusicPath:musicPath andRange:testRange];
    self.clipMusicDetailsView.musicTotalTime = [self durationWithMusic:musicPath];
    // 开始播放后立刻暂停
    //[self replay];
    [self.replayCtrl playVideo];
    [self replay];
    //[self.clipMusicDetailsView animationContinue];
}
- (void) testMuteBtn_OnClicked:(id)sender
{
    SelectMusicViewController *vc = [SelectMusicViewController new];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
    
//    UIButton * b = (UIButton*)sender;
//    if( b.selected == false ) {
//        [self.replayCtrl setReplayMuteStatus:true];
//    }
//    else {
//        [self.replayCtrl setReplayMuteStatus:false];
//    }
//    b.selected = !b.selected;
}
#pragma mark    ======== SelectMusicViewControllerDelegate ========
- (void)SelectMusicViewControllerPressedDissmiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id<UIViewControllerInteractiveTransitioning>)SelectMusicViewControllerInteractiveTransitionForPresent{
    return _interactivePush;
}


// 删除最后一个滤镜
- (void) testDelBtn_OnClicked:(id)sender
{
    [self.replayCtrl removeLastEffectShader];
    
    return;
}
-(void)cutVideoCancel {
   // [self.clipMusicDetailsView seekTo:0];
}
-(void)cutVideoDoneWithRange:(NSRange)r {
    [self.replayCtrl setReplayValidRange:r];
    _cutTime = r.location;
    _cutTopTime = r.length;
//    NSLog(@"111self.clipMusicDetailsView.musicStartTime----%f",self.clipMusicDetailsView.musicStartTime);
//    //self.clipMusicDetailsView.musicStartTime = 1.00f *r.location /1000  + _lastMusicTime;
    CGFloat time = 1.00f *r.location /1000 - 1.00f *_lastCutTime /1000 + _lastMusicTime;
    self.clipMusicDetailsView.musicStartTime = time;
//    NSLog(@"222self.clipMusicDetailsView.musicStartTime----%f",self.clipMusicDetailsView.musicStartTime);
//
    self.clipMusicDetailsView.videoTime = 1.00f *r.length / 1000   ;
//    NSLog(@"333self.clipMusicDetailsView.musicStartTime----%f",self.clipMusicDetailsView.musicStartTime);
//
    _lastMusicTime = time;
    if (_lastMusicTime < 0) {
        _lastMusicTime = 0;
    }
   
  //  [self.clipMusicDetailsView seekTo:0 ];
   // [self.clipMusicDetailsView end];
    //[self.clipMusicDetailsView seekTo:r.location /1000 + 0.5];

     [self.replayCtrl setMusicPath:_musicPath andRange:NSMakeRange(_lastMusicTime * 1000 +r.location, 999 * 1000)];
    [self replay];
    _lastCutTime = r.location;
}
- (void) testClipBtn_OnClicked:(id)sender
{
    [self.replayCtrl pauseVideo];
    
    CutVideoController * cut = [[CutVideoController alloc] init];
    self.navigationController.delegate = cut;
    cut.vPath = self.vPath;
    cut.delegate = self;
    cut.fromeView = self.mediaContainer;
    NSLog(@"self.totalTime--- %f ",self.totalTime);
    cut.videoTotalTime = self.totalTime;
   
    [self.navigationController pushViewController:cut animated:YES];
    
//    CutVideoController * cut = [[CutVideoController alloc] init];
//    cut.vPath = self.vPath;
//    cut.delegate = self;
//    [self.navigationController pushViewController:cut animated:false];
    
    
//    if (self.testMusicBtn.selected == YES) {
//        return;
//    }
//    self.testClipBtn.selected = !self.testClipBtn.selected;
//    if( self.testClipBtn.selected ) {
//        [UIView animateWithDuration:.5 animations:^{
//            self.clipView.frame = CGRectMake(0, SCREEN_HEIGHT - 150 , SCREEN_WIDTH, 150);
//        }];
//    } else {
//        [self clipVViewDown];
//
//    }
//
//    self.testClipBtn.selected = !self.testClipBtn.selected;
//
//    if( self.testClipBtn.selected ) {
//        [self.replayCtrl setReplayValidRange:NSMakeRange(3000, 6000)];
//        [self.view makeToast:@"测试剪切3-9秒" duration:2.0f position:CSToastPositionCenter];
//    }
//    else {
//        [self.replayCtrl setReplayValidRange:NSMakeRange(0, 0)];
//    }
//
//    return;
}

- (void) testMusicBtn_OnClicked:(id)sender
{
    
    if (self.testClipBtn.selected == YES) {
        return;
    }
    self.testMusicBtn.selected = !self.testMusicBtn.selected;
    if( self.testMusicBtn.selected ) {
        [UIView animateWithDuration:.5 animations:^{
            self.clipMusicView.frame = CGRectMake(0, SCREEN_HEIGHT - 150 - 50 , SCREEN_WIDTH, 150);
        }];
       // [self.clipMusicDetailsView showAgain];
    } else {
        [self clipMViewDown];
        //[self.clipMusicDetailsView end];
    }
    
//    self.testMusicBtn.selected = !self.testMusicBtn.selected;
//
//    if( self.testMusicBtn.selected ) {
//        //NSString * musicPath = [[NSBundle mainBundle] pathForResource:@"hongzhaoyuan.mp3" ofType:nil];
//        NSString * musicPath = [[NSBundle mainBundle] pathForResource:@"love.mp3" ofType:nil];
//        [self.replayCtrl setMusicPath:musicPath andRange:NSMakeRange(0, 0)];
//    }
//    else {
//        [self.replayCtrl setMusicPath:nil andRange:NSMakeRange(0, 0)];
//    }
//
//    [self.replayCtrl stopVideo];
//
//    [self.replayCtrl playVideo];
//    [self.replayCtrl pauseVideo];
//
//    return;
}


- (void) mergeBtn_OnClicked:(id)sender
{
   // [self.view makeToastActivity:CSToastPositionCenter];
    [self.replayCtrl saveConfigToDisk];
    MergeViewController *vc = [MergeViewController new];
    
    vc.fromRecord = _fromRecord;
    vc.draftsFolder = _draftsFolder;
    if (_headerImageArr.count > 6) {
        vc.imageArr = _headerImageArr;
    }else{
        return;
    }
    [self.navigationController pushViewController:vc animated:YES];
    return;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
       
        NSMutableArray * m = [[NSMutableArray alloc] init];
        NSString * root = [[NSBundle mainBundle] resourcePath];
        for( int i=0; i<=237; i++ ) {
            NSString * path = [NSString stringWithFormat:@"%@/animation/wudao/out_%03d.png", root, i];
            [m addObject:path];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
        self.testExportLabel.hidden = NO;
        });
 //       [self.replayCtrl setAnimationList:m atInterval:60 toRect:CGRectMake(20, 20, 92, 160)];
        _exportReplayFile = YES;
        NSString * path = [NSString stringWithFormat:@"%@/sample.mp4", NSTemporaryDirectory()];
        NSString * path1 = [NSString stringWithFormat:@"%@/allKeySample.mp4", NSTemporaryDirectory()];
        //[self.replayCtrl exportReplayFile:path withSize:CGSizeMake(576,1024) andBitrate:2500000];
         BOOL success = [self.replayCtrl exportReplayFile:path andKeyFile:path1 withSize:CGSizeMake(544, 960) andBitrate:2500000];
//        if (!self.fromRecord) {
//             dispatch_async(dispatch_get_main_queue(), ^{
//            [self.view hideToastActivity];
//
//            [self.view makeToast:@"视频已保存" duration:2.0f position:CSToastPositionCenter];
//            return;
//            });
//        }
        if (success) {
            _exportReplayFile = NO;
            NSString * n = path;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.testExportLabel.hidden = YES;
                self.testExportLabel.text = @"";
            });
            
            
            if( [[NSFileManager defaultManager] fileExistsAtPath:n] ) {
                NSLog(@"file ok");
                usleep(300*1000);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(n.length > 0){
                    NSURL *outputURL = [NSURL URLWithString:n];
                    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                        [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (error) {
                                    NSLog(@"recordFile failed %@ ", n);
                                } else {
                                    NSLog(@"recordFile succeed %@ ", n);
                                }
                                
                               // [self.view hideToastActivity];
                                
                                [self.view makeToast:@"视频已经导出到相册" duration:2.0f position:CSToastPositionCenter];
                                
                                
                                //[self performSelector:@selector(delayPopup) withObject:nil afterDelay:2.5f];
                            });
                        }];
                    }
                }
                
            });
        }
        
        
    });
    
    return;
}

- (void) slowBtn_OnClicked:(id)sender
{
    [self.slowSlideView setHidden:!self.slowSlideView.hidden];
}

- (void) backBtn_OnClicked:(id)sender
{
    //[self dismissViewControllerAnimated:true completion:nil];
    //[self.navigationController popToRootViewControllerAnimated:true];
    [self.navigationController popViewControllerAnimated:true];
    
    return;
}

- (void) pickBtn_OnClicked:(id)sender
{
    /** 小于当前需要获取的图片数量 */
    if (self.imageArr.count < 7) {
        return;
    }
    HeadPortraitViewController *vc = [[HeadPortraitViewController alloc]init];
    vc.view.frame = self.view.frame;
    vc.fromeView = self.mediaContainer;
    vc.videoTotalTime = &(_totalTime);
    vc.imageArr = self.imageArr;
    vc.vPath = self.vPath;
    __weak typeof(self)weakSelf = self;
    vc.headerImageArrBlock = ^(NSArray *headerImageArr) {
        weakSelf.headerImageArr = headerImageArr;
    };
    self.navigationController.delegate = vc;
    [self.navigationController pushViewController:vc animated:YES];
    
  //  [self show];
    
    
//    //调用系统相册的类
//    UIImagePickerController *pickerController = [[UIImagePickerController alloc]init];
//
//    //设置选取的照片是否可编辑
//    pickerController.allowsEditing = YES;
//    //设置相册呈现的样式
//    pickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;//图片分组列表样式
//    //照片的选取样式还有以下两种
//    //UIImagePickerControllerSourceTypePhotoLibrary,直接全部呈现系统相册
//    //UIImagePickerControllerSourceTypeCamera//调取摄像头
//
//    //选择完成图片或者点击取消按钮都是通过代理来操作我们所需要的逻辑过程
//    pickerController.delegate = self;
//    //使用模态呈现相册
//    [self presentViewController:pickerController animated:YES completion:^{
//
//    }];
//
//    return;
}

//选择照片完成之后的代理方法
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    //info是所选择照片的信息
    //    UIImagePickerControllerEditedImage//编辑过的图片
    //    UIImagePickerControllerOriginalImage//原图
    
    NSLog(@"%@",info);
    //刚才已经看了info中的键值对，可以从info中取出一个UIImage对象，将取出的对象赋给按钮的image
    UIImage *resultImage = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    //如果按钮创建时用的是系统风格UIButtonTypeSystem，需要在设置图片一栏设置渲染模式为"使用原图"
    
    //使用模态返回到软件界面
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self.pickBtn setImage:resultImage forState:UIControlStateNormal];

    self.pickedImage = resultImage;
    [self.replayCtrl setVideoCoverImage:resultImage];
    
    return;
}

//点击取消按钮所执行的方法
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{    
    //这是捕获点击右上角cancel按钮所触发的事件，如果我们需要在点击cancel按钮的时候做一些其他逻辑操作。就需要实现该代理方法，如果不做任何逻辑操作，就可以不实现
    [picker dismissViewControllerAnimated:YES completion:nil];
}

// 当滑块设置的时候，滑块所在位置开始3秒内做慢动作
- (void) onSlowSliderValueChanged:(float)v
{
    int dur = [self.replayCtrl getDuration];
    int s = dur * v;
    int e = dur * v + 3000;
    [self.replayCtrl setSlowPlayFrom:s toTime:e];
}
-(UILabel *)testExportLabel {
    if (!_testExportLabel) {
        _testExportLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2 - 100, SCREEN_HEIGHT / 2 - 30, 200, 60)];
        _testExportLabel.font = [UIFont systemFontOfSize:14];
        _testExportLabel.backgroundColor = [UIColor blackColor];
        _testExportLabel.textColor = [UIColor whiteColor];
        _testExportLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_testExportLabel];
        _testExportLabel.hidden = YES;
    }
    return _testExportLabel;
}
- (void) notifyReplayStatus:(NSString*)evt withInfo:(NSDictionary*)info
{

    // 播放器回调
    if( [evt isEqualToString:kGTVReplayEventCompleted] ) {
        self.mediaControl.isEventCompleted = YES;
        [self.replayCtrl pauseVideo];
        NSLog(@"evt ----   kGTVReplayEventCompleted");
    }
    // 预读完毕，获取到文件时长
    if( [evt isEqualToString:kGTVReplayEventPrepared] ) {
        NSLog(@"evt ----   kGTVReplayEventPrepared");
    }
    // 播放失败
    if( [evt isEqualToString:kGTVReplayEventError] ) {
        NSLog(@"evt ----   kGTVReplayEventError");
    }
    // 合成进度更新(返回percent)
    if( [evt isEqualToString:kGTVReplayEventExportStatus] ) {
        NSLog(@"evt ----   kGTVReplayEventExportStatus");
        if (info) {
            NSString *percent = info[@"percent"];
            NSLog(@"percent    %@",percent);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _testExportLabel.text = percent;
            });
           
        }
    }
    
    //特效
    if( [evt isEqualToString:kGTVReplayEffectKeyStart] ) {
        NSLog(@"evt ----   kGTVReplayEffectKeyStart");
    }
    if( [evt isEqualToString:kGTVReplayEffectKeyStop] ) {
        NSLog(@"evt ----   kGTVReplayEffectKeyStop");
    }
    if( [evt isEqualToString:kGTVReplayEffectKeyName] ) {
        NSLog(@"evt ----   kGTVReplayEffectKeyName");
    }
    
    
}

-(void)show {
    [UIView animateWithDuration:.5 animations:^{
        _videoFrameView.hidden = NO;
    }];
}
-(void)dismiss {
    [UIView animateWithDuration:.5 animations:^{
        _videoFrameView.hidden = YES;
    }];
}
-(UIView *)videoFrameView{
    if (!_videoFrameView) {
        _videoFrameView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _videoFrameView.backgroundColor = [UIColor blackColor];
        _videoFrameView.tag = 55;
        _videoFrameView.hidden = YES;
        
        
      
        
    
        [self addSubViews];
        
        UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        iv.tag = 9870;
        iv.center = self.view.center;
        iv.backgroundColor = [UIColor yellowColor];
        // [self movieToImage];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(nothing)];
        [self.videoFrameView addGestureRecognizer:tap];
        _videoFrameView.userInteractionEnabled = YES;
        
        [self.view addSubview:_videoFrameView];
    }
    return _videoFrameView;
}
-(void)nothing{
    NSLog(@"nothing");
}
-(void)addSubViews{
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20, SCREEN_HEIGHT - 40, 80, 20)];
    [btn setTitle:@"取消" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    [_videoFrameView addSubview:btn];
    
    UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 100, SCREEN_HEIGHT - 40, 80, 20)];
    [btn1 setTitle:@"确定" forState:UIControlStateNormal];
    
    [btn1 addTarget:self action:@selector(pickBtn_WithFrame:) forControlEvents:UIControlEventTouchUpInside];
    
    [_videoFrameView addSubview:btn1];
    
    UIImageView *bigIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width * 11 / 9)];
    self.animView = bigIV;
    [_videoFrameView addSubview:bigIV];
    bigIV.tag = 201;
    
    self.videoFrameRateView = [[VideoFrameView alloc]initWithFrame:CGRectMake(16, SCREEN_HEIGHT - 60 - 60-1, SCREEN_WIDTH-32, 60 +2)];
    [self.videoFrameRateView initViewsWithNum:7];
    self.videoFrameRateView.backgroundColor = [UIColor blackColor];
    //[self.videoFrameRateView setImagetoTimeLine:[UIImage imageNamed:@"IMG_0455.JPG"] withWidth:4];
    self.videoFrameRateView.delegate = self;
    //self.videoFrameRateView.totalTime = self.totalTime;
    [self.videoFrameView addSubview:self.videoFrameRateView];
    
    
    UILabel *l3 = [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 60 - 60-1 - 20, SCREEN_WIDTH, 15)];
    l3.textColor = [UIColor whiteColor];
    l3.text = @"0";
    l3.textAlignment = NSTextAlignmentCenter;
    [_videoFrameView addSubview:l3];
    timeLabel = l3;
    
    
}
-(void)setImageForDraggingTime:(CGFloat)nowTime {
    
    
    timeLabel.text = [NSString stringWithFormat:@"%.2f",nowTime];
    
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_yuv_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(nowTime * 1000, 1000);
        NSArray * res = [GTVideoTool syncGrabJPEG:self.vPath toFolder:rootfolder withinRange:r andNumLimit:1];
        
        NSDictionary * elem = nil;
        NSMutableArray * l = [[NSMutableArray alloc] init];
        for( int i=0; i<res.count; i++ ) {
            
            elem = [res objectAtIndex:i];
            NSString * fpath = ((NSString*)[elem objectForKey:@"filepath"]);
            NSData * d = [NSData dataWithContentsOfFile:fpath];
            UIImage * img = [UIImage imageWithData:d];
            if (img == nil) {
                return;
            }
            [l addObject:img];
        }
        
       // dispatch_async(dispatch_get_main_queue(), ^{
            
          //  [self.view hideToastActivity];
    [self.animView stopAnimating];
            if (l.count > 0) {
            [self.animView setImage:l[0]];
            [self.videoFrameRateView loadSmallImage:(UIImage *)l[0]];
            }
//            self.animView.animationImages = l;
//            self.animView.animationDuration = 0.8f;
//            self.animView.animationRepeatCount = 100;
//            if (l.count > 0) {
//                [self.videoFrameRateView loadSmallImage:(UIImage *)l[0]];
//
//            }
            
           // [self.animView startAnimating];//开始动画
     //   });
   // });
    
}
-(void)VideoFrameViewDidEndWithTime:(CGFloat)time {
    thumbList_index = time / (self.totalTime /12 );
    
    timeLabel.text = [NSString stringWithFormat:@"%.2f",time];
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_yuv_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(time * 1000, 1000);
        NSArray * res = [GTVideoTool syncGrabJPEG:self.vPath toFolder:rootfolder withinRange:r andNumLimit:7];
        
        NSDictionary * elem = nil;
        NSMutableArray * l = [[NSMutableArray alloc] init];
        for( int i=0; i<res.count; i++ ) {
            
            elem = [res objectAtIndex:i];
            NSString * fpath = ((NSString*)[elem objectForKey:@"filepath"]);
            NSData * d = [NSData dataWithContentsOfFile:fpath];
            UIImage * img = [UIImage imageWithData:d];
            if (img == nil) {
                return;
            }
            [l addObject:img];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view hideToastActivity];
            
            self.animView.animationImages = l;
            self.animView.animationDuration = 0.8f;
            self.animView.animationRepeatCount = 100;
            if (l.count > 0) {
                [self.videoFrameRateView loadSmallImage:(UIImage *)l[0]];
                
            }
            
            [self.animView startAnimating];//开始动画
        });
    });
}




-(void) getTime {
    [self loadImages];
}
- (void) loadImages
{
    NSTimeInterval duration = [self.replayCtrl getDuration] * 1.0f / 1000.0f;
    NSInteger intDuration = duration ;
    self.totalTime = _cutTopTime = (CGFloat)duration;
    _topTime = intDuration;
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_pic_grab_two", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    
    
    [self.videoFrameView makeToastActivity:CSToastPositionCenter];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        
        
        //
        NSRange r = NSMakeRange(0, intDuration * 1000);
        NSArray * res = [GTVideoTool syncGrabJPEG:_vPath toFolder:rootfolder withinRange:r andNumLimit:7];
        self.thumbList = res;
        NSMutableArray *imageArr = [NSMutableArray array];
        dispatch_async(dispatch_get_main_queue(), ^{
            for (int i = 0 ; i < res.count; i++) {
                NSDictionary * elem = nil;
                elem = (NSDictionary*)[res objectAtIndex:i];
                //     UIImageView *v = [self.view viewWithTag:301 + i];
                
                NSString * fpath = [elem objectForKey:@"filepath"];
                NSData * d = [NSData dataWithContentsOfFile:fpath];
                UIImage * img = [UIImage imageWithData:d];
                
                // if (i < 7) {
//                UIImageView *v1 = [self.view viewWithTag:101 + i];
//                [v1 setImage:img];
                [self.videoFrameRateView loadImages:img withIndex:i];
                [imageArr addObject:img];
                // }
                //*************
                //            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 25)];
                //             NSString * tstr = [elem objectForKey:@"timestamp"];
                //            label.font = [UIFont systemFontOfSize:10];
                //            label.text = [NSString stringWithFormat:@"%@ms", tstr];
                //            [v addSubview:label];
                // [v1 addSubview:label];
                
                [self.clipVideoView clipVideoViewAddImage:img withIndex:i];
                //     [v setImage:img];
                
                if (i == 0) {
                    UIImageView *ivBig = [self.view viewWithTag:201];
                    [ivBig setImage:img];
                    
                    [self.videoFrameRateView loadSmallImage:img];
                }
                
            }
            self.imageArr = imageArr;
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.videoFrameView hideToastActivity];
            
        });
    });
}

-(void)loadAnimationViewWithTime:(CGFloat)time {
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_yuv_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(time * 1000, 1000);
        NSArray * res = [GTVideoTool syncGrabJPEG:self.vPath toFolder:rootfolder withinRange:r andNumLimit:7];
        
        NSDictionary * elem = nil;
        NSMutableArray * l = [[NSMutableArray alloc] init];
        for( int i=0; i<res.count; i++ ) {
            
            elem = [res objectAtIndex:i];
            NSString * fpath = ((NSString*)[elem objectForKey:@"filepath"]);
            NSData * d = [NSData dataWithContentsOfFile:fpath];
            UIImage * img = [UIImage imageWithData:d];
            if (img == nil) {
                return;
            }
            [l addObject:img];
        }
        self.headerImageArr = l;
    });
}

-(NSArray*)creatMusicData
{
    
    
    NSMutableArray *array = [NSMutableArray array];
    
    MusicData *effect = [[MusicData alloc] init];
    effect.name = @"原始";
    // effect.iconPath = [[NSBundle mainBundle] pathForResource:@"nilMusic" ofType:@"png"];
    effect.isSelected = YES;
    [array addObject:effect];
    
    NSString *m1 = @"hongzhaoyuan";
    NSString *m2 = @"love";
    NSString *m3 = @"lvxing";
    NSString *m4 = @"panama";
    NSString *m5 = @"sorry";
    NSString *m6 = @"sweet";
    NSString *m7 = @"witness";
    
    NSArray *mArr = [NSArray arrayWithObjects:m1,m2,m3,m4,m5,m6,m7,nil];
    
    for (int i = 0 ; i < 7; i++) {
        MusicData *effect1 = [[MusicData alloc] init];
        NSString *str = mArr[i];
        effect1.musicPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@.mp3",str] ofType:nil];
        //   effect.iconPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"icon%d",i] ofType:@"png"];
        effect1.name = mArr[i];
        [array addObject:effect1];
    }
    
    
    
    return array;
}
#pragma mark -- UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

// 告诉系统每组多少个
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return _musicAry.count;
    
    
    
}

// 告诉系统每个Cell如何显示
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // 1.从缓存池中取
    
    static NSString *cellID2 = @"MyCollectionCell2";
    MusicItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID2 forIndexPath:indexPath];
    MusicData* data = [_musicAry objectAtIndex:indexPath.row];
    //UIImage* image = [UIImage imageWithContentsOfFile:data.iconPath];
    // cell.iconImgView.image = image;
    cell.iconImgView.backgroundColor = [UIColor yellowColor];
    cell.nameLabel.text = data.name;
    cell.CheckMarkImgView.hidden = !data.isSelected;
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i = 0 ; i < _musicAry.count; i++) {
        if (i != indexPath.row) {
            MusicData* data = [_musicAry objectAtIndex:i];
            data.isSelected = NO;
        }else{
            MusicData* data = [_musicAry objectAtIndex:i];
            data.isSelected = YES;
        }
        
    }
    MusicData* data1 = [_musicAry objectAtIndex:indexPath.row];
    _mPath = data1.musicPath;
    
    
    //[self.clipMusicDetailsView end];
    //[self.clipMusicDetailsView showAgain];
    NSRange testRange = NSMakeRange(0, 0);
    [self.replayCtrl setMusicPath:_mPath andRange:testRange];
    
    if (indexPath.row == 0 ) {
        self.clipMusicDetailsView.musicTotalTime = 0;
    }else{
        self.clipMusicDetailsView.musicTotalTime = [self durationWithMusic:_mPath];
    }
    
    [self.replayCtrl playVideo];
    [self replay];
    
   // [self.clipMusicDetailsView startAnimation];
}
- (float)durationWithMusic:(NSString *)mPath{
    
    AVURLAsset* audioAsset =[AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:mPath] options:nil];
    CMTime audioDuration = audioAsset.duration;
    float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    return audioDurationSeconds;
    
}


- (void) pickBtn_WithFrame:(id)sender
{

    
    if (thumbList_index < 0 || thumbList_index >= self.thumbList.count) {
        
        [self.view makeToast:@"未选择" duration:1.5f position:CSToastPositionCenter];
                return;
    }
    
    NSDictionary * elem = [self.thumbList objectAtIndex:self.selectedIndex];
    int ts = ((NSString*)[elem objectForKey:@"timestamp"]).intValue;
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    // 抽取动画帧
    NSString * m = _vPath;
    
    NSString * rootfolder = [NSString stringWithFormat:@"%@/gtv_pic_grab_temp", NSTemporaryDirectory()];
    
    // 清除旧文件
    if ([[NSFileManager defaultManager] removeItemAtPath:rootfolder error:NULL]) {
        NSLog(@"Removed successfully");
    }
    
    if( [[NSFileManager defaultManager] fileExistsAtPath:rootfolder] == false ) {
        [[NSFileManager defaultManager] createDirectoryAtPath:rootfolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSRange r = NSMakeRange(ts, 1000);
        NSArray * res = [GTVideoTool syncGrabRawYUV:m toFolder:rootfolder withinRange:r andNumLimit:5];
        
        // 生成webp
        NSString * outwebp = [NSString stringWithFormat:@"%@/test.webp", NSTemporaryDirectory()];
        char * inputlist[10];
        for( int i=0; i<res.count; i++ ) {
            NSDictionary * d = (NSDictionary*)[res objectAtIndex:i];
            NSString * filepath = [d objectForKey:@"filepath"];
            inputlist[i] = (char *)[filepath cStringUsingEncoding:NSUTF8StringEncoding];
        }
        char * output_file = (char*)[outwebp cStringUsingEncoding:NSUTF8StringEncoding];
        img2webp(inputlist, 5, output_file);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view hideToastActivity];
            
            [self.view makeToast:@"test.webp已生成至temp目录，请从xcode下查看" duration:2.0f position:CSToastPositionCenter];
        });
    });
    
    return;
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
