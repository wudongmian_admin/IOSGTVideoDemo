//
//  SlowSlideView.h
//  gtv
//
//  Created by gtv on 2017/11/11.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SlowSlideViewDelegate <NSObject>

@optional

- (void) onSlowSliderValueChanged:(float)v;

@end

@interface SlowSlideView : UIView

@property (nonatomic, weak) id<SlowSlideViewDelegate> delegate;

- (float) getSlowPoint;

@end
