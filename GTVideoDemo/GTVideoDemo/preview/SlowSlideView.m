//
//  SlowSlideView.m
//  gtv
//
//  Created by gtv on 2017/11/11.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import "SlowSlideView.h"

@interface SlowSlideView()

@property (nonatomic, strong) UIButton * settingBtn;
@property (nonatomic, strong) UISlider * slider;

@end

@implementation SlowSlideView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self ) {
        self.settingBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, frame.size.height)];
        self.slider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    }
    
    [self.slider setThumbImage:[UIImage imageNamed:@"slide"] forState:UIControlStateNormal];
    [self.slider setMaximumTrackTintColor:[UIColor whiteColor]];
    [self.slider setMinimumTrackTintColor:[UIColor whiteColor]];
    
    [self addSubview:self.settingBtn];
    [self addSubview:self.slider];
    
    [self.slider setValue:0.5];
    
    [self.slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    return self;
}

- (void) sliderValueChanged:(id)sender
{
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(onSlowSliderValueChanged:)] ) {
        [self.delegate onSlowSliderValueChanged:self.slider.value];
    }
}

- (void) settingBtn_OnClicked:(id)sender
{
    [self.slider setHidden:!self.slider.hidden];
    
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(onSlowSliderValueChanged:)] ) {
        if( self.slider.hidden == false )
            [self.delegate onSlowSliderValueChanged:self.slider.value];
        else
            [self.delegate onSlowSliderValueChanged:-1];
    }
    
    return;
}

- (float) getSlowPoint
{
    if( [self.slider isHidden] || [self isHidden] )
        return -1.0f;
    
    return self.slider.value;
}

@end
