//
//  RecordStatusView.h
//  gtv
//
//  Created by gtv on 2017/11/11.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RecordStatusViewDelegate <NSObject>

- (void) finishedRecord;
- (int) currentRecordMilliSeconds;

@end

@interface RecordStatusView : UIView

@property (nonatomic, weak) id<RecordStatusViewDelegate> delegate;

- (void) reset;
- (void) restart;
//- (void) upateSeconds:(int)secs;
- (void) stop;

@end
