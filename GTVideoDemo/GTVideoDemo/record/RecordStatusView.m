//
//  RecordStatusView.m
//  gtv
//
//  Created by gtv on 2017/11/11.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import "ComDef.h"
#import "RecordStatusView.h"

@interface RecordStatusView() {
    int limit;
    int notified;
}

@property (nonatomic, strong) UILabel * lbl;
@property (nonatomic, strong) UIView * progress;

@property (nonatomic, strong) NSTimer * refreshTimer;

@end

@implementation RecordStatusView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        // 一个label，一个进度条
        self.lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height, frame.size.width, frame.size.height)];
        [self.lbl setBackgroundColor:[UIColor clearColor]];
        [self.lbl setTextAlignment:NSTextAlignmentCenter];
        [self.lbl setFont:[UIFont systemFontOfSize:14.0f]];
        [self.lbl setTextColor:[UIColor redColor]];
        
        [self addSubview:self.lbl];
      //  [self.lbl setHidden:true];
        
        self.progress = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height - 3.0f, frame.size.width, 3.0f)];
        self.progress.backgroundColor = RGBToColor(239, 75, 129);
        
        [self addSubview:self.progress];
        
        [self upateSeconds:0];
    }
    
    return self;
}

- (void) reset
{
    [self upateSeconds:0];
}

- (void) restart
{
    if( self.refreshTimer != nil )
       [self.refreshTimer invalidate];
    
    self.refreshTimer = [NSTimer timerWithTimeInterval:0.3f target:self selector:@selector(refreshTimeout:) userInfo:nil repeats:true];
    [[NSRunLoop mainRunLoop] addTimer:self.refreshTimer forMode:NSDefaultRunLoopMode];
    
    limit = 0;
    notified = 1;
    
    [self upateSeconds:limit];
    
    return;
}

- (void) stop
{
    if( self.refreshTimer == nil )
        return;
    
    [self.refreshTimer invalidate];
    self.refreshTimer = nil;
    
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(finishedRecord)] ) {
        
        @synchronized(self.progress) {
            if( notified > 0 ) {
                [self.delegate finishedRecord];
                notified --;
            }
        }
    }
}

- (void) refreshTimeout:(NSTimer*)t
{
    if( t != self.refreshTimer )
        return;
    
    // 获取当前已经录制的时间
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(currentRecordMilliSeconds)] ) {
        
        int milli = [self.delegate currentRecordMilliSeconds];
        [self upateSeconds:milli];
        
        if( milli >= 60000 ) {
            [self stop];
        }
    }
    else {
        
        limit --;
        [self upateSeconds:limit];
        
        if( limit <= 0 ) {
            [self stop];
        }
    }
}

- (void) upateSeconds:(int)milli
{
    [self.lbl setText:[NSString stringWithFormat:@"00:%02d", milli/1000]];
    float width = ((float)milli) * self.frame.size.width / 60000.0f;
    self.progress.frame = CGRectMake(0, self.frame.size.height - 3.0f, width, 2.0f);
    
    return;
}

@end
