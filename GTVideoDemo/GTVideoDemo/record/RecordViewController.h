//
//  RecordViewController.h
//  gtv
//
//  Created by gtv on 2017/9/29.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordViewController : UIViewController

@property (nonatomic, assign)BOOL   _fromDrafts;
@property (nonatomic, strong) NSString * draftsFolder;

@end
