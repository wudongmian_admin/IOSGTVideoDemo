//
//  RecordViewController.m
//  gtv
//
//  Created by gtv on 2017/9/29.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import "GTVideoLib/GTVideoLib.h"

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "RecordViewController.h"
#import "DPHorizontalScrollView.h"

#import "CountDownView.h"
#import "RecordStatusView.h"
#import "UIView+Toast.h"

#import "ComDef.h"
#import "GTVConfig.h"

#import "MusicSelectView.h"
#import "PreviewViewController.h"

enum {
    
    E_REC_TYPE_NONE = 0,
    E_REC_TYPE_DELAY,
    E_REC_TYPE_HAND,
};

enum {
    
    E_REC_STS_IDLE = 0,
    E_REC_STS_LAUNCHING,
    E_REC_STS_RUNNING,
    E_REC_STS_PAUSE,
    E_REC_STS_OVER,
};

#define SET_MODE_INSTA_FILTER   0
#define SET_MODE_EFFECT_FACE    1
#define SET_MODE_BEAUTY_ADJUST  2
#define SET_MODE_SPEED_SELECT   3
#define SET_MODE_MUSIC_SELECT   4

#define SETTING_BTN_HEIGHT          40
#define SETTING_CONTENT_HEIGHT      90
#define SETTING_PANEL_HEIGHT        (SETTING_BTN_HEIGHT+SETTING_CONTENT_HEIGHT)
#define SETTING_SCROLL_ITEM_WIDTH   60

// insta 特效列表
//static NSString * picList[] = {
//    @"if_normal.png", @"if_1977.png", @"if_amaro.png", @"if_brannan.png", @"if_earlybird.png",
//    @"if_hefe.png", @"if_hudson.png", @"if_inkwell.png", @"if_lomofi.png", @"if_lordkelvin.png",
//    @"if_nashville.png", @"if_rise.png", @"if_sierra.png", @"if_sutro.png", @"if_toaster.png", @"if_valencia.png",
//    @"if_walden.png", @"if_xproii.png"
//};
//static NSString * picList[] = {
//    @"normal.png", @"IMG_0455.JPG", @"IMG_0456.JPG", @"IMG_0457.JPG",
//    @"IMG_0458.JPG", @"IMG_0459.JPG", @"IMG_0460.JPG", @"IMG_0461.JPG", @"IMG_0462.JPG", @"IMG_0463.JPG",
//    @"IMG_0464.JPG", @"IMG_0465.JPG", @"IMG_0466.JPG", @"IMG_0467.JPG", @"IMG_0468.JPG", @"IMG_0469.JPG", @"IMG_0470.JPG",
//    @"IMG_0471.JPG"
//};
//static NSString * filterList[] = {
//    DEF_GTV_FILTER_NONE, DEF_GTV_FILTER_IF1977, DEF_GTV_FILTER_IFAMARO, DEF_GTV_FILTER_IFBRANNAN, DEF_GTV_FILTER_IFEARLY,
//    DEF_GTV_FILTER_IFHEFE, DEF_GTV_FILTER_IFHUDSON, DEF_GTV_FILTER_IFINKWELL, DEF_GTV_FILTER_IFLOMOFI, DEF_GTV_FILTER_IFLORDKELVIN,
//    DEF_GTV_FILTER_IFNASHVILLE, DEF_GTV_FILTER_IFRISE, DEF_GTV_FILTER_IFSIERRA, DEF_GTV_FILTER_IFSUTRO, DEF_GTV_FILTER_IFTOASTER,
//    DEF_GTV_FILTER_IFVALENCIA, DEF_GTV_FILTER_IFWALDEN, DEF_GTV_FILTER_IFXPROII
//};
static NSString * picList[] = {
    @"normal.png",
    @"IMG_0455.JPG", @"IMG_0456.JPG", @"IMG_0457.JPG", @"IMG_0458.JPG",
    @"IMG_0459.JPG", @"IMG_0460.JPG", @"IMG_0461.JPG", @"IMG_0462.JPG",
    @"IMG_0463.JPG", @"IMG_0464.JPG", @"IMG_0465.JPG", @"IMG_0466.JPG",
    @"IMG_0467.JPG", @"IMG_0468.JPG", @"IMG_0469.JPG", @"IMG_0470.JPG",
    
    @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG",
    @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG",
    @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG",
    @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG", @"IMG_0471.JPG",
    
    @"IMG_0471.JPG", @"IMG_0471.JPG"
};

static NSString * filterNameList[] = {
    @"无",
    
    @"日系-森林", @"日系-奶茶", @"日系-日出", @"日系-柔美",
    @"日系-甜美", @"日系-伊豆", @"日系-樱花", @"日系-自然",
    
    @"海-祝福", @"海-岛国", @"海-等候", @"海-风铃",
    @"海-海风", @"海-日记", @"海-物语", @"海-幸福",
    
    @"电影-怀旧", @"电影-经典", @"电影-水墨", @"电影-西部",
    @"电影-暮色", @"电影-荒漠", @"电影-瓦伦", @"电影-布朗",
    
    @"DARK", @"LIGHT",
    
    @"AMARO", @"HUDSON", @"LOMO", @"RISE",
    @"SIERRA", @"SUTRO", @"TOASTER", @"WALDEN", @"XPROII"
};

static NSString * filterList[] = {
    DEF_GTV_FILTER_NONE,
    
    DEF_GTV_FILTER_RIXI_SENLIN, DEF_GTV_FILTER_RIXI_NAICHA, DEF_GTV_FILTER_RIXI_RICHU, DEF_GTV_FILTER_RIXI_ROUMEI,
    DEF_GTV_FILTER_RIXI_TIANMEI, DEF_GTV_FILTER_RIXI_YIDOU, DEF_GTV_FILTER_RIXI_YINGHUA, DEF_GTV_FILTER_RIXI_ZIRAN,
    
    DEF_GTV_FILTER_HAI_ZUFU, DEF_GTV_FILTER_HAI_DAOGUO, DEF_GTV_FILTER_HAI_DENGHOU, DEF_GTV_FILTER_HAI_FENGLING,
    DEF_GTV_FILTER_HAI_HAIFENG, DEF_GTV_FILTER_HAI_RIJI, DEF_GTV_FILTER_HAI_WUYU, DEF_GTV_FILTER_HAI_XINGFU,
    
    DEF_GTV_FILTER_IF1977, DEF_GTV_FILTER_IFHEFE, DEF_GTV_FILTER_IFINKWELL, DEF_GTV_FILTER_IFLORDKELVIN,
    DEF_GTV_FILTER_IFNASHVILLE, DEF_GTV_FILTER_IFEARLY, DEF_GTV_FILTER_IFVALENCIA, DEF_GTV_FILTER_IFBRANNAN,
    
    DEF_GTV_FILTER_DARKGRAY, DEF_GTV_FILTER_LIGHTGRAY,
    
    DEF_GTV_FILTER_IFAMARO, DEF_GTV_FILTER_IFHUDSON, DEF_GTV_FILTER_IFLOMOFI, DEF_GTV_FILTER_IFRISE,
    DEF_GTV_FILTER_IFSIERRA, DEF_GTV_FILTER_IFSUTRO, DEF_GTV_FILTER_IFTOASTER, DEF_GTV_FILTER_IFWALDEN, DEF_GTV_FILTER_IFXPROII
};

// 贴纸列表
static NSString * effectList[] = {
    @"-", @"wuchang", @"yiyumaoer", @"mother", @"cap", @"devil", @"ear", @"flower", @"fox", @"frog", @"hats", @"hats2", @"horns", @"hulu", @"panda", @"rabbit", @"feather", @"hair", @"heart", @"tree", @"petals"
};
static NSString * effectNameList[] = {
    @"no_eff.png", @"bunny.png", @"bunny.png", @"rabbiteating.png", @"rabbiteating.png",
    @"rabbiteating.png", @"rabbiteating.png", @"rabbiteating.png", @"rabbiteating.png", @"rabbiteating.png", @"rabbiteating.png", @"rabbiteating.png"
};
static NSString * effectTitleList[] = {
    @"无", @"wuchang", @"yiyumaoer", @"mother", @"cap", @"devil", @"ear", @"flower", @"fox", @"frog", @"hats", @"hats2", @"horns", @"hulu", @"panda", @"rabbit", @"feather", @"hair", @"heart", @"tree", @"petals"
};


// 音乐列表
static NSString * musicList[] = {
    @"-", @"panama.mp3", @"lvxing.mp3", @"hongzhaoyuan.mp3", @"love.mp3"
};
static NSString * musicNameList[] = {
    @"无", @"音乐一", @"音乐二", @"音乐三", @"音乐四"
};

// 录制速度
static NSString * speedList[] = {
    @"慢", @"普通", @"快"
};

@interface RecordViewController () <CountDownViewDelegate, DPHorizontalScrollViewDelegate, UIGestureRecognizerDelegate, GTVRecControllerDelegate> {
    
    dispatch_queue_t queue_;
    
    int _currTotalDuration;
    int _musicSelectViewFinished;
}

@property (nonatomic, strong) GTVRecController * gtvRec;

@property (nonatomic, strong) NSString * tempRecPath;

@property (nonatomic, strong) RecordStatusView * stsView;
@property (nonatomic, strong) UIView * backgroundView;

@property (nonatomic, strong) UIView * camView;
@property (nonatomic, strong) UIButton * deleteBtn;
@property (nonatomic, strong) UIButton * clearBtn;
@property (nonatomic, strong) UIButton * publishBtn;        // 开始录制（录制限制长度？）
@property (nonatomic, strong) UIButton * configBtn;         // 设定按钮
@property (nonatomic, strong) UIButton * flashBtn;          // 闪光按钮
@property (nonatomic, strong) UIButton * rotateBtn;         // 镜头旋转
@property (nonatomic, strong) UIButton * nextBtn;           // 下一步按钮

@property (nonatomic, strong) UIView * commonSettingView;          // 通用设置按钮
@property (nonatomic, strong) UIButton * setFilterBtn;      // 设置滤镜按钮
@property (nonatomic, strong) UIButton * setPaperBtn;       // 设置贴纸按钮
@property (nonatomic, strong) UIButton * setBeautyBtn;      // 设置美颜按钮
@property (nonatomic, strong) UIButton * setMusicBtn;       // 设置音乐按钮
@property (nonatomic, strong) UIButton * setSpeedBtn;       // 设置速度按钮
@property (nonatomic, strong) UIButton * setDelayBtn;       // 延迟录制按钮

@property (nonatomic, strong) UIButton * backBtn;

@property (nonatomic, strong) UIView * selectedMarkView;    // 标记选中状态的view
@property (nonatomic, strong) UIView * settingView;         // 整个设置view
@property (nonatomic, strong) NSMutableArray * settingBtnList;         // 设置按钮列表
@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, strong) UIView * footView;
@property (nonatomic, strong) UIView * beautySlideView;

@property (nonatomic, strong) UIImageView * tapRectView;
@property (nonatomic, strong) DPHorizontalScrollView * scrollView;
@property (nonatomic, assign) int mode; // 0:显示滤镜 1:显示贴纸 2:设置美颜 3:显示音乐 4:选择速度

@property (nonatomic, assign) int choosedFilterIndex; // 0:无滤镜
@property (nonatomic, assign) int choosedEffectIndex; // 0:无特效
@property (nonatomic, assign) int choosedMusicIndex; // 0:无音乐
@property (nonatomic, assign) int choosedSpeedIndex;

// 美颜参数设置
@property (nonatomic, assign) float choosedEyeValue;
@property (nonatomic, assign) float choosedSlimValue;
@property (nonatomic, assign) float choosedWhitenValue;
@property (nonatomic, assign) float choosedSmoothValue;
// 音乐设置
@property (nonatomic, strong) NSString * choosedMusicPath;

// 暂停按钮
@property (nonatomic, strong) UIButton * pauseBtn;  // 录制中的时候显示

// 录制状态
@property (nonatomic, assign) int recStatus;
@property (nonatomic, assign) int recType;  // 延迟录制／手动录制（用于区分是显示暂停按钮还是录制按钮）

// 播放动画
@property (nonatomic, strong) UIButton * animBtn;

@property(nonatomic,strong) UILabel     *timeLabel;

@end

@implementation RecordViewController
{
    NSString        *_tmpDir;
    NSTimer         *_testTimer;
    CGFloat         _testTime;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tempRecPath = [NSString stringWithFormat:@"%@", NSTemporaryDirectory()];
    CGRect rect = [UIScreen mainScreen].bounds;
    CGFloat y = 0;
    BOOL IphoneX = (CGSizeEqualToSize(CGSizeMake(375.f, 812.f), [UIScreen mainScreen].bounds.size) || CGSizeEqualToSize(CGSizeMake(812.f, 375.f), [UIScreen mainScreen].bounds.size));
    if (IphoneX) {
        y = 44;
    }
    
//    self.camView = [[GTVCameraView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    self.camView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    [self.view addSubview:self.camView];
    {
        //添加手势
        UITapGestureRecognizer * camTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cameraView_OnTapped:)];
        //将手势添加到需要相应的view中去
        [self.camView addGestureRecognizer:camTapGesture];
        //选择触发事件的方式（默认单机触发）
        [camTapGesture setNumberOfTapsRequired:1];
    }
    
    self.view.backgroundColor = [GTVConfig config].backgroundColor;
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, y, rect.size.width, 47)];
    self.backgroundView.backgroundColor = [UIColor clearColor];//[AliyunIConfig config].backgroundColor;
    //self.backgroundView.alpha = 0.6;
    [self.view addSubview:self.backgroundView];
    
    self.stsView = [[RecordStatusView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 47)];
    self.stsView.backgroundColor = [GTVConfig config].backgroundColor;
    //self.stsView.alpha = 0.6;
    [self.backgroundView addSubview:self.stsView];
    //[self.stsView setHidden:true];
    
    self.backBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.backBtn.frame = CGRectMake(0, 3, 44, 44);
    
    [self.backBtn setImage:[UIImage imageNamed:@"com-cancel"] forState:UIControlStateNormal];
    [self.backBtn addTarget:self action:@selector(backBtn_OnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.backgroundView addSubview:self.backBtn];
    
    self.nextBtn = [[UIButton alloc] initWithFrame:CGRectMake(rect.size.width-66, 3, 66, 44)];
    [self.nextBtn addTarget:self action:@selector(nextBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextBtn setBackgroundColor:[UIColor clearColor]];
    [self.nextBtn.titleLabel setFont:[UIFont systemFontOfSize:14.f]];
    self.nextBtn.titleLabel.contentMode = UIViewContentModeCenter;
    [self.nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    
    [self.nextBtn setEnabled:true];// 默认不可点击
    [self.backgroundView addSubview:self.nextBtn];
    
    self.flashBtn = [[UIButton alloc] initWithFrame:CGRectMake(rect.size.width/2-50, 3, 44, 44)];
    [self.flashBtn addTarget:self action:@selector(flashBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.flashBtn setImage:[UIImage imageNamed:@"record-flash"] forState:UIControlStateNormal];
    [self.flashBtn setBackgroundColor:[UIColor clearColor]];
    [self.backgroundView addSubview:self.flashBtn];
    
    self.rotateBtn = [[UIButton alloc] initWithFrame:CGRectMake(rect.size.width/2+6, 3, 44, 44)];
    [self.rotateBtn setImage:[UIImage imageNamed:@"record-rotate"] forState:UIControlStateNormal];
    [self.rotateBtn addTarget:self action:@selector(rotateBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.rotateBtn setBackgroundColor:[UIColor clearColor]];
    
    [self.backgroundView addSubview:self.rotateBtn];
    
    // 右侧操作栏
    {
        int left = rect.size.width - 60;
        int top = 60 + y;
        int subTop = 0;
        
        self.commonSettingView = [[UIView alloc] initWithFrame:CGRectMake(left, top, 44, 300)];
        [self.commonSettingView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:self.commonSettingView];

        self.setPaperBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, subTop, 44, 44)];
        self.setPaperBtn.tag = SET_MODE_EFFECT_FACE;
        [self.setPaperBtn addTarget:self action:@selector(commonSettingBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.setPaperBtn setImage:[UIImage imageNamed:@"record-face"] forState:UIControlStateNormal];
        [self.setPaperBtn setBackgroundColor:[UIColor clearColor]];
        [self.commonSettingView addSubview:self.setPaperBtn];
        
        subTop += 60;
        
        self.setFilterBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, subTop, 44, 44)];
        self.setFilterBtn.tag = SET_MODE_INSTA_FILTER;
        [self.setFilterBtn addTarget:self action:@selector(commonSettingBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.setFilterBtn setImage:[UIImage imageNamed:@"record-filter"] forState:UIControlStateNormal];
        [self.setFilterBtn setBackgroundColor:[UIColor clearColor]];
        [self.commonSettingView addSubview:self.setFilterBtn];
        
        subTop += 60;
        
        self.setBeautyBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, subTop, 44, 44)];
        self.setBeautyBtn.tag = SET_MODE_BEAUTY_ADJUST;
        [self.setBeautyBtn addTarget:self action:@selector(commonSettingBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.setBeautyBtn setImage:[UIImage imageNamed:@"record-beauty"] forState:UIControlStateNormal];
        [self.setBeautyBtn setBackgroundColor:[UIColor clearColor]];
        [self.commonSettingView addSubview:self.setBeautyBtn];
        
        subTop += 60;
        
        self.setSpeedBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, subTop, 44, 44)];
        self.setSpeedBtn.tag = SET_MODE_SPEED_SELECT;
        [self.setSpeedBtn addTarget:self action:@selector(commonSettingBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.setSpeedBtn setImage:[UIImage imageNamed:@"record-speed"] forState:UIControlStateNormal];
        [self.setSpeedBtn setBackgroundColor:[UIColor clearColor]];
        [self.commonSettingView addSubview:self.setSpeedBtn];
        
        subTop += 60;
        
        self.setDelayBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, subTop, 44, 44)];
        self.setDelayBtn.tag = 4;
        [self.setDelayBtn addTarget:self action:@selector(setDelayBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.setDelayBtn setImage:[UIImage imageNamed:@"record-countdown"] forState:UIControlStateNormal];
        [self.setDelayBtn setBackgroundColor:[UIColor clearColor]];
        [self.commonSettingView addSubview:self.setDelayBtn];
        /*
        self.setMusicBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, subTop, 44, 44)];
        self.setMusicBtn.tag = 3;
        [self.setMusicBtn addTarget:self action:@selector(commonSettingBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.setMusicBtn setImage:[AliyunImage imageNamed:@"edit_music"] forState:UIControlStateNormal];
        [self.setMusicBtn setBackgroundColor:[UIColor clearColor]];
        [self.commonSettingView addSubview:self.setMusicBtn];
        */
    }
    
    self.choosedEffectIndex = 0;
    self.choosedFilterIndex = 0;
    self.choosedMusicIndex = 0;
    self.choosedSpeedIndex = 1; // 默认是匀速
    
    self.choosedEyeValue = 0.0;
    self.choosedSlimValue = 0.0;
    self.choosedWhitenValue = 0.5;
    self.choosedSmoothValue = 0.5;
    
    self.choosedMusicPath = nil;

    self.pauseBtn = [[UIButton alloc] initWithFrame:CGRectMake(rect.size.width/2-40, rect.size.height-110, 80, 80)];
    [self.pauseBtn setBackgroundImage:[UIImage imageNamed:@"record-rec"] forState:UIControlStateNormal];
    [self.pauseBtn setTitle:@"暂停" forState:UIControlStateNormal];
    [self.pauseBtn addTarget:self action:@selector(pauseBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.pauseBtn];
    
    self.publishBtn = [[UIButton alloc] initWithFrame:CGRectMake(rect.size.width/2-40, rect.size.height-110, 80, 80)];
    [self.publishBtn setBackgroundImage:[UIImage imageNamed:@"record-rec"] forState:UIControlStateNormal];
    [self.publishBtn setTitle:@"录制" forState:UIControlStateNormal];
    [self.publishBtn addTarget:self action:@selector(publishBtn_OnTouchDown:) forControlEvents:UIControlEventTouchDown];
    [self.publishBtn addTarget:self action:@selector(publishBtn_OnTouchEnd:) forControlEvents:UIControlEventTouchUpInside];
    [self.publishBtn addTarget:self action:@selector(publishBtn_OnTouchEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [self.publishBtn addTarget:self action:@selector(publishBtn_OnTouchEnd:) forControlEvents:UIControlEventTouchCancel];
    [self.view addSubview:self.publishBtn];

    self.deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, rect.size.height-110+20-100, 45, 45)];
    [self.deleteBtn setBackgroundImage:[UIImage imageNamed:@"icon_delete"] forState:UIControlStateNormal];
    [self.deleteBtn addTarget:self action:@selector(deleteBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.deleteBtn];
    
    self.clearBtn = [[UIButton alloc] initWithFrame:CGRectMake(rect.size.width-95, rect.size.height-110+20-100, 75, 45)];
    [self.clearBtn setTitle:@"关美颜" forState:UIControlStateNormal];
    [self.clearBtn setTitle:@"开美颜" forState:UIControlStateSelected];
    [self.clearBtn addTarget:self action:@selector(clearBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.clearBtn];
    
    self.animBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, rect.size.height-110+20-100-80, 90, 45)];
    [self.animBtn setTitle:@"打分动画" forState:UIControlStateNormal];
    [self.animBtn addTarget:self action:@selector(animBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.animBtn setBackgroundColor:[UIColor lightGrayColor]];
    [self.view addSubview:self.animBtn];
    
    // 初始化设定画面
    [self initSettingView];
    
    // 音乐选择界面
    MusicSelectView * mv = [[MusicSelectView alloc] initWithFrame:CGRectMake(0, 44, rect.size.width, rect.size.height-44)];
    [self.view addSubview:mv];
    mv.delegate = self;
    _musicSelectViewFinished = 0;
    
    
    //NSString *randomName = [self getCurrentTime];
    NSString * tmpDir = [NSString stringWithFormat:@"%@/abcdefg/", NSTemporaryDirectory()];
    //_tmpDir = [NSString stringWithFormat:@"%@/abcdef/%@/", NSTemporaryDirectory(),randomName];
//    if( [[NSFileManager defaultManager] fileExistsAtPath:tmpDir] == false ) {
//        [[NSFileManager defaultManager] createDirectoryAtPath:tmpDir withIntermediateDirectories:YES attributes:nil error:nil];
//    }
    
    // 创建录制对象，需要指定工作目录，以及delegate
    //self.gtvRec = [[GTVRecController alloc] initWithRootPath:tmpDir                                                 andDelegate:self];
    self.gtvRec = [[GTVRecController alloc] initWithRootPath:tmpDir andDelegate:self defaultUseCamera:YES];
    
    // 设置录制预览view
    [self.gtvRec setRenderView:self.camView
                     withFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    
    // 设置录制视频分辨率
    [self.gtvRec setVideoSize:CGSizeMake(544, 960)];
    
    // 设置录制速度(可重复设置)
    [self.gtvRec setRecordSpeed:DEF_GTV_SPEED_NORMAL];
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(appDidEnterBackground:)
//                                                 name:UIApplicationDidEnterBackgroundNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(appDidBecomeActive:)
//                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)appDidEnterBackground:(id)sender {
    [self.gtvRec stopRecord];
}
-(void)appDidBecomeActive:(id)sender {
    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    return dateTime;
}
- (void) initSettingView
{
    self.settingBtnList = [[NSMutableArray alloc] init];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    if( self.settingView == nil ) {
        self.settingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
        [self.settingView setBackgroundColor:[UIColor clearColor]];
    }
    
    if( self.footView == nil ) {
     
        self.footView = [[UIView alloc] initWithFrame:CGRectMake(0, rect.size.height-SETTING_PANEL_HEIGHT, rect.size.width, SETTING_PANEL_HEIGHT)];
        [self.footView setBackgroundColor:[GTVConfig config].backgroundColor];
        [self.footView setAlpha:0.5f];
        [self.settingView addSubview:self.footView];
    }
    
    if( self.scrollView == nil ) {
     
        self.scrollView = [[DPHorizontalScrollView alloc] initWithFrame:CGRectMake(0, rect.size.height-SETTING_CONTENT_HEIGHT, rect.size.width, SETTING_CONTENT_HEIGHT)];
        self.scrollView.scrollViewDelegate = self;
        [self.settingView addSubview:self.scrollView];
    }
    
    {
        int btn_width = 50;
        int btn_interval = btn_width + 10;
        
        UIButton * b1 = [[UIButton alloc] initWithFrame:CGRectMake(10, rect.size.height-SETTING_PANEL_HEIGHT+3, btn_width, 35)];
        [b1 setTitle:@"滤镜" forState:UIControlStateNormal];
        [b1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        b1.tag = SET_MODE_INSTA_FILTER;
        [self.settingView addSubview:b1];
        [self.settingBtnList addObject:b1];
        
        UIButton * b2 = [[UIButton alloc] initWithFrame:CGRectMake(10+btn_interval, rect.size.height-SETTING_PANEL_HEIGHT+3, btn_width, 35)];
        [b2 setTitle:@"贴纸" forState:UIControlStateNormal];
        [b2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        b2.tag = SET_MODE_EFFECT_FACE;
        [self.settingView addSubview:b2];
        [self.settingBtnList addObject:b2];
        
        UIButton * b3 = [[UIButton alloc] initWithFrame:CGRectMake(10+btn_interval*2, rect.size.height-SETTING_PANEL_HEIGHT+3, btn_width, 35)];
        [b3 setTitle:@"美颜" forState:UIControlStateNormal];
        [b3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        b3.tag = SET_MODE_BEAUTY_ADJUST;
        [self.settingView addSubview:b3];
        [self.settingBtnList addObject:b3];
        
        UIButton * b4 = [[UIButton alloc] initWithFrame:CGRectMake(10+btn_interval*3, rect.size.height-SETTING_PANEL_HEIGHT+3, btn_width, 35)];
        [b4 setTitle:@"速度" forState:UIControlStateNormal];
        [b4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        b4.tag = SET_MODE_SPEED_SELECT;
        [self.settingView addSubview:b4];
        [self.settingBtnList addObject:b4];
        
        /*
        UIButton * b4 = [[UIButton alloc] initWithFrame:CGRectMake(10+btn_interval*3, rect.size.height-SETTING_PANEL_HEIGHT+3, btn_width, 35)];
        [b4 setTitle:@"音乐" forState:UIControlStateNormal];
        [b4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        b4.tag = SET_MODE_MUSIC_SELECT;
        [self.settingView addSubview:b4];
        [self.settingBtnList addObject:b4];
        */
        [b1 addTarget:self action:@selector(modeBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [b2 addTarget:self action:@selector(modeBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [b3 addTarget:self action:@selector(modeBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [b4 addTarget:self action:@selector(modeBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [b5 addTarget:self action:@selector(modeBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if( self.selectedMarkView == nil ) {
        
        self.selectedMarkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SETTING_SCROLL_ITEM_WIDTH, SETTING_CONTENT_HEIGHT)];
        self.selectedMarkView.layer.borderColor = [[GTVConfig config].timelineTintColor CGColor];
        self.selectedMarkView.layer.borderWidth = 1.5f;
    }
    
    {
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSettingViewTapped:)];
        [self.settingView addGestureRecognizer:recognizer];
        self.settingView.userInteractionEnabled = YES;
//        UIButton * coverBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height-SETTING_PANEL_HEIGHT)];
//        [coverBtn addTarget:self action:@selector(coverBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [self.settingView addSubview:coverBtn];
    }
    
    if( self.beautySlideView == nil ) {
        
        self.beautySlideView = [[UIView alloc] initWithFrame:CGRectMake(0, rect.size.height-SETTING_CONTENT_HEIGHT, rect.size.width, SETTING_CONTENT_HEIGHT)];
        {
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 30, 20)];
            [lbl setText:@"大眼"];
            [lbl setFont:[UIFont systemFontOfSize:14.0f]];
            [lbl setTextColor:[UIColor whiteColor]];
            [self.beautySlideView addSubview:lbl];
            
            UISlider * sliderVolume = [[UISlider alloc] initWithFrame:CGRectMake(40, 10, rect.size.width/2-40, 20)];
            [sliderVolume setMaximumTrackTintColor:rgba(239,75,129,1)];
            [sliderVolume setMinimumTrackTintColor:[UIColor whiteColor]];
            [sliderVolume addTarget:self action:@selector(slider_OnValueChanged:) forControlEvents:UIControlEventValueChanged];
            [self.beautySlideView addSubview:sliderVolume];
            sliderVolume.value = self.choosedEyeValue;
            sliderVolume.tag = 0;
        }
        {
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, SETTING_CONTENT_HEIGHT/2+10, 30, 20)];
            [lbl setText:@"瘦脸"];
            [lbl setFont:[UIFont systemFontOfSize:14.0f]];
            [lbl setTextColor:[UIColor whiteColor]];
            [self.beautySlideView addSubview:lbl];
            
            UISlider * sliderVolume = [[UISlider alloc] initWithFrame:CGRectMake(40, SETTING_CONTENT_HEIGHT/2+10, rect.size.width/2-40, 20)];
            [sliderVolume setMaximumTrackTintColor:rgba(239,75,129,1)];
            [sliderVolume setMinimumTrackTintColor:[UIColor whiteColor]];
            [sliderVolume addTarget:self action:@selector(slider_OnValueChanged:) forControlEvents:UIControlEventValueChanged];
            [self.beautySlideView addSubview:sliderVolume];
            sliderVolume.value = self.choosedSlimValue;
            sliderVolume.tag = 1;
        }
        {
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(rect.size.width/2+5, 10, 30, 20)];
            [lbl setText:@"美白"];
            [lbl setFont:[UIFont systemFontOfSize:14.0f]];
            [lbl setTextColor:[UIColor whiteColor]];
            [self.beautySlideView addSubview:lbl];
            
            UISlider * sliderVolume = [[UISlider alloc] initWithFrame:CGRectMake(rect.size.width/2+40, 10, rect.size.width/2-40, 20)];
            [sliderVolume setMaximumTrackTintColor:rgba(239,75,129,1)];
            [sliderVolume setMinimumTrackTintColor:[UIColor whiteColor]];
            [sliderVolume addTarget:self action:@selector(slider_OnValueChanged:) forControlEvents:UIControlEventValueChanged];
            [self.beautySlideView addSubview:sliderVolume];
            sliderVolume.value = self.choosedWhitenValue;
            sliderVolume.tag = 2;
        }
        {
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(rect.size.width/2+5, SETTING_CONTENT_HEIGHT/2+10, 30, 20)];
            [lbl setText:@"磨皮"];
            [lbl setFont:[UIFont systemFontOfSize:14.0f]];
            [lbl setTextColor:[UIColor whiteColor]];
            [self.beautySlideView addSubview:lbl];
            
            UISlider * sliderVolume = [[UISlider alloc] initWithFrame:CGRectMake(rect.size.width/2+40, SETTING_CONTENT_HEIGHT/2+10, rect.size.width/2-40, 20)];
            [sliderVolume setMaximumTrackTintColor:rgba(239,75,129,1)];
            [sliderVolume setMinimumTrackTintColor:[UIColor whiteColor]];
            [sliderVolume addTarget:self action:@selector(slider_OnValueChanged:) forControlEvents:UIControlEventValueChanged];
            [self.beautySlideView addSubview:sliderVolume];
            sliderVolume.value = self.choosedSmoothValue;
            sliderVolume.tag = 3;
        }
    }
    
    return;
}

- (void) showSettingView
{
    //[self.view addSubview:self.settingView];
    [self.view insertSubview:self.settingView belowSubview:self.commonSettingView];
    
    self.mode = SET_MODE_INSTA_FILTER;
    
    //[self.scrollView reloadData];
    [self reloadSettingView];
    
    //将两个按钮移除
    [self.publishBtn removeFromSuperview];
    [self.pauseBtn removeFromSuperview];
    
    return;
}

- (void) hiddenSettingView
{
    [self.view addSubview:self.publishBtn];
    [self.view addSubview:self.pauseBtn];
    
    [self.settingView removeFromSuperview];
}

- (void) reloadSettingView
{
    CGRect rect = [UIScreen mainScreen].bounds;
    
    for( int i=0; i<self.settingBtnList.count; i++ ) {
        UIButton * b = [self.settingBtnList objectAtIndex:i];
        if( b.tag == self.mode ) {
            [b setTitleColor:[GTVConfig config].timelineTintColor forState:UIControlStateNormal];
        }
        else {
            [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
    
    [self.scrollView removeFromSuperview];
    [self.beautySlideView removeFromSuperview];
    
    self.scrollView.scrollViewDelegate = nil;
    self.scrollView = nil;
    
    if( self.mode == SET_MODE_BEAUTY_ADJUST ) {
        
        [self.settingView addSubview:self.beautySlideView];
    }
    else if( self.mode == SET_MODE_SPEED_SELECT ) {
        
        self.scrollView = [[DPHorizontalScrollView alloc] initWithFrame:CGRectMake(rect.size.width/2-SETTING_SCROLL_ITEM_WIDTH*1.5, rect.size.height-SETTING_CONTENT_HEIGHT, SETTING_SCROLL_ITEM_WIDTH*3, SETTING_CONTENT_HEIGHT)];
        self.scrollView.scrollViewDelegate = self;
        [self.settingView addSubview:self.scrollView];
        
        [self.scrollView reloadData];
    }
    else {
     
        self.scrollView = [[DPHorizontalScrollView alloc] initWithFrame:CGRectMake(0, rect.size.height-SETTING_CONTENT_HEIGHT, rect.size.width, SETTING_CONTENT_HEIGHT)];
        self.scrollView.scrollViewDelegate = self;
        [self.settingView addSubview:self.scrollView];
        
        [self.scrollView reloadData];
    }
    
    return;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.gtvRec startPreivew];
    
    if( _musicSelectViewFinished > 0 ) {
        [self operation_onRecordInit];
    }
    
    return;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.gtvRec stopPreview];
    
    [self.gtvRec stopRecord];
    [self.gtvRec destroyRecord];
    
    self.scrollView.scrollViewDelegate = nil;
    [self.scrollView removeFromSuperview];
    self.scrollView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/**
 *  总列数
 */
- (NSInteger)numberOfColumnsInTableView:(DPHorizontalScrollView *)tableView
{
    if( self.mode == SET_MODE_INSTA_FILTER ) {
        return sizeof(filterList)/sizeof(filterList[0]);
    }
    else if( self.mode == SET_MODE_EFFECT_FACE ) {
        return sizeof(effectList)/sizeof(effectList[0]);
    }
    else if( self.mode == SET_MODE_MUSIC_SELECT ) {
        return 5;
    }
    else if( self.mode == SET_MODE_SPEED_SELECT ) {
        return 3;
    }
    
    return 0;
}

/**
 *  每列显示的view
 */
- (UIView *)tableView:(DPHorizontalScrollView *)tableView viewForColumnAtIndex:(NSInteger)index
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SETTING_SCROLL_ITEM_WIDTH, SETTING_CONTENT_HEIGHT)];
    
    if( self.mode == SET_MODE_INSTA_FILTER ) {
        
        if( self.choosedFilterIndex == index ) {
            [self.selectedMarkView removeFromSuperview];
            self.selectedMarkView.frame = CGRectMake(0, 10, SETTING_SCROLL_ITEM_WIDTH, SETTING_SCROLL_ITEM_WIDTH);
            [view addSubview:self.selectedMarkView];
        }
        
        if( index < sizeof(filterNameList)/sizeof(filterNameList[0]) ) {
            UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2+10, SETTING_SCROLL_ITEM_WIDTH-4, SETTING_SCROLL_ITEM_WIDTH-4)];
            //            UIImage * img = [UIImage imageNamed:picList[index]];
            //            [iv setImage:img];
            [iv setBackgroundColor:[UIColor lightGrayColor]];
            [view addSubview:iv];
            
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(2, 2+10, SETTING_SCROLL_ITEM_WIDTH-4, SETTING_SCROLL_ITEM_WIDTH-4)];
            [lbl setTextAlignment:NSTextAlignmentCenter];
            [lbl setFont:[UIFont systemFontOfSize:12.0f]];
            [lbl setText:filterNameList[index]];
            [lbl setTextColor:[UIColor whiteColor]];
            [view addSubview:lbl];
        }
    }
    else if( self.mode == SET_MODE_EFFECT_FACE ) {
        
        if( self.choosedEffectIndex == index ) {
            self.selectedMarkView.frame = CGRectMake(0, 0, SETTING_SCROLL_ITEM_WIDTH, SETTING_SCROLL_ITEM_WIDTH);
            [self.selectedMarkView removeFromSuperview];
            [view addSubview:self.selectedMarkView];
        }
        
        if( index < sizeof(effectList)/sizeof(effectList[0]) ) {
            //NSString * str = effectNameList[index];
            UIImageView * v = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, SETTING_SCROLL_ITEM_WIDTH-4, SETTING_SCROLL_ITEM_WIDTH-4)];
            //[v setImage:[UIImage imageNamed:str]];
            [v setBackgroundColor:[UIColor lightGrayColor]];
            [view addSubview:v];
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(1, SETTING_SCROLL_ITEM_WIDTH, SETTING_SCROLL_ITEM_WIDTH-2, SETTING_CONTENT_HEIGHT-SETTING_SCROLL_ITEM_WIDTH-1)];
            [lbl setTextAlignment:NSTextAlignmentCenter];
            [lbl setFont:[UIFont systemFontOfSize:12.0f]];
            [lbl setText:effectTitleList[index]];
            [lbl setTextColor:[UIColor whiteColor]];
            [view addSubview:lbl];
        }
    }
    else if( self.mode == SET_MODE_MUSIC_SELECT ) {
        
        if( self.choosedMusicIndex == index ) {
            [self.selectedMarkView removeFromSuperview];
            self.selectedMarkView.frame = CGRectMake(0, 0, SETTING_SCROLL_ITEM_WIDTH, SETTING_SCROLL_ITEM_WIDTH);
            [view addSubview:self.selectedMarkView];
        }
        
        if( index < sizeof(musicList)/sizeof(musicList[0]) ) {
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(1, 1, SETTING_SCROLL_ITEM_WIDTH-2, SETTING_CONTENT_HEIGHT-2)];
            [lbl setText:musicNameList[index]];
            [view addSubview:lbl];
        }
    }
    else if( self.mode == SET_MODE_SPEED_SELECT ) {
        
        if( self.choosedSpeedIndex == index ) {
            [self.selectedMarkView removeFromSuperview];
            self.selectedMarkView.frame = CGRectMake(0, (SETTING_CONTENT_HEIGHT-SETTING_SCROLL_ITEM_WIDTH)/2, SETTING_SCROLL_ITEM_WIDTH, SETTING_SCROLL_ITEM_WIDTH);
            [view addSubview:self.selectedMarkView];
        }
        
        if( index < sizeof(speedList)/sizeof(speedList[0]) ) {
            UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(1, 1, SETTING_SCROLL_ITEM_WIDTH-2, SETTING_CONTENT_HEIGHT-2)];
            [lbl setTextColor:[UIColor whiteColor]];
            [lbl setTextAlignment:NSTextAlignmentCenter];
            [lbl setText:speedList[index]];
            [view addSubview:lbl];
        }
    }
    else {
        NSLog(@"unknown mode");
    }
    
    return view;
}

/**
 *  每行view的宽度
 */
- (CGFloat)tableView:(DPHorizontalScrollView *)tableView widthForColumnAtIndex:(NSInteger)index
{
    return 60.0f;
}

/**
 *  点击每列回调
 */
- (void)didSelectViewAtIndex:(NSInteger)index
{
    if( self.mode == SET_MODE_INSTA_FILTER ) {
        self.choosedFilterIndex = index;
        [self.gtvRec setFilterShader:filterList[index]];
    }
    else if( self.mode == SET_MODE_EFFECT_FACE ) {
        self.choosedEffectIndex = index;
        
        NSString * folder = effectList[index];
        //资源路径 sticker
        NSString * path = [[NSBundle mainBundle] resourcePath];
        
        path = [NSString stringWithFormat:@"%@/sticker/%@/", path, folder];
        //path = [NSString stringWithFormat:@"%@/sticker/rabbiteating/", path];
        NSLog(@"path = %@", path);
        [self.gtvRec useStickerByPath:path];
    }
    else if( self.mode == SET_MODE_MUSIC_SELECT ) {
        self.choosedMusicIndex = index;
        NSString * path = musicList[index];
        self.choosedMusicPath = [[NSBundle mainBundle] pathForResource:path ofType:@"" inDirectory:@""];
        NSLog(@"choosedMusicPath = %@", self.choosedMusicPath);
//        if( self.choosedMusicPath.length > 0 ) {
//            [[MusicHandler defaultHandler] playMusic:self.choosedMusicPath];
//        }
//        else {
//            [[MusicHandler defaultHandler] stopPlay];
//        }
    }
    else if( self.mode == SET_MODE_SPEED_SELECT ) {
        self.choosedSpeedIndex = index;
        if( self.choosedSpeedIndex == 0 ) {
            [self.gtvRec setRecordSpeed:DEF_GTV_SPEED_LOW];
        }
        else if( self.choosedSpeedIndex == 2 ) {
            [self.gtvRec setRecordSpeed:DEF_GTV_SPEED_HIGH];
        }
        else {
            [self.gtvRec setRecordSpeed:DEF_GTV_SPEED_NORMAL];
        }
    }
    
    [self.scrollView reloadData];
}

- (void) rotateBtn_OnClicked:(id)sender
{
    [self.gtvRec rotateCamera];
    
    return;
}

- (void) modeBtn_OnClicked:(id)sender
{
    UIButton * btn = (UIButton*)sender;
    
    self.mode = btn.tag;
    
    //将两个按钮移除
    [self.publishBtn removeFromSuperview];
    [self.pauseBtn removeFromSuperview];
    
    [self reloadSettingView];
    
    return;
}

- (void) flashBtn_OnClicked:(id)sender
{
    if( [self.gtvRec isLightOn] ) {
        [self.gtvRec setLightStatus:false];
    }
    else {
        BOOL res = [self.gtvRec setLightStatus:true];
        if( res == false ) {
            // 摄像头不支持闪光
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"操作失败" message:@"前置摄像头无闪光灯"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void) setDelayBtn_OnClicked:(id)sender
{
    // 录制结束
    if( self.recStatus == E_REC_STS_OVER ) {
        [self.view makeToast:@"录制已经结束，请点击下一步" duration:2.0f position:CSToastPositionCenter];
        return;
    }
    
    NSString * path = self.tempRecPath;
    
    // 做一个延迟展示
    CGRect rect = [UIScreen mainScreen].bounds;
    CountDownView * cv = [[CountDownView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    [self.view addSubview:cv];
    cv.userInfo = path;
    cv.finishDelegate = self;
    [cv startCountDown:3];
    
    [self operation_onRecordPrepare];
    
    [self hiddenSettingView];
    
    return;
}

- (void) animBtn_OnClicked:(id)sender
{
    // 播放打分动画
    NSString * path = [[NSBundle mainBundle] resourcePath];
    path = [NSString stringWithFormat:@"%@/animation/%@/", path, @"hulu"];
    [self.gtvRec startPlayAnimation:path];
    // 延迟关闭动画
    [self performSelector:@selector(stopAnim) withObject:nil afterDelay:3.0f];
    [self.animBtn setHidden:true];
}

- (void) stopAnim
{
    [self.gtvRec stopPlayAnimation];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.animBtn setHidden:false];
    });
}

- (void) commonSettingBtn_OnClicked:(UIButton *)sender
{
    [self.publishBtn removeFromSuperview];
    [self.pauseBtn removeFromSuperview];
    
    if( self.settingView.superview == nil ) {
     
        [self.view insertSubview:self.settingView belowSubview:self.commonSettingView];
        
        self.mode = sender.tag;
        
        //[self.scrollView reloadData];
        [self reloadSettingView];
    }
    else {
        
        self.mode = sender.tag;
        
        //[self.scrollView reloadData];
        [self reloadSettingView];
    }
    
    return;
}

- (void) configBtn_OnClicked:(id)sender
{
    // 弹出设定界面
    [self showSettingView];
    
    return;
}

- (void) cameraView_OnTapped:(UIGestureRecognizer*)gestureRecognizer
{
    CGPoint point = [gestureRecognizer locationInView:self.camView];
    
    [self.tapRectView removeFromSuperview];
    
    self.tapRectView = [[UIImageView alloc] initWithFrame:CGRectMake(point.x-50, point.y-50, 100, 100)];
    self.tapRectView.layer.borderColor = [UIColor orangeColor].CGColor;
    self.tapRectView.layer.borderWidth = 1.0f;
    //设置对焦动画
    self.tapRectView.center = point;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.tapRectView.transform = CGAffineTransformMakeScale(1.25, 1.25);
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            self.tapRectView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            self.tapRectView.hidden = YES;
        }];
    }];
    
    [self.camView addSubview:self.tapRectView];
    
    float x = point.x / self.camView.frame.size.width;
    float y = point.y / self.camView.frame.size.height;
    
    [self.gtvRec focusAtPoint:CGPointMake(y, x)];
    
    return;
}

- (void) onSettingViewTapped:(UIGestureRecognizer*)r
{
    CGPoint point = [r locationInView:self.settingView];
    if( point.y > self.footView.frame.origin.y ) {
        return;
    }
    [self hiddenSettingView];
    
    return;
}

- (void) coverBtn_OnClicked:(id)sender
{
    [self hiddenSettingView];
    
    return;
}

- (void) backBtn_OnClicked:(id)sender
{
    //[self dismissViewControllerAnimated:true completion:nil];
    [self.gtvRec stopRecord];
    
    if (__fromDrafts) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }else{
        [self.navigationController popToRootViewControllerAnimated:true];
    }
    
    
    return;
}

- (void) pauseBtn_OnClicked:(id)sender
{
    [self operation_onRecordPause];
    
    return;
}

- (void) finishCountDown:(id)view
{
    CountDownView * cv = (CountDownView*)view;
    
    //NSString * path = cv.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        cv.finishDelegate = nil;
        [cv removeFromSuperview];
    });
    
    self.recType = E_REC_TYPE_DELAY;
    
    [self operation_onRecordResume];
    
    return;
}

-(void)timeForPack {
    _testTime += 0.02;
}
- (void) nextBtn_OnClicked:(id)sender {
    _testTime = 0;
    _testTimer = [NSTimer timerWithTimeInterval:0.02 target:self selector:@selector(timeForPack) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_testTimer forMode:NSRunLoopCommonModes];
    // 如果当前还处于录像中状态，首先需要结束录像
    [self operation_onRecordStop];
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
        [self.gtvRec mergeAllClips];
        
        usleep(3*1000);
        
        dispatch_async(dispatch_get_main_queue(), ^{

            [self.view hideToastActivity];
        });
    });
    
    return;
}

- (void) checkIfTouchRecording
{
    if( self.recType == E_REC_TYPE_HAND ) {
        
        [UIView animateWithDuration:0.5 animations:^{
            self.publishBtn.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                self.publishBtn.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                
                [self performSelectorOnMainThread:@selector(checkIfTouchRecording) withObject:nil waitUntilDone:false];
            }];
        }];
        
    }
    
    return;
}

- (void) clearBtn_OnClicked:(id)sender {
    
    //[self.gtvRec clearAllCurrentClips];
    BOOL flag = [self.gtvRec isBeautyOn];
    [self.gtvRec switchBeauty:!flag];
    self.clearBtn.selected = !self.clearBtn.selected;
    
    return;
}

- (void) deleteBtn_OnClicked:(id)sender {
    
    [self.gtvRec removeLastClip];
    
    // 强制恢复
    [self.stsView restart];
    
    self.recStatus = E_REC_STS_PAUSE;
    [self operation_onRecordPause];
    
    return;
}

- (void) publishBtn_OnTouchDown:(id)sender {
    
    // 录制结束
    if( self.recStatus == E_REC_STS_OVER ) {
        [self.view makeToast:@"录制已经结束，请点击下一步" duration:2.0f position:CSToastPositionCenter];
        return;
    }
    
    NSLog(@"publishBtn_OnTouchDown is called.");
    
    self.recType = E_REC_TYPE_HAND;
    
    [self operation_onRecordResume];
    
    [self checkIfTouchRecording];
    
    return;
}

- (void) publishBtn_OnTouchEnd:(id)sender {
    
    self.recType = E_REC_TYPE_NONE;
    
    [self operation_onRecordPause];
    
    return;
}

- (void) slider_OnValueChanged:(UISlider *)sender
{
    // 大眼
    if( sender.tag == 0 ) {
        
        self.choosedEyeValue = sender.value;
        
        [self.gtvRec setBigEyeIntensity:self.choosedEyeValue];
    }
    // 瘦脸
    else if( sender.tag == 1 ) {
        
        self.choosedSlimValue = sender.value;
        
        [self.gtvRec setSlimFaceIntensity:self.choosedSlimValue];
    }
    // 美白
    else if( sender.tag == 2 ) {
        
        self.choosedWhitenValue = sender.value;
        
        [self.gtvRec setSkinWhiteIntensity:self.choosedWhitenValue];
    }
    // 磨皮
    else if( sender.tag == 3 ) {
        
        self.choosedSmoothValue = sender.value;
        
        [self.gtvRec setSkinSmoothIntensity:self.choosedSmoothValue];
    }
    
    return;
}

- (int) currentRecordMilliSeconds
{
    int milli = _currTotalDuration;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        if( milli > 8000.0f && self.nextBtn.enabled == false ) {
            [self.nextBtn setEnabled:true];
            [self.nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    });
    
    // 获取当前已经录制的录像长度
    return milli;
}

- (void) finishedRecord
{
    [self operation_onRecordStop];
 
    dispatch_async(dispatch_get_main_queue(), ^{
    
        [self.view makeToast:@"录制已经结束，请点击下一步" duration:2.0f position:CSToastPositionCenter];
    });
    
//    [self.stsView setHidden:true];
//    [self.backgroundView setHidden:false];
    
    // 跳到下一个画面
    // [self.navigationController popViewControllerAnimated:true];
    
    return;
}

// 0:未录制状态 1:录制中状态 2:录制暂停 3:录制完成
- (void) updateRecordStatus
{
    int sts = self.recStatus;
    
    if( sts == E_REC_STS_IDLE ) {
        
        [self.commonSettingView setHidden:false];
    }
    else if( sts == E_REC_STS_LAUNCHING ) {
        
        [self.pauseBtn setHidden:true];
        [self.publishBtn setHidden:true];
        
        [self.backgroundView setHidden:true];
        [self.commonSettingView setHidden:true];
    }
    else if( sts == E_REC_STS_RUNNING ) {
        
        // 手动录制
        if( self.recType == E_REC_TYPE_HAND ) {
         
            [self.pauseBtn setHidden:true];
            [self.publishBtn setHidden:false];
        }
        // 延迟录制
        else {
            
            [self.pauseBtn setHidden:false];
            [self.publishBtn setHidden:true];
        }
        
        [self.backgroundView setHidden:false];
        [self.commonSettingView setHidden:true];
    }
    else if( sts == E_REC_STS_PAUSE ) {
        
        [self.pauseBtn setHidden:true];
        [self.publishBtn setHidden:false];
        
        [self.backgroundView setHidden:false];
        [self.commonSettingView setHidden:false];
    }
    else {
        
        [self.pauseBtn setHidden:true];
        [self.publishBtn setHidden:true];
        
        [self.backgroundView setHidden:false];
        [self.commonSettingView setHidden:true];
    }
    
    return;
}

- (void) operation_onRecordInit
{
    if( self.choosedMusicPath.length > 0 ) {
        [self.gtvRec setMusicPath:self.choosedMusicPath andStartTime:_currTotalDuration];
    }
    [self.gtvRec initRecord];
//    {
//        NSString * musicPath = [[NSBundle mainBundle] pathForResource:@"love.mp3" ofType:nil];
//        self.choosedMusicPath = musicPath;
//        [self.gtvRec setMusicPath:musicPath andStartTime:0];
//    }

    // 进度条清零
    [self.stsView reset];
    [self.stsView restart];
    self.stsView.delegate = self;
    
    self.recStatus = E_REC_STS_PAUSE;
    
    [self updateRecordStatus];
    
    return;
}

- (void) operation_onRecordPrepare
{
    if (self.recStatus == E_REC_STS_OVER) {
        return;
    }
    
    // 进入倒计时状态
    self.recStatus = E_REC_STS_LAUNCHING;
    [self updateRecordStatus];
    
    return;
}

- (void) operation_onRecordPause
{
    if (self.recStatus == E_REC_STS_OVER) {
        return;
    }
    
    self.recStatus = E_REC_STS_PAUSE;
    [self updateRecordStatus];
    
    [self.gtvRec stopRecord];
    
    return;
}

- (void) operation_onRecordResume
{
    if (self.recStatus == E_REC_STS_OVER) {
        return;
    }
    
    self.recStatus = E_REC_STS_RUNNING;
    [self updateRecordStatus];
    
    // 恢复录制
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [self.gtvRec startRecord];
    });

    return;
}

- (void) operation_onRecordStop
{
    if (self.recStatus == E_REC_STS_OVER) {
        return;
    }
    
    [self.gtvRec stopRecord];
    
    [self.stsView stop];
    
    self.recStatus = E_REC_STS_OVER;
    [self updateRecordStatus];
    
    return;
}

#pragma mark    ======== MusicSelectViewDelegate ========
/**
 选择完音乐后开始
 */
- (void) finishSelectWithIndex:(int)index fromView:(UIView*)v
{
    self.choosedMusicIndex = index;
    
    if( index == 0 ) {
        
        self.choosedMusicPath = nil;
    }
    else {
        
        NSString * path = musicList[index];
        self.choosedMusicPath = [[NSBundle mainBundle] pathForResource:path ofType:@"" inDirectory:@""];
        NSLog(@"choosedMusicPath = %@", self.choosedMusicPath);
    }
    
    [v removeFromSuperview];

    if( self.choosedMusicPath.length > 0 ) {
        [self.gtvRec setMusicPath:self.choosedMusicPath andStartTime:0];
    }
    
    [self operation_onRecordInit];
    
    _musicSelectViewFinished = 1;
    
    return;
}

- (void) updateRecordVideoDuration:(NSDictionary*)dict
{
    NSString * ts = [dict objectForKey:@"totalDuration"];
    
    _currTotalDuration = (int)ts.integerValue;
    
    NSLog(@"dict = %@ ", ts);
    self.timeLabel.text = [NSString stringWithFormat:@"00:%02d",_currTotalDuration / 1000];
    return;
}
-(UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 20)];
        _timeLabel.textColor = [UIColor blueColor];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.view addSubview:_timeLabel];
    }
    return _timeLabel;
}
#pragma mark    ======== GTVRecControllerDelegate ========
- (void) notifyRecordStatus:(NSString*)evt withInfo:(NSDictionary*)info
{
    NSLog(@"notifyRecordStatus:%@ - %@", evt, info);
    
    // 录制准备完成的时候调用
    if( [evt isEqualToString:kGTVRecEventInited] ) {
        [self updateRecordVideoDuration:info];
    }
    // 录制过程中更新录制进度状态
    else if( [evt isEqualToString:kGTVRecEventStatus] ) {
        [self updateRecordVideoDuration:info];
    }
    // 录制结束时调用
    else if( [evt isEqualToString:kGTVRecEventDestroyed] ) {
        [self updateRecordVideoDuration:info];
    }
    // 录制异常中断
    else if( [evt isEqualToString:kGTVRecEventError] ) {
        
    }
    // 录制完成，导出视频完成时调用
    else if( [evt isEqualToString:kGTVRecEventExported] ) {
        
        /***
        NSString * n = [info objectForKey:@"recordFile"];
        if(n.length > 0){
            NSURL *outputURL = [NSURL URLWithString:n];

            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (error) {
                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存失败" message:@"相册视频保存失败"
                            //                                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                        [alert show];
                            NSLog(@"recordFile failed %@ ", n);
                        } else {
                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存成功" message:@"视频已保存到相册"
                            //                                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                        [alert show];
                            NSLog(@"recordFile succeed %@ ", n);
                        }
                    });
                }];
            }
        }
        {
        NSString * r = [info objectForKey:@"tempFile"];
        if(n.length > 0){
            NSURL *outputURL = [NSURL URLWithString:r];

            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (error) {
                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存失败" message:@"相册视频保存失败"
                            //                                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                        [alert show];
                            NSLog(@"tempFile failed %@ ", r);
                        } else {
                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存成功" message:@"视频已保存到相册"
                            //                                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                        [alert show];
                            NSLog(@"tempFile succeed %@ ", r);
                        }
                    });
                }];
            }
        }
        }
        NSString * r = [info objectForKey:@"reverseFile"];
        if(n.length > 0){
            NSURL *outputURL = [NSURL URLWithString:r];

            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (error) {
                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存失败" message:@"相册视频保存失败"
                            //                                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                        [alert show];
                            NSLog(@"reverseFile failed %@ ", r);
                        } else {
                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存成功" message:@"视频已保存到相册"
                            //                                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            //                        [alert show];
                            NSLog(@"reverseFile succeed %@ ", r);
                        }
                    });
                }];
            }
        }
        ***/
        NSString * n = [info objectForKey:@"recordFile"];
        NSString * r = [info objectForKey:@"reverseFile"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
//            [self.view hideToastActivity];
//
//            [self.view hideToastActivity];
//
//            [self.view makeToast:@"111111" duration:2.0f position:CSToastPositionCenter];
            
            if( [self.navigationController topViewController] != self ) {
                return;
            }
            
            [_testTimer invalidate];
            _testTimer = nil;
            NSLog(@"pack_____testTime     %f",_testTime);
            // 跳转到预览界面
            //[self.navigationController popViewControllerAnimated:false];
            PreviewViewController * p = [[PreviewViewController alloc] init];
            p.vPath = n;
            p.vReversePath = r;
            p.fromRecord = true;
            //NSString *randomName = [self getCurrentTime];
            NSString * tmpDir = [NSString stringWithFormat:@"%@/abcdefg/", NSTemporaryDirectory()];
           // _tmpDir = [NSString stringWithFormat:@"%@/abcdef/%@/", NSTemporaryDirectory(),randomName];
            NSString * d = tmpDir;//[NSString stringWithFormat:@"%@/abcdef/", NSTemporaryDirectory()];
            NSLog(@"d = %@", d);
            p.workFolder = d;
            p.musicPath = self.choosedMusicPath;
            if (__fromDrafts) {
                p.draftsFolder = _draftsFolder;
            }
            [self.navigationController pushViewController:p animated:true];
        });
    }
    
//    if( [evt isEqualToString:kGTVRecEventStopped] ) {
//
//        NSArray * list = [info objectForKey:@"list"];
//        for( int i=0; i<list.count; i++ ) {
//
//            NSDictionary * elem = [list objectAtIndex:i];
//            NSString * fname = [elem objectForKey:@"filename"];
//            NSString * recPath = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), fname];
//            NSURL *outputURL = [NSURL URLWithString:recPath];
//
//            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//            if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
//                [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        if (error) {
//                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存失败" message:@"相册视频保存失败"
//                            //                                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                            //                        [alert show];
//                            NSLog(@"recPath failed %@ ", recPath);
//                        } else {
//                            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"保存成功" message:@"视频已保存到相册"
//                            //                                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                            //                        [alert show];
//                            NSLog(@"recPath succeed %@ ", recPath);
//                        }
//                    });
//                }];
//            }
//        }
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.navigationController popToRootViewControllerAnimated:true];
//        });
//    }
}

@end
