//
//  MusicSelectElemView.h
//  gtv
//
//  Created by gtv on 2017/12/14.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MusicSelectElemViewDelegate <NSObject>

- (void) musicElemViewOnTouched:(UIView*)view;
- (void) musicElemViewOnPlayed:(UIView*)view;

@end

@interface MusicSelectElemView : UIView

@property (nonatomic, assign) id<MusicSelectElemViewDelegate> delegate;

- (void) setIconImage:(UIImage*)img andName:(NSString*)name andInfo:(NSString*)info;

// 外部动态更新
- (void) setSelected:(BOOL)flg;
- (void) setPlaying:(BOOL)flg;

- (BOOL) isPlaying;
- (BOOL) isSelected;

@end
