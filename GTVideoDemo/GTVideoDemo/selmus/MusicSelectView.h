//
//  MusicSelectView.h
//  gtv
//
//  Created by gtv on 2017/12/6.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MusicSelectViewDelegate <NSObject>

- (void) finishSelectWithIndex:(int)idx fromView:(UIView*)v;

@end

@interface MusicSelectView : UIView

@property (nonatomic, weak) id<MusicSelectViewDelegate> delegate;

@end
