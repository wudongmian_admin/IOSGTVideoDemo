//
//  MusicSelectView.m
//  gtv
//
//  Created by gtv on 2017/12/6.
//  Copyright © 2017年 gtv. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "GTVConfig.h"
#import "MusicSelectView.h"
#import "MusicSelectElemView.h"

static NSString * musicList[] = {
    @"-", @"panama.mp3", @"lvxing.mp3", @"hongzhaoyuan.mp3", @"love.mp3"
};
static NSString * musicNameList[] = {
    @"无", @"Panama", @"带你去旅行", @"红昭愿", @"Love Story"
};
static NSString * musicIconList[] = {
    @"0", @"selmusic-cover1", @"selmusic-cover2", @"selmusic-cover3", @"selmusic-cover4"
};
static NSString * musicInfoList[] = {
    @"0", @"Matteo\n00:15", @"校长\n00:32", @"音阙诗听\n00:17", @"Tailor Swift\n00:56"
};

@interface MusicSelectView() <MusicSelectElemViewDelegate> {
    int choosed;
}

@property (nonatomic, strong) NSMutableArray * btnList;
@property (nonatomic, strong) NSMutableArray * elemList;

@property (nonatomic, strong) AVAudioPlayer * audioPlayer;

@end

@implementation MusicSelectView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        
        [self setBackgroundColor:[GTVConfig config].backgroundColor];
        
        self.elemList = [[NSMutableArray alloc] init];
        
        float width = frame.size.width;
        float height = width * 233 / 720;
        float top = 0;

        for( int i=1; i<5; i++ ) {
            
            MusicSelectElemView * elem = [[MusicSelectElemView alloc] initWithFrame:CGRectMake(0, top, width, height)];
            [elem setIconImage:[UIImage imageNamed:musicIconList[i]] andName:musicNameList[i] andInfo:musicInfoList[i]];
            elem.tag = i;
            elem.delegate = self;
            
            [self.elemList addObject:elem];
            [self addSubview:elem];
            
            top += height;
        }
        
        UIView * splitView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 0.5)];
        [splitView setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.9]];
        [self addSubview:splitView];
        
        UIButton * b0 = [[UIButton alloc] initWithFrame:CGRectMake(width/20, frame.size.height - 80, frame.size.width - width/10, 44)];
        [b0 setTitle:@"开  拍" forState:UIControlStateNormal];
        [b0 setBackgroundImage:[UIImage imageNamed:@"selmusic-start"] forState:UIControlStateNormal];
        [b0 addTarget:self action:@selector(startBtn_OnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:b0];
        
        choosed = 0;
    }
    
    return self;
}

- (void) startBtn_OnClicked:(UIButton * )sender
{
    //[[MusicHandler defaultHandler] stopPlay];
    
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(finishSelectWithIndex:fromView:)] ) {
        [self.delegate finishSelectWithIndex:choosed fromView:self];
    }
    
    return;
}

- (void) musicElemViewOnTouched:(UIView*)view
{
    // 先清除所有view的选中状态
    for( int i=0; i<self.elemList.count; i++ ) {
        MusicSelectElemView * e = (MusicSelectElemView*)[self.elemList objectAtIndex:i];
        [e setSelected:false];
    }
    
    MusicSelectElemView * o = (MusicSelectElemView*)view;
    if( o.tag == choosed ) {
        // 选择撤销
        choosed = 0;
    }
    else {
        choosed = o.tag;
        [o setSelected:true];
    }
}

- (void) musicElemViewOnPlayed:(UIView*)view
{
    [self stopMusic];
    
    MusicSelectElemView * o = (MusicSelectElemView*)view;
    if( [o isPlaying] ) {
        [o setPlaying:false];
    }
    else {
        [o setPlaying:true];
        
        NSString * path = musicList[o.tag];
        NSString * choosedMusicPath = [[NSBundle mainBundle] pathForResource:path ofType:@"" inDirectory:@""];
        NSLog(@"choosedMusicPath = %@", choosedMusicPath);
        if( choosedMusicPath.length > 0 ) {
            [self playMusic:choosedMusicPath];
        }
        else {
            [self stopMusic];
        }
    }
    
    // 先清除所有其他view的播放状态
    for( int i=0; i<self.elemList.count; i++ ) {
        MusicSelectElemView * e = (MusicSelectElemView*)[self.elemList objectAtIndex:i];
        if( e != view )
            [e setPlaying:false];
    }
}

- (void) playMusic:(NSString*)p
{
    NSURL *fileURL = [NSURL URLWithString:p];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    
    self.audioPlayer.numberOfLoops = -1;
    [self.audioPlayer play];
    
    return;
}

- (void) stopMusic
{
    [self.audioPlayer stop];
    self.audioPlayer = nil;
}

@end
