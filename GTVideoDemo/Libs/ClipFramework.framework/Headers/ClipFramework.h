//
//  ClipFramework.h
//  ClipFramework
//
//  Created by 锦 on 2018/3/30.
//  Copyright © 2018年 锦. All rights reserved.
// 

#import <UIKit/UIKit.h>

//! Project version number for ClipFramework.
FOUNDATION_EXPORT double ClipFrameworkVersionNumber;

//! Project version string for ClipFramework.
FOUNDATION_EXPORT const unsigned char ClipFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ClipFramework/PublicHeader.h>
#import "ClipMusicView.h"
#import "ClipVideoView.h"
#import "VideoFrameView.h"
#import "FilterFrameView.h"
#import "CustomAnimatedTransitioning.h"
#import "CustomInteractiveTransition.h"
