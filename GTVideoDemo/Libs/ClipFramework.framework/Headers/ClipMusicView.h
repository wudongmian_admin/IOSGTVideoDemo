//
//  ClipMusicView.h
//  GTVideoDemo
//
//  Created by 锦 on 2018/3/30.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ClipMusicViewDelegate <NSObject>

@optional

/**
 musicNowTime播放当前从这个时间开始  
 */
- (void) ClipMusicViewDidEndWithMusicTime:(CGFloat )musicNowTime MusictotalTime:(CGFloat )musicAllTime;

@end


@interface ClipMusicView : UIView
@property (nonatomic, weak) id<ClipMusicViewDelegate> delegate;
/**
 音乐开始时长
 */
@property (nonatomic)CGFloat musicStartTime;
/**
 音乐总时长
 */
@property (nonatomic)CGFloat musicTotalTime;
/**
 效果播放时长和video一样
 */
@property (nonatomic)CGFloat videoTime;

/**
 跳转到某个时间点
 */
-(void)seekTo:(CGFloat )currentTime;

@end





