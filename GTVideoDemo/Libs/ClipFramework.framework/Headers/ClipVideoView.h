//
//  ClipVideoView.h
//  GTVideoDemo
//
//  Created by 锦 on 2018/3/30.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClipVideoView : UIView



/**
 剪辑开始时间
 */
@property (nonatomic, assign)CGFloat min;
/**
 剪辑的结束时间
 */
@property (nonatomic, assign)CGFloat max;
/**
 加载视频得到时长
 */
-(void)loadVideoTime:(CGFloat )time;
/**
 初始化时给显示照片view 的数量
 */
-(void)initSubViewsWithNumber:(int )num;

/**
 照片imageView赋值 数组
 */
-(void)clipVideoViewAddImages:(NSArray *)imageArr;
/**
 照片imageView赋值 下标
 */
-(void)clipVideoViewAddImage:(UIImage *)image withIndex:(int )index;
@end
