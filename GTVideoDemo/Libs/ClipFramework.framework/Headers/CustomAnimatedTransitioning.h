//
//  CustomAnimatedTransitioning.h
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CustomWay) {
    CustomPresentWay = 0,
    CustomPushWay
};

typedef NS_ENUM(NSUInteger, CustomTransitionType) {
    CustomTransitionTypePush = 0,
    CustomTransitionTypePop
};

@interface CustomAnimatedTransitioning : NSObject<UIViewControllerAnimatedTransitioning>

+ (instancetype)transitionWithTransitionType:(CustomTransitionType)type andWay:(CustomWay)way andFromeView:(UIView *)fromeView andToView:(UIView *)toView;
- (instancetype)initWithTransitionType:(CustomTransitionType)type andWay:(CustomWay)way andFromeView:(UIView *)fromeView andToView:(UIView *)toView;

@end
