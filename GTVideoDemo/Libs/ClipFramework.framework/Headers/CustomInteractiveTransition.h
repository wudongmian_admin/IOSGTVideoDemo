//
//  CustomInteractiveTransition.h
//  GTVideoDemo
//
//  Created by Jin on 2018/4/23.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GestureConifg)(void);

typedef NS_ENUM(NSUInteger, CustomInteractiveTransitionGestureDirection) {//手势的方向
    CustomInteractiveTransitionGestureDirectionLeft = 0,
    CustomInteractiveTransitionGestureDirectionRight,
    CustomInteractiveTransitionGestureDirectionUp,
    CustomInteractiveTransitionGestureDirectionDown
};

typedef NS_ENUM(NSUInteger, CustomInteractiveTransitionType) {//手势控制哪种转场
    CustomInteractiveTransitionTypePresent = 0,
    CustomInteractiveTransitionTypeDismiss,
    CustomInteractiveTransitionTypePush,
    CustomInteractiveTransitionTypePop,
};

@interface CustomInteractiveTransition : UIPercentDrivenInteractiveTransition
/**记录是否开始手势，判断pop操作是手势触发还是返回键触发*/
@property (nonatomic, assign) BOOL interation;
/**促发手势present的时候的config，config中初始化并present需要弹出的控制器*/
@property (nonatomic, copy) GestureConifg presentConifg;
/**促发手势push的时候的config，config中初始化并push需要弹出的控制器*/
@property (nonatomic, copy) GestureConifg pushConifg;

//初始化方法

+ (instancetype)interactiveTransitionWithTransitionType:(CustomInteractiveTransitionType)type GestureDirection:(CustomInteractiveTransitionGestureDirection)direction;
- (instancetype)initWithTransitionType:(CustomInteractiveTransitionType)type GestureDirection:(CustomInteractiveTransitionGestureDirection)direction;

/** 给传入的控制器添加手势*/
- (void)addPanGestureForViewController:(UIViewController *)viewController;
@end


/**
 在需要动画的
 @property (nonatomic, strong) CustomInteractiveTransition *interactivePush;
 _interactivePush = [CustomInteractiveTransition interactiveTransitionWithTransitionType:CustomInteractiveTransitionTypePresent GestureDirection:CustomInteractiveTransitionGestureDirectionUp];
 
 
 1、如果是push  需要设置navi的代理 还要给第一个VC要动画的view
 EffectVideoController * cut = [[EffectVideoController alloc] init];
 self.navigationController.delegate = cut;
 cut.fromeView = self.mediaContainer;
 [self.navigationController pushViewController:cut animated:YES];
 
 
 ***   push的view需要 继承<UINavigationControllerDelegate>
 @property (nonatomic, strong) CustomInteractiveTransition *interactiveTransition;
 //初始化手势过渡的代理
 self.interactiveTransition = [CustomInteractiveTransition interactiveTransitionWithTransitionType:CustomInteractiveTransitionTypePop GestureDirection:CustomInteractiveTransitionGestureDirectionRight];
 实现代理 ======== UINavigationControllerDelegate ========
 需要 FromeView 和 ToView   toview 给个和需要指定的view的一样的frame的view就可以了
 
 demo上有
 
 
 2、如果是present
 直接[self presentViewController:vc animated:YES completion:nil];
 
 
 ***   present的view  要实现
 - (instancetype)init
 {
 self = [super init];
 if (self) {
 self.transitioningDelegate = self;
 self.modalPresentationStyle = UIModalPresentationCustom;
 //self.modalPresentationStyle = UIModalPresentationFullScreen;
 }
 return self;
 }
 @property (nonatomic, strong) CustomInteractiveTransition *interactiveDismiss;
 实现======== UIViewControllerTransitioningDelegate ========  不需要 FromeView 和 ToView
 
 */
