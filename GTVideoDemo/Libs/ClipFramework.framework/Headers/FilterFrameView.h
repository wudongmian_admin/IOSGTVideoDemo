//
//  FilterFrameView.h
//  GTVideoDemo
//
//  Created by apple on 2018/4/16.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterFrameView : UIView

/**
 视频总时长
 */
@property (nonatomic)CGFloat totalTime;
/**
 是否需要filter加效果
 */
@property (nonatomic, assign)BOOL hasFilterView;
/**
 传入播放器是否暂停
 */
@property (nonatomic, assign)BOOL isPause;
/**
 返回当前拖到时间的block
 */
@property (nonatomic,copy)void(^DraggingBlock)(double nowTime);
/**
 当前的时间
 */
-(void)setVideoNowTime:(CGFloat )nowTime;
/**
 新的filterView
 */
-(void)setNewFilterViewWithColor:(CGColorRef )color;

/**
 删除上一个filterView
 */
-(void)deleteFilter;
/**
 初始化里面View的数量，必须要给
 */
-(void)initSubViewsWithNum:(int)num;

/**
 预览照片image  0 ~ 初始化view的数量
 */
-(void)loadImages:(UIImage *)image withIndex:(int )index;
@end


/**
 *  此Animation是在MediaControlView的回调下进去的
 *  MediaControlView 的 - (void)refreshMediaControl这个方法里返回总时间和当前时间 以及但播放器暂停后的回调
    暂停后回调:  self.mediaControl.videoPausedBlock = ^{
    weakSelf.filterFrameView.hasFilterView = NO;
    };
 *  长按filter效果后 只需 [self.filterFrameView setNewFilterViewWithColor:(__bridge CGColorRef)(c)];
      self.filterFrameView.hasFilterView = YES;
 *
 *
 *
 *
 *
 *
 */
