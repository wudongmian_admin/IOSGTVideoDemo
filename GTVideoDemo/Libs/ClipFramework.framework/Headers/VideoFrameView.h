//
//  VideoFrameView.h
//  GTVideoDemo
//
//  Created by 锦 on 2018/3/30.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol VideoFrameViewDelegate <NSObject>

@optional

/**
 拖动结束返回当前时间
 */
-(void)VideoFrameViewDidEndWithTime:(CGFloat )time;

-(void)setImageForDraggingTime:(CGFloat )nowTime;
@end
@interface VideoFrameView : UIView
@property (nonatomic, weak) id<VideoFrameViewDelegate> delegate;
/**
 初始化预览照片的数量
 */
-(void)initViewsWithNum:(int )num;
/**
 加载视频总时长
 */
-(void)loadTime:(CGFloat )time;
/**
 拖动条的image
 */
-(void)loadSmallImage:(UIImage *)image;
/**
 预览照片image
 */
-(void)loadImages:(UIImage *)image withIndex:(int )index;

/**
 中间时间条的样式 给个宽度
 */
-(void)setImagetoTimeLine:(UIImage *)image withWidth:(CGFloat )width;


@end
