//
//  GTVDef.h
//  GTVideoLib
//
//  Created by gtv on 2018/1/26.
//  Copyright © 2018年 gtv. All rights reserved.
//

#ifndef GTVDef_h
#define GTVDef_h

typedef void (^GTVCodeBlock)(void);

#define DEF_GTV_REC_CONFIG_NAME             @"gtv_segments.conf"
#define DEF_GTV_REPLAY_CONFIG_NAME          @"gtv_replay_setting.json"

// 录像速度
#define DEF_GTV_SPEED_LOW                   (-1)
#define DEF_GTV_SPEED_NORMAL                (0)
#define DEF_GTV_SPEED_HIGH                  (1)

// 滤镜名称
#define DEF_GTV_FILTER_NONE                 @"FILTER_NONE"
#define DEF_GTV_FILTER_IF1977               @"FILTER_IF1977"
#define DEF_GTV_FILTER_IFAMARO              @"FILTER_IFAMARO"
#define DEF_GTV_FILTER_IFBRANNAN            @"FILTER_IFBRANNAN"
#define DEF_GTV_FILTER_IFEARLY              @"FILTER_IFEARLY"
#define DEF_GTV_FILTER_IFHEFE               @"FILTER_IFHEFE"
#define DEF_GTV_FILTER_IFHUDSON             @"FILTER_IFHUDSON"
#define DEF_GTV_FILTER_IFINKWELL            @"FILTER_IFINKWELL"
#define DEF_GTV_FILTER_IFLOMOFI             @"FILTER_IFLOMOFI"
#define DEF_GTV_FILTER_IFLORDKELVIN         @"FILTER_IFLORDKELVIN"
#define DEF_GTV_FILTER_IFNASHVILLE          @"FILTER_IFNASHVILLE"
#define DEF_GTV_FILTER_IFRISE               @"FILTER_IFRISE"
#define DEF_GTV_FILTER_IFSIERRA             @"FILTER_IFSIERRA"
#define DEF_GTV_FILTER_IFSUTRO              @"FILTER_IFSUTRO"
#define DEF_GTV_FILTER_IFTOASTER            @"FILTER_IFTOASTER"
#define DEF_GTV_FILTER_IFVALENCIA           @"FILTER_IFVALENCIA"
#define DEF_GTV_FILTER_IFWALDEN             @"FILTER_IFWALDEN"
#define DEF_GTV_FILTER_IFXPROII             @"FILTER_IFXPROII"
#define DEF_GTV_FILTER_LIGHTGRAY            @"FILTER_LIGHTGRAY"
#define DEF_GTV_FILTER_DARKGRAY             @"FILTER_DARKGRAY"

#define DEF_GTV_FILTER_HAI_ZUFU             @"FILTER_HAI_ZUFU"
#define DEF_GTV_FILTER_HAI_DAOGUO           @"FILTER_HAI_DAOGUO"
#define DEF_GTV_FILTER_HAI_DENGHOU          @"FILTER_HAI_DENGHOU"
#define DEF_GTV_FILTER_HAI_FENGLING         @"FILTER_HAI_FENGLING"
#define DEF_GTV_FILTER_HAI_HAIFENG          @"FILTER_HAI_HAIFENG"
#define DEF_GTV_FILTER_HAI_RIJI             @"FILTER_HAI_RIJI"
#define DEF_GTV_FILTER_HAI_WUYU             @"FILTER_HAI_WUYU"
#define DEF_GTV_FILTER_HAI_XINGFU           @"FILTER_HAI_XINGFU"

#define DEF_GTV_FILTER_RIXI_SENLIN          @"FILTER_RIXI_SENLIN"
#define DEF_GTV_FILTER_RIXI_NAICHA          @"FILTER_RIXI_NAICHA"
#define DEF_GTV_FILTER_RIXI_RICHU           @"FILTER_RIXI_RICHU"
#define DEF_GTV_FILTER_RIXI_ROUMEI          @"FILTER_RIXI_ROUMEI"
#define DEF_GTV_FILTER_RIXI_TIANMEI         @"FILTER_RIXI_TIANMEI"
#define DEF_GTV_FILTER_RIXI_YIDOU           @"FILTER_RIXI_YIDOU"
#define DEF_GTV_FILTER_RIXI_YINGHUA         @"FILTER_RIXI_YINGHUA"
#define DEF_GTV_FILTER_RIXI_ZIRAN           @"FILTER_RIXI_ZIRAN"
#define DEF_GTV_FILTER_RIXI_FENNENG         @"FILTER_RIXI_FENNENG"

// 特效名称
#define DEF_GTV_EFFECT_NONE                 @"EFFECT_NONE"      // 无特效
#define DEF_GTV_EFFECT_LINGHUN              @"EFFECT_LINGHUN"   // 灵魂
#define DEF_GTV_EFFECT_DOUDONG              @"EFFECT_DOUDONG"   // 抖动
#define DEF_GTV_EFFECT_YISHI                @"EFFECT_YISHI"     // 异世
#define DEF_GTV_EFFECT_HEIBAI               @"EFFECT_HEIBAI"    // 黑白
#define DEF_GTV_EFFECT_JIANRUI              @"EFFECT_JIANRUI"   // 尖锐
#define DEF_GTV_EFFECT_BODONG               @"EFFECT_BODONG"    // 波动
#define DEF_GTV_EFFECT_TOUSHI               @"EFFECT_TOUSHI"    // 透视
#define DEF_GTV_EFFECT_SHANSUO              @"EFFECT_SHANSUO"   // 闪烁
#define DEF_GTV_EFFECT_SUMIAO               @"EFFECT_SUMIAO"    // 素描
#define DEF_GTV_EFFECT_LINGYI               @"EFFECT_LINGYI"    // 灵异
#define DEF_GTV_EFFECT_YINXIANG             @"EFFECT_YINXIANG"  // 印象

#endif /* GTVDef_h */
