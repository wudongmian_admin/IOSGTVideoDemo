//
//  GTVRecController.h
//  GTVideoLib
//
//  Created by gtv on 2018/1/25.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kGTVRecEventInited                  @"kGTVRecEventInited"
#define kGTVRecEventStarted                 @"kGTVRecEventStarted"
#define kGTVRecEventStopped                 @"kGTVRecEventStopped"
#define kGTVRecEventDestroyed               @"kGTVRecEventDestroyed"
#define kGTVRecEventExported                @"kGTVRecEventExported"
#define kGTVRecEventRemoved                 @"kGTVRecEventRemoved"  // 该event在有录像分段被删除的时候触发
#define kGTVRecEventStatus                  @"kGTVRecEventStatus"
#define kGTVRecEventError                   @"kGTVRecEventError"

@protocol GTVRecControllerDelegate <NSObject>

- (void) notifyRecordStatus:(NSString*)evt withInfo:(NSDictionary*)info;

@end

@protocol GTVRecVideoFrameDelegate <NSObject>
// 该回调事件用于提供给AI模块获取视频帧(kCVPixelFormatType_32BGRA)
- (void) notifyVideoFrame:(CVPixelBufferRef)ref;

@end

@interface GTVRecController : NSObject

// 创建的时候需要指定工作目录，该目录下存有录像的缓存文件
- (id) initWithRootPath:(NSString*)path andDelegate:(id<GTVRecControllerDelegate>)cb;
- (id) initWithRootPath:(NSString*)path andDelegate:(id<GTVRecControllerDelegate>)cb defaultUseCamera:(BOOL)front;

// 必须设置，否则录像没有视频
- (void) setRenderView:(UIView*)v withFrame:(CGRect)f;

// 设置视频size
- (void) setVideoSize:(CGSize)siz;

// 设置配乐文件和开始时间
- (void) setMusicPath:(NSString*)path andStartTime:(int)milli;

// 设置录像速度（可以重复设置） DEF_GTV_SPEED_LOW/DEF_GTV_SPEED_NORMAL/DEF_GTV_SPEED_HIGH
- (void) setRecordSpeed:(int)s;

// 执行该API，从目录中获取旧录制信息，会抛出准备完成事件
- (void) initRecord;

// 暂停录制，结束当前的mp4文件
- (void) stopRecord;

// 恢复录制，创建新的muxer文件
- (void) startRecord;

// 结束录制（并且结束当前的muxer文件）
- (void) destroyRecord;

// 合并多段录制的录像（生成顺序和倒序）
- (BOOL) mergeAllClips;

// 删除最后一段录像（非录制状态调用）
- (BOOL) removeLastClip;

// 清除当前所有分段录像（非录制状态调用）
- (BOOL) clearAllCurrentClips;

// 获取当前录像分段数量(必须在initRecord和stopRecord之间调用)
- (int) getCurrentClipsCount;

////////////////////////////////////////////////////////////////////////////////
// 预览view控制，以下方法必须在setRenderView之后调用，否则无效
////////////////////////////////////////////////////////////////////////////////

// 开始/停止预览
- (void) startPreivew;
- (void) stopPreview;

- (void) rotateCamera;
- (BOOL) isUseFrontCamera:(BOOL*)flag;

// 控制缩放
- (float) currentZoomFactor;
- (void) zoomTo:(float)factor;

// 美颜状态，以及开关
- (BOOL) isBeautyOn;
- (void) switchBeauty:(BOOL)flag;

// 设置贴纸路径
- (void) useStickerByPath:(NSString*)p;
// 设置瘦脸
- (void) setSlimFaceIntensity:(float)f;
// 设置大眼
- (void) setBigEyeIntensity:(float)f;
// 设置磨皮
- (void) setSkinSmoothIntensity:(float)p;
// 设置美白
- (void) setSkinWhiteIntensity:(float)p;
// 设置曝光
- (void) setExposureIntensity:(float)p;
// 设置饱和
- (void) setSaturatedIntensity:(float)p;

// 设置滤镜
- (void) setFilterShader:(NSString*)name;
// 设置滤镜百分比(一般不需要使用)
- (void) setInstaIntensity:(float)f;

// 设置logo(请使用GTVReplayController的logo函数)
- (void) setMark:(UIImage*)img atRect:(CGRect)rect;

// 调整焦点
- (void) focusAtPoint:(CGPoint)point;

// 设置是否开启闪光灯（如果不支持返回false）
- (BOOL) setLightStatus:(BOOL)flag;     // must be called after preview

// 获取闪光灯开启状态
- (BOOL) isLightOn;

// 拍照获取图片
typedef void (^GTVTakePictureCompleteFunc)(UIImage * image);
- (void) takePicture:(GTVTakePictureCompleteFunc)cb;

// 获取视频流（用于AI）
- (void) startListenVideoFrame:(id<GTVRecVideoFrameDelegate>)d;
- (void) stopListenVideoFrame;

// 设置逐帧动画（用于AI）
- (void) startPlayAnimation:(NSString*)folder;
- (void) stopPlayAnimation;

@end
