//
//  GTVReplayController.h
//  GTVideoLib
//
//  Created by gtv on 2018/1/30.
//  Copyright © 2018年 gtv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kGTVReplayEventPrepared             @"kGTVReplayEventPrepared"      // 预读完毕，获取到文件时长
#define kGTVReplayEventCompleted            @"kGTVReplayEventCompleted"     // 播放完毕（循环播放时不会触发）
#define kGTVReplayEventError                @"kGTVReplayEventError"         // 播放失败
#define kGTVReplayEventExportStatus         @"kGTVReplayEventExportStatus"         // 合成进度更新(返回percent)

#define kGTVReplayEffectKeyStart            @"startTimestamp"
#define kGTVReplayEffectKeyStop             @"stopTimestamp"
#define kGTVReplayEffectKeyName             @"effect"

@protocol GTVReplayControllerDelegate <NSObject>

- (void) notifyReplayStatus:(NSString*)evt withInfo:(NSDictionary*)info;

@end

@interface GTVReplayController : NSObject

// 设置播放的回调
- (void) setReplayDelegate:(id<GTVReplayControllerDelegate>)cb;

// 设置渲染view
- (void) setReplayRenderView:(UIView*)v withFrame:(CGRect)r;

// 设置配乐文件和开始时间(playVideo后设置无效)
- (void) setMusicPath:(NSString*)path andRange:(NSRange)r;

// 设置慢速播放区间
- (void) setSlowPlayFrom:(int)s toTime:(int)e;

// 设置视频源
- (void) setReplayVideoPath:(NSString*)path;

// 设置设定文件存储目录（必须在playVideo之前执行，否则无效）
// 若将设定文件存储目录设置为草稿箱所在目录，特效的设定信息就会存储在草稿箱中
// 如果没有传入文件夹，则合成的时候不会导出相关设定信息，同样播放的时候也不会加载设定信息
// forceInitFlag:
//      true:强制清除旧的设定信息，从录制界面跳转到编辑界面的时候用true
//      false:继承旧的设定信息，从草稿箱跳转到编辑界面的时候用false
- (void) setReplayConfigFolder:(NSString*)folder forceInitFlag:(BOOL)flag;

// 设置循环播放次数(-1:表示无限循环)
- (void) setRepeatCount:(int)cnt;

// 设置有效播放区域（可用于裁剪）
- (void) setReplayValidRange:(NSRange)r;

// 禁音视频（有配乐的情况下可使用该API，最终导出的视频将只包含配乐的音频数据）
// true:禁音 false:非禁音
- (void) setReplayMuteStatus:(BOOL)flag;

// 播放控制
- (BOOL) playVideo;
- (void) pauseVideo;
- (void) resumeVideo;
- (void) stopVideo;
- (BOOL) seekVideoTo:(int)milli;
- (BOOL) isPaused;

// 播放进度(毫秒)
- (int) getDuration;
- (int) currentPlaybackTime;

// 特效控制(旧API，不再支持)
- (void) setEffectShader:(NSString*)name;
- (NSString*) currentEffectShader;

// APP按下某特效的时候，开始设置特效
- (BOOL) startEffectShader:(NSString*)name;
// APP松开某特效的时候，停止设置特效
- (BOOL) stopEffectShader:(NSString*)name;
// 移除最后一个特效
- (BOOL) removeLastEffectShader;
// 清除所有特效
- (BOOL) clearAllEffect;
// clone特效列表
- (BOOL) cloneEffectList:(NSArray*)elist;
// 获取当前的特效分组（用于界面展示）
- (NSArray*) currentEffectList;

// 设置水印
- (void)setMark:(UIImage*)img atRect:(CGRect)rect;
- (void)setExtraMark:(UIImage*)img atRect:(CGRect)rect;

// 设置逐帧动画数组以及动画间隔
- (void)setAnimationList:(NSArray*)list atInterval:(int)ms toRect:(CGRect)rect;
// 设置封面
- (void) setVideoCoverImage:(UIImage*)image;

// 导出编辑文件(会block住)
- (BOOL) exportReplayFile:(NSString*)path withSize:(CGSize)siz andBitrate:(int)brate;
- (BOOL) exportReplayFile:(NSString*)path andKeyFile:(NSString*)keyfile withSize:(CGSize)siz andBitrate:(int)brate;
// 取消导出（会block住直到导出中断）
- (void) cancelExportReplayFile;

// 存储设定信息
- (void) saveConfigToDisk;

@end
