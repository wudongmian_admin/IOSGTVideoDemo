
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "imageio_util.h"
#include "encode.h"
#include "mux.h"
#include "libimg2webp.h"

#define  LOGI       printf
#define  LOGE       printf

static int ReadYUV(const uint8_t* const data, size_t data_size,
                   WebPPicture* const pic) {
    //const int use_argb = pic->use_argb;
    const int uv_width = (pic->width + 1) / 2;
    const int uv_height = (pic->height + 1) / 2;
    const int y_plane_size = pic->width * pic->height;
    const int uv_plane_size = uv_width * uv_height;
    const size_t expected_data_size = y_plane_size + 2 * uv_plane_size;
    
    if (data_size != expected_data_size) {
        LOGE(
                "input data doesn't have the expected size (%d instead of %d)\n",
                (int)data_size, (int)expected_data_size);
        return 0;
    }
    
    pic->use_argb = 0;
    if (!WebPPictureAlloc(pic)) return 0;
    ImgIoUtilCopyPlane(data, pic->width, pic->y, pic->y_stride,
                       pic->width, pic->height);
    ImgIoUtilCopyPlane(data + y_plane_size, uv_width,
                       pic->u, pic->uv_stride, uv_width, uv_height);
    ImgIoUtilCopyPlane(data + y_plane_size + uv_plane_size, uv_width,
                       pic->v, pic->uv_stride, uv_width, uv_height);
    //return use_argb ? WebPPictureYUVAToARGB(pic) : 1;
    return 1;
}

static int ReadImageYuv(const char filename[], WebPPicture* const pic) {
    const uint8_t* data = NULL;
    size_t data_size = 0;
    int ok = 0;
    
    ok = ImgIoUtilReadFile(filename, &data, &data_size);
    if (!ok || data_size <= 8) goto End;
    
    pic->width = *((int*)data);
    pic->height = *((int*)(data+4));
    
    ok = ReadYUV(data+8, data_size-8, pic);
    
End:
    if (!ok) {
        LOGE("Error! Could not process file %s\n", filename);
    }
    free((void*)data);
    
    return ok;
}

static int SetLoopCount(int loop_count, WebPData* const webp_data)
{
    int ok = 1;
    WebPMuxError err;
    uint32_t features;
    WebPMuxAnimParams new_params;
    WebPMux* const mux = WebPMuxCreate(webp_data, 1);
    if (mux == NULL) return 0;
    
    err = WebPMuxGetFeatures(mux, &features);
    ok = (err == WEBP_MUX_OK);
    if (!ok || !(features & ANIMATION_FLAG)) goto End;
    
    err = WebPMuxGetAnimationParams(mux, &new_params);
    ok = (err == WEBP_MUX_OK);
    if (ok) {
        new_params.loop_count = loop_count;
        err = WebPMuxSetAnimationParams(mux, &new_params);
        ok = (err == WEBP_MUX_OK);
    }
    if (ok) {
        WebPDataClear(webp_data);
        err = WebPMuxAssemble(mux, webp_data);
        ok = (err == WEBP_MUX_OK);
    }
    
End:
    WebPMuxDelete(mux);
    if (!ok) {
        LOGE("Error during loop-count setting\n");
    }
    return ok;
}

int img2webp(const char**input_file_list, int file_num, const char* output_file)
{
    int ret = 0;
    const char* output = NULL;
    WebPAnimEncoder* enc = NULL;
    int verbose = 0;
    int pic_num = 0;
    int duration = 100;
    int timestamp_ms = 0;
    int ok = 1;
    int loop_count = 0;
    int width = 0, height = 0;
    WebPAnimEncoderOptions anim_config;
    WebPConfig config;
    WebPPicture pic;
    WebPData webp_data;
    
    int i = 0;
    
    WebPDataInit(&webp_data);
    if (!WebPAnimEncoderOptionsInit(&anim_config) ||
        !WebPConfigInit(&config) ||
        !WebPPictureInit(&pic)) {
        fprintf(stderr, "Library version mismatch!\n");
        ret = 2;
        return 2;
    }
    
    output = output_file;
    // anim_config.kmin = ExUtilGetInt(argv[++c], 0, &parse_error);  //default
    // anim_config.kmax = ExUtilGetInt(argv[++c], 0, &parse_error);    // default
    // loop_count = ExUtilGetInt(argv[++c], 0, &parse_error);
    anim_config.minimize_size = 1;
    
    pic_num = 0;
    config.lossless = 0;
    for (i = 0; i < file_num; ++i) {
        // config.quality = ExUtilGetFloat(argv[++c], &parse_error);
        // other dfault
        
        // setting is fixed, no need
        //            if (ok) {
        //              ok = WebPAnimEncoderAdd(enc, &pic, timestamp_ms, &config);
        //              if (!ok) {
        //                fprintf(stderr, "Error while adding frame #%d\n", pic_num);
        //              }
        //            }
        
        // read next input image
        
        pic.use_argb = 1;
        //ok = ReadImage(input_file_list[i], &pic);
        ok = ReadImageYuv(input_file_list[i], &pic);
        ret = 3;
        if (!ok) goto End;
        ret = 31;
        
        
        if (enc == NULL) {
            width  = pic.width;
            height = pic.height;
            enc = WebPAnimEncoderNew(width, height, &anim_config);
            ok = (enc != NULL);
            if (!ok) {
                ret = 4;
                LOGE("Could not create WebPAnimEncoder object.\n");
            }
        }
        
        if (ok) {
            ret =5;
            ok = (width == pic.width && height == pic.height);
            if (!ok) {
                ret = 6;
                LOGE("Frame #%d dimension mismatched! "
                     "Got %d x %d. Was expecting %d x %d.\n",
                     pic_num, pic.width, pic.height, width, height);
            }
        }
        
        if (ok) {
            ret = 7;
            ok = WebPAnimEncoderAdd(enc, &pic, timestamp_ms, &config);
            if (!ok) {
                ret = 8;
                LOGE("Error while adding frame #%d\n", pic_num);
            }
        }
        WebPPictureFree(&pic);
        ret = 9;
        if (!ok) goto End;
        
        if (verbose) {
            LOGE("Added frame #%3d at time %4d (file: %s)\n",
                 pic_num, timestamp_ms, input_file_list[i]);
        }
        timestamp_ms += duration;
        ++pic_num;
    }
    // add a last fake frame to signal the last duration
    ok = ok && WebPAnimEncoderAdd(enc, NULL, timestamp_ms, NULL);
    ok = ok && WebPAnimEncoderAssemble(enc, &webp_data);
    if (!ok) {
        ret = 10;
        LOGE("Error during final animation assembly.\n");
    }
    
End:
    // free resources
    WebPAnimEncoderDelete(enc);
    
    if (ok && loop_count > 0) {  // Re-mux to add loop count.
        ret = 11;
        ok = SetLoopCount(loop_count, &webp_data);
    }
    
    if (ok) {
        ret = 12;
        if (output != NULL) {
            ret =13;
            ok = ImgIoUtilWriteFile(output, webp_data.bytes, webp_data.size);
            if (ok) LOGE("output file: %s     ", output);
        } else {
            LOGE("[no output file specified]   ");
        }
    }
    
    if (ok) {
        ret = 14;
        LOGE("[%d frames, %u bytes].\n",
             pic_num, (unsigned int)webp_data.size);
    }
    
    WebPDataClear(&webp_data);
    return ok ? 0 : 1;
}

